package connectionist.ohtwo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kakao.auth.Session;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;

import java.util.HashMap;
import java.util.Map;

public class AppConfigActivity extends AppCompatActivity {


    LinearLayout noticeButton;

    Switch switchAppPush;
    Switch switchAppVibe;

    LinearLayout helpButton;
    LinearLayout logoutButton;
    LinearLayout withdrawButton;

    TextView configIndividual;
    TextView configTos;
    TextView configLicense;
    TextView configMeetingReset;

    //Button vibeButton;

    TextView configConfirmButton;

    SharedPreferences appConfig;
    SharedPreferences.Editor appConfigEditor;
    boolean appPush;
    boolean appVibe;

    String roomKey;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_config);

        appConfig = getSharedPreferences("appConfig", MODE_PRIVATE);
        appConfigEditor = appConfig.edit();
        configLoad();

        noticeButton = findViewById(R.id.noticeButton);
        noticeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), NoticeActivity.class);
                startActivity(intent);
            }
        });

        switchAppPush = findViewById(R.id.switchAppPush);
        switchAppPush.setChecked(appPush);

        switchAppPush.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                appPush = b;
            }
        });


        switchAppVibe = findViewById(R.id.switchAppVibe);
        switchAppVibe.setChecked(appVibe);
        switchAppVibe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                appVibe = b;
            }
        });

        helpButton = findViewById(R.id.help_button);
        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalApplication.needHelpSkip = false;
                Intent intent = new Intent(getApplicationContext(), HelpActivity.class);
                startActivity(intent);
            }
        });

        logoutButton = findViewById(R.id.logout_button);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

        withdrawButton =  findViewById(R.id.withdraw_button);
        withdrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), DialogTwoButtonConfig.class);
                intent.putExtra("contents", "모든 회원 정보가 사라집니다.\n정말로 탈퇴하시겠습니까?");
                startActivityForResult(intent, 1);
            }
        });

        /*vibeButton =  findViewById(R.id.vibeButton);
        vibeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ohtwoUtil.vibration(getApplicationContext());
            }
        });*/

        configTos = findViewById(R.id.configTos);
        configTos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://125.176.119.133:18080/Connectionist_termsofservice.htm"));
                startActivity(myIntent);
            }
        });

        configIndividual = findViewById(R.id.configIndividual);
        configIndividual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://125.176.119.133:18080/Connectionist_individual.html"));
                startActivity(myIntent);
            }
        });

        configLicense = findViewById(R.id.configLicense);
        configLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://125.176.119.133:18080/license.pdf"));
                startActivity(myIntent);
            }
        });

        configMeetingReset = findViewById(R.id.configMeetingReset);
        configMeetingReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String,Object> map = new HashMap<>();
                map.put("isCaptain", false);
                map.put("isWaiting", false);
                map.put("isMeeting", false);
                FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.id).updateChildren(map);
            }
        });


        configConfirmButton = findViewById(R.id.configConfirmButton);
        configConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                appConfigEditor.putBoolean("appPush", appPush);
                appConfigEditor.putBoolean("appVibe", appVibe);

                appConfigEditor.apply();
                appConfigEditor.commit();
                onBackPressed();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

            if(resultCode==11){

                FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.id).removeValue();
                FirebaseDatabase.getInstance().getReference("blindDate").orderByChild("users/"+currentUser.id).equalTo(true).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot item : dataSnapshot.getChildren()){
                            ChatModel  chatModel = item.getValue(ChatModel.class);
                            if(chatModel.users.containsKey(currentUser.id)){
                                roomKey = item.getKey();
                                FirebaseDatabase.getInstance().getReference().child("blindDate").child(roomKey).removeValue();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                apacheCall.removeUserData(currentUser.id);
                boolean success = callJsp.removeUserInfo(currentUser.id);
                ohtwoUtil.cleanCurrentUser();
                logout();

                if(success){

                    Toast.makeText(getApplicationContext(), "정상적으로 탈퇴 되었습니다.\n그 동안 이용해주셔서 감사합니다.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getBaseContext(), loginActivity.class);
                    startActivity(intent);
                    finish();
                } else{
                    Toast.makeText(getApplicationContext(), "탈퇴 과정에서 오류가 발생하였습니다.\n다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                }

            } else if(resultCode==12){
                //
            } else if(resultCode==21){
                //
            } else{
                super.onActivityResult(requestCode, resultCode, data);
            }

    }


    public void logout(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                loadingActivity.naverOAuthLoginModule.logoutAndDeleteToken(getApplicationContext()); //naver
            }
        }).start();

        if (Session.getCurrentSession().checkAndImplicitOpen()) {       //kakao

            UserManagement.getInstance().requestLogout(new LogoutResponseCallback() {
                @Override
                public void onCompleteLogout() {
                    Intent intent = new Intent(getBaseContext(), loginActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

        }

        LoginManager.getInstance().logOut(); //facebook

        ohtwoUtil.cleanCurrentUser();

        Intent intent = new Intent(getBaseContext(), loginActivity.class);
        startActivity(intent);
        finish();

    }

    public void configLoad(){
        appPush = appConfig.getBoolean("appPush", true);
        appVibe = appConfig.getBoolean("appVibe", true);

    }

}
