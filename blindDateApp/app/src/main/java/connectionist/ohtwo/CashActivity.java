package connectionist.ohtwo;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.Constants;
import com.anjlab.android.iab.v3.SkuDetails;
import com.anjlab.android.iab.v3.TransactionDetails;

import java.util.ArrayList;

public class CashActivity extends AppCompatActivity implements BillingProcessor.IBillingHandler
    {

    BillingProcessor bp;
    public static ArrayList<SkuDetails> products;

    Activity CashActivity;

    LinearLayout add20Button;
    LinearLayout add100Button;
    LinearLayout add200Button;
    LinearLayout add500Button;

    LinearLayout refreshItemButton;

    public static TextView cacheSansoText;

    TextView rfTextTime;

    TextView cashConfirmButton;


    private String BP_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAusxuU/KVBsCvA5i5DUXoXf9ac+JdglBFtJohoTXcKwJIpFOWGqrxyXQXiAudCG1hjzoL/bxYao95ydWB8m1JqhyQYLo+ahtiIiVkHIJNWG7H6vHHybEQoYLdj9V3w/0TQVm1sD+fAaLkETsp/DGLTf4wJmRHlvEGNNwEggklOCsuT0fTsheW/NwLmQjQSe1DPM5oKY2WraPyTxKUOuAXUFmKzViWKbpBZbC5PhRH1uW6mOtfKUW07a7twNcPFWW2uY743bjE25cZ5Pv13EReETqCPqIKS7zvgn848qJ5hDMrb9KlEGG1uhk//xufYiQYYmF2jFlfbXTu6yzUyDax3wIDAQAB";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash);

        CashActivity = this;

        bp = new BillingProcessor(this, BP_KEY, this);
        bp.initialize();

        cacheSansoText = findViewById(R.id.cacheSansoText);
        cacheSansoText.setText("내 산소 " + currentUser.cash);

        rfTextTime = findViewById(R.id.rfTextTime);
        if(currentUser.hasRefreshItem)
            rfTextTime.setText(currentUser.isRefreshItem);
        else
            rfTextTime.setText("");

        add20Button = findViewById(R.id.add20Button);
        add100Button = findViewById(R.id.add100Button);
        add200Button = findViewById(R.id.add200Button);
        add500Button = findViewById(R.id.add500Button);

        refreshItemButton = findViewById(R.id.refreshItemButton);

        cashConfirmButton = findViewById(R.id.cashConfirmButton);



/*        checkTenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    boolean result = ohtwoUtil.checkTen();
                    if(result)
                        Toast.makeText(getApplicationContext(), "10 산소 이상을 보유하고 있습니다.", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getApplicationContext(), "10 산소도 없네요 거지야", Toast.LENGTH_SHORT).show();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });*/

        add20Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bp.isPurchased("sanso20")){
                    bp.consumePurchase("sanso20");
                }
                bp.purchase(CashActivity, "sanso20", "connectionistOhtwo");
            }
        });

        add100Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bp.isPurchased("sanso100")){
                    bp.consumePurchase("sanso100");
                }
                bp.purchase(CashActivity, "sanso100", "connectionistOhtwo");
            }
        });

        add200Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bp.isPurchased("sanso200")){
                    bp.consumePurchase("sanso200");
                }
                bp.purchase(CashActivity, "sanso200", "connectionistOhtwo");
            }
        });

        add500Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bp.isPurchased("sanso500")){
                    bp.consumePurchase("sanso500");
                }
                bp.purchase(CashActivity, "sanso500", "connectionistOhtwo");
            }
        });

/*        addTwentyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    boolean result = ohtwoUtil.addTwenty();
                    if(result){
                        currentUser.cash = ohtwoUtil.encThreadResult;
                        o2Text.setText(ohtwoUtil.encThreadResult);
                        Toast.makeText(getApplicationContext(), "충전이 정상적으로 이루어졌습니다.", Toast.LENGTH_SHORT).show();
                    } else{
                        Toast.makeText(getApplicationContext(), "충전이 정상적으로 이루어지지 않았습니다.", Toast.LENGTH_SHORT).show();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });*/

        /*subThreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    boolean check = ohtwoUtil.checkThree();
                    if(check){
                        boolean result = ohtwoUtil.subThree();
                        if(result){
                            currentUser.cash = ohtwoUtil.encThreadResult;
                            o2Text.setText(ohtwoUtil.encThreadResult);
                            Toast.makeText(getApplicationContext(), "3 산소를 썼어요", Toast.LENGTH_SHORT).show();
                        } else{
                            Toast.makeText(getApplicationContext(), "3 산소가 제대로 안 써졌어요 개이득!", Toast.LENGTH_SHORT).show();
                        }
                    } else{
                        Toast.makeText(getApplicationContext(), "3 산소가 없네요 충전하고 써라 거지야", Toast.LENGTH_SHORT).show();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });*/


        refreshItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bp.isPurchased("refreshitem")){
                    bp.consumePurchase("refreshitem");
                }
                bp.purchase(CashActivity, "refreshitem", "connectionistOhtwo");
            }
        });

        /*DialogTwoButtonTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), DialogTwoButton.class);
                intent.putExtra("contents", "투 버튼\n다이얼로그\n테스트\nOK?");
                startActivityForResult(intent, 1);
            }
        });

        DialogOneButtonTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), DialogOneButton.class);
                intent.putExtra("contents", "원 버튼\n다이얼로그\n테스트\nOK?");
                startActivityForResult(intent, 1);
            }
        });*/

        cashConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }




        @Override
        public void onBillingInitialized() {
            /*
             * Called when BillingProcessor was initialized and it's ready to purchase
             */



        }

        @Override
        public void onProductPurchased(String productId, TransactionDetails details) {
            /*
             * Called when requested PRODUCT ID was successfully purchased
             */

            // 구매한 아이템 정보
            SkuDetails sku = bp.getPurchaseListingDetails(productId);
            int count=0;
            // 구매 처리
            try {
                //consume
                if(productId.equals("sanso20")) {
                    bp.consumePurchase("sanso20");

                    // 구매에 성공하였습니다! 메세지 띄우기
                    String purchaseMessage = sku.title + " 구매에 성공하셨습니다.";
                    Toast.makeText(this, purchaseMessage, Toast.LENGTH_LONG).show();

                    //callServer
                    boolean result = ohtwoUtil.addTwenty();

                    if (result) {
                        currentUser.cash = ohtwoUtil.encThreadResult;
                        //아래처럼 캐시 표시하는 부분 encThread에서 함
                        //o2Text.setText(ohtwoUtil.encThreadResult);
                        Toast.makeText(getApplicationContext(), "충전이 정상적으로 이루어졌습니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "충전이 정상적으로 이루어지지 않았습니다.", Toast.LENGTH_SHORT).show();
                        ohtwoUtil.cashError(currentUser.id, "sanso20", details.purchaseInfo.purchaseData.orderId);
                        while(count < 3){

                            boolean retryResult = ohtwoUtil.addTwenty();
                            if(retryResult){
                                ohtwoUtil.cashError(currentUser.id, "sanso20", "success");
                                count = 444444;
                            }

                            System.out.println("cash error report " + count + "retryResult:["+retryResult+"]");
                            count ++;

                        }

                    }
                } else if(productId.equals("sanso100")) {
                    bp.consumePurchase("sanso100");

                    // 구매에 성공하였습니다! 메세지 띄우기
                    String purchaseMessage = sku.title + " 구매에 성공하셨습니다.";
                    Toast.makeText(this, purchaseMessage, Toast.LENGTH_LONG).show();

                    //callServer
                    boolean result = ohtwoUtil.addHundred();
                    if (result) {
                        currentUser.cash = ohtwoUtil.encThreadResult;
                        //o2Text.setText(ohtwoUtil.encThreadResult);
                        Toast.makeText(getApplicationContext(), "충전이 정상적으로 이루어졌습니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "충전이 정상적으로 이루어지지 않았습니다.", Toast.LENGTH_SHORT).show();
                        ohtwoUtil.cashError(currentUser.id, "sanso100", details.purchaseInfo.purchaseData.orderId);
                        while(count < 3){

                            boolean retryResult = ohtwoUtil.addHundred();
                            if(retryResult){
                                ohtwoUtil.cashError(currentUser.id, "sanso100", "success");
                                count = 444444;
                            }

                            System.out.println("cash error report " + count + "retryResult:["+retryResult+"]");
                            count ++;

                        }
                    }
                } else if(productId.equals("sanso200")) {
                    bp.consumePurchase("sanso200");

                    // 구매에 성공하였습니다! 메세지 띄우기
                    String purchaseMessage = sku.title + " 구매에 성공하셨습니다.";
                    Toast.makeText(this, purchaseMessage, Toast.LENGTH_LONG).show();

                    //callServer
                    boolean result = ohtwoUtil.addTwoHundred();
                    if (result) {
                        currentUser.cash = ohtwoUtil.encThreadResult;
                        //o2Text.setText(ohtwoUtil.encThreadResult);
                        Toast.makeText(getApplicationContext(), "충전이 정상적으로 이루어졌습니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "충전이 정상적으로 이루어지지 않았습니다.", Toast.LENGTH_SHORT).show();
                        ohtwoUtil.cashError(currentUser.id, "sanso200", details.purchaseInfo.purchaseData.orderId);
                        while(count < 3){

                            boolean retryResult = ohtwoUtil.addTwoHundred();
                            if(retryResult){
                                ohtwoUtil.cashError(currentUser.id, "sanso200", "success");
                                count = 444444;
                            }

                            System.out.println("cash error report " + count + "retryResult:["+retryResult+"]");
                            count ++;

                        }
                    }
                } else if(productId.equals("sanso500")) {
                    bp.consumePurchase("sanso500");

                    // 구매에 성공하였습니다! 메세지 띄우기
                    String purchaseMessage = sku.title + " 구매에 성공하셨습니다.";
                    Toast.makeText(this, purchaseMessage, Toast.LENGTH_LONG).show();

                    //callServer
                    boolean result = ohtwoUtil.addFiveHundred();
                    if (result) {
                        currentUser.cash = ohtwoUtil.encThreadResult;
                        //o2Text.setText(ohtwoUtil.encThreadResult);
                        Toast.makeText(getApplicationContext(), "충전이 정상적으로 이루어졌습니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "충전이 정상적으로 이루어지지 않았습니다.", Toast.LENGTH_SHORT).show();
                        ohtwoUtil.cashError(currentUser.id, "sanso500", details.purchaseInfo.purchaseData.orderId);
                        while(count < 3){

                            boolean retryResult = ohtwoUtil.addFiveHundred();
                            if(retryResult){
                                ohtwoUtil.cashError(currentUser.id, "sanso500", "success");
                                count = 444444;
                            }

                            System.out.println("cash error report " + count + "retryResult:["+retryResult+"]");
                            count ++;

                        }
                    }
                } else if(productId.equals("refreshitem")){
                    bp.consumePurchase("refreshitem");

                    // 구매에 성공하였습니다! 메세지 띄우기
                    String purchaseMessage = sku.title + " 구매에 성공하셨습니다.";
                    Toast.makeText(this, purchaseMessage, Toast.LENGTH_LONG).show();

                    //callServer
                    boolean result = ohtwoUtil.refreshItem();
                    if (result) {
                        currentUser.isRefreshItem = ohtwoUtil.encThreadResult;
                        currentUser.hasRefreshItem = ohtwoUtil.calRefresh();
                        rfTextTime.setText(ohtwoUtil.encThreadResult);
                        Toast.makeText(getApplicationContext(), "정상적으로 광고가 제거 되었습니다.", Toast.LENGTH_SHORT).show();

                        main_page.underLineText = new SpannableString("남은 기간 : " + currentUser.isRefreshItem);
                        main_page.underLineText.setSpan(new UnderlineSpan(), 0, main_page.underLineText.length(), 0);
                        main_page.refreshItemMoveText.setText(main_page.underLineText);
                        main_page.refreshButton1.setVisibility(View.INVISIBLE);
                        main_page.refreshButton2.setVisibility(View.INVISIBLE);

                    } else {
                        Toast.makeText(getApplicationContext(), "정상적으로 광고가 제거되지 않았습니다.", Toast.LENGTH_SHORT).show();
                        ohtwoUtil.cashError(currentUser.id, "refreshItem", details.purchaseInfo.purchaseData.orderId);
                        while(count < 3){

                            boolean retryResult = ohtwoUtil.refreshItem();
                            if(retryResult){
                                ohtwoUtil.cashError(currentUser.id, "refreshItem", "success");
                                count = 444444;
                            }

                            System.out.println("cash error report " + count + "retryResult:["+retryResult+"]");
                            count ++;

                        }
                    }
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }

        @Override
        public void onBillingError(int errorCode, Throwable error) {
            /*
             * Called when some error occurred. See Constants class for more details
             *
             * Note - this includes handling the case where the user canceled the buy dialog:
             * errorCode = Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
             */
            if(errorCode == Constants.BILLING_RESPONSE_RESULT_USER_CANCELED){
                Toast.makeText(getApplicationContext(), "산소 충전을 취소하셨습니다.", Toast.LENGTH_SHORT).show();
            } else{
                Toast.makeText(getApplicationContext(), "산소 충전 중 에러가 발생하였습니다.", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onPurchaseHistoryRestored() {
            /*
             * Called when purchase history was restored and the list of all owned PRODUCT ID's
             * was loaded from Google Play
             */
        }


        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (!bp.handleActivityResult(requestCode, resultCode, data)) {

                if(resultCode==11){
                    Toast.makeText(getApplicationContext(), "Two Button Dialog OK", Toast.LENGTH_SHORT).show();
                } else if(resultCode==12){
                    Toast.makeText(getApplicationContext(), "Two Button Dialog Cancel", Toast.LENGTH_SHORT).show();
                } else if(resultCode==21){
                    Toast.makeText(getApplicationContext(), "One Button Dialog OK", Toast.LENGTH_SHORT).show();
                } else{
                    super.onActivityResult(requestCode, resultCode, data);
                }
            } else{
                super.onActivityResult(requestCode, resultCode, data);
            }
        }

        @Override
        public void onDestroy() {
            if (bp != null) {
                bp.release();
            }
            super.onDestroy();
        }

}
