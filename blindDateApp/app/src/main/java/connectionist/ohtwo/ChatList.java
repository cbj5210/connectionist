package connectionist.ohtwo;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

public class ChatList extends AppCompatActivity {

    private RecyclerView recyclerView;
    public ImageView home_button;

    private String userName = null;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd\nHH:mm");

    public static Activity chatListActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);

        chatListActivity = this;

        //하단의 onResume()에 동일하게 적어야 동작

        home_button = findViewById(R.id.home_button);
        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerView = (RecyclerView)findViewById(R.id.chatlist_recyclerview);
        recyclerView.setAdapter(new ChatRecyclerViewAdapter());
        recyclerView.setLayoutManager(new LinearLayoutManager(ChatList.this));

    }
    class ChatRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

        private List<ChatModel> chatModels = new ArrayList<>();
        private String uid;
        private ArrayList<String> dstUsers = new ArrayList<>();
        Uri profileUri;
        SharedPreferences appConfig;
        SharedPreferences.Editor appConfigEditor;

        public ChatRecyclerViewAdapter() {
            uid = currentUser.id;
            FirebaseDatabase.getInstance().getReference().child("blindDate").orderByChild("users/"+uid).equalTo(true).addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    chatModels.clear();

                    for(DataSnapshot item : dataSnapshot.getChildren()){
                        chatModels.add(item.getValue(ChatModel.class));
                    }
                    notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatlist_item, parent,false);
            return new CustomViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

            final CustomViewHolder customViewHolder = (CustomViewHolder)holder;

            String dstUid = null;

            for(String user: chatModels.get(position).users.keySet()){
                if(!user.equals(uid)){

                    dstUid = user;
                    dstUsers.add(dstUid);
                    Uri.parse("file:///" + Environment.getExternalStorageDirectory() + "/.connectionist/"+dstUid+"/user.jpg");

                    /*
                    try {
                        JSONObject outputJson = callJsp.searchUserInfo(dstUid);
                        userName = outputJson.getString("name");
                        dstUid = user;
                        dstUsers.add(dstUid);
                        Uri.parse("file:///" + Environment.getExternalStorageDirectory() + "/.connectionist/"+dstUid+"/user.jpg");
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                    */


        }
    }
    //customViewHolder.textView_chatName.setText(userName);
    appConfig = getSharedPreferences("appConfig", MODE_PRIVATE);
    appConfigEditor = appConfig.edit();
    String tempName = "";
    tempName = appConfig.getString(dstUid, "이름없음");

            if(tempName.equals("이름없음")){
                tempName = callJsp.requestName(dstUid);
                appConfigEditor.putString(dstUid, tempName);
                appConfigEditor.apply();
                appConfigEditor.commit();
            }

            customViewHolder.textView_chatName.setText(tempName);

            apacheCall.imageDownload(dstUid, chatListActivity);

    String imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/.connectionist/"+dstUid+"/user.jpg";

    File tempFile = new File(imgFilePath);
            if(tempFile.exists()){
        Drawable drawable = Drawable.createFromPath(imgFilePath);
        customViewHolder.imageView_chatlist.setImageDrawable(drawable);
    }

            //apacheCall.imageViewDownloadContext(customViewHolder.imageView_chatlist, d stUid, customViewHolder.itemView.getContext());
            /*Glide.with(customViewHolder.itemView.getContext())
                    .load(profileUri)
                    .asBitmap()
                    .into((customViewHolder).imageView_chatlist);*/

            // recentMsg
            Map<String,ChatModel.Message> messageMap = new TreeMap<>(Collections.reverseOrder());
            messageMap.putAll(chatModels.get(position).message);
            if(messageMap.keySet().toArray().length > 0) {
                String recentMsgKey = (String) messageMap.keySet().toArray()[0];
                customViewHolder.textView_recentChat.setText(chatModels.get(position).message.get(recentMsgKey).message);

                customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(recyclerView.getContext(), chatActivity.class);
                        intent.putExtra("otherID", dstUsers.get(position));

                        startActivity(intent);
                    }
                });

                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
                long unixTime = (long) chatModels.get(position).message.get(recentMsgKey).timestamp;
                Date date = new Date(unixTime);
                customViewHolder.textView_time.setText(simpleDateFormat.format(date));
            }
        }

        @Override
        public int getItemCount() {
            return chatModels.size();
        }

        private class CustomViewHolder extends RecyclerView.ViewHolder {

            public ImageView imageView_chatlist;
            public TextView textView_chatName;
            public TextView textView_recentChat;
            public TextView textView_time;
            public CustomViewHolder(View view) {
                super(view);

                imageView_chatlist = (ImageView)view.findViewById(R.id.chatlist_imageView);
                textView_chatName = (TextView)view.findViewById(R.id.chatlist_chatname);
                textView_recentChat = (TextView)view.findViewById(R.id.chatlist_recentchat);
                textView_time = (TextView)view.findViewById(R.id.chatlist_time);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.activity_chat_list);

        home_button = findViewById(R.id.home_button);
        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerView = (RecyclerView)findViewById(R.id.chatlist_recyclerview);
        recyclerView.setAdapter(new ChatRecyclerViewAdapter());
        recyclerView.setLayoutManager(new LinearLayoutManager(ChatList.this));
    }
}
