package connectionist.ohtwo;

import java.util.HashMap;
import java.util.Map;

public class ChatModel {

    public Map<String,Boolean> users = new HashMap<>(); //채팅방의 유저들
    public Map<String,Message> message = new HashMap<>();//채팅방의 대화내용

    public static class Message {

        public String uid;
        public String message;
        public Object timestamp;
    }

}