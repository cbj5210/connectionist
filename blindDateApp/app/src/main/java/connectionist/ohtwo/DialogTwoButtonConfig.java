package connectionist.ohtwo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class DialogTwoButtonConfig extends Activity {

    TextView dialogTwoButtonTextConfig;

    TextView dialogTwoButtonConfirmConfig;
    TextView dialogTwoButtonCancelConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 타이틀바 제거
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 팝업이 올라오면 배경 블러처리
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount = 0.7f;
        getWindow().setAttributes(layoutParams);
        setContentView(R.layout.dialog_two_button_config);
        dialogTwoButtonTextConfig = findViewById(R.id.dialogTwoButtonTextConfig);

        Intent intent = getIntent();
        String contents = intent.getStringExtra("contents");

        if(contents.indexOf("\\n")>-1){
            contents = contents.replaceAll("\\\\n", "\n");
        }

        dialogTwoButtonTextConfig.setText(contents);

        dialogTwoButtonCancelConfig = findViewById(R.id.dialogTwoButtonCancelConfig);
        dialogTwoButtonCancelConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                //intent.putExtra("result", "Close Popup");
                setResult(12, intent);

                //액티비티(팝업) 닫기
                finish();
            }
        });

        dialogTwoButtonConfirmConfig = findViewById(R.id.dialogTwoButtonConfirmConfig);
        dialogTwoButtonConfirmConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                //intent.putExtra("result", "Close Popup");
                setResult(11, intent);

                //액티비티(팝업) 닫기
                finish();
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //바깥레이어 클릭시 안닫히게
        if(event.getAction()== MotionEvent.ACTION_OUTSIDE){
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        //안드로이드 백버튼 막기
        return;
    }

}
