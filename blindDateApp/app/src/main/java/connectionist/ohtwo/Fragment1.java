package connectionist.ohtwo;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class Fragment1 extends Fragment {

    public Fragment1(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View f1View = inflater.inflate(R.layout.activity_fragment1, container, false);

        ImageView iv = (ImageView) f1View.findViewById(R.id.imageViewF1);

        String imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/.connectionist/"+main_page.idUri[0]+"/user.jpg";
        Drawable drawable = Drawable.createFromPath(imgFilePath);

        iv.setImageResource(R.drawable.dim);
        iv.setBackground(drawable);

        return f1View;

        /*if (fm1View!=null && fm1View.getParent() != null) {
            ((ViewGroup) fm1View.getParent()).removeView(fm1View);
        }
        if(fm1View ==null) {
            fm1View = inflater.inflate(R.layout.activity_fragment1, null);
        } else{

        }*/
        /*if(container!=null)
            container.addView(fm1View);*/
        //return fm1View;
    }

    public void onStart() {
        super.onStart();
    }

}
