package connectionist.ohtwo;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Fragment1Help extends Fragment {

    public Fragment1Help(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View f1View = inflater.inflate(R.layout.activity_fragment1_help, container, false);

        TextView tv = f1View.findViewById(R.id.helpSkip1);

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), main_page.class);
                //Intent intent = new Intent(getBaseContext(), OpenWaitActivity.class);
                intent.putExtra("isRefresh","true");
                startActivity(intent);
            }
        });

        if(GlobalApplication.needHelpSkip)
            tv.setVisibility(View.VISIBLE);
        else
            tv.setVisibility(View.INVISIBLE);

        return f1View;

        /*if (fm1View!=null && fm1View.getParent() != null) {
            ((ViewGroup) fm1View.getParent()).removeView(fm1View);
        }
        if(fm1View ==null) {
            fm1View = inflater.inflate(R.layout.activity_fragment1, null);
        } else{

        }*/
        /*if(container!=null)
            container.addView(fm1View);*/
        //return fm1View;
    }

    public void onStart() {
        super.onStart();
    }

}
