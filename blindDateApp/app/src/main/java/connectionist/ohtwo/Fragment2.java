package connectionist.ohtwo;

import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class Fragment2 extends Fragment {

    public Fragment2(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View f2View = inflater.inflate(R.layout.activity_fragment2, container, false);

        ImageView iv = (ImageView) f2View.findViewById(R.id.imageViewF2);

        String imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/.connectionist/"+main_page.idUri[1]+"/user.jpg";
        Drawable drawable = Drawable.createFromPath(imgFilePath);

        iv.setImageResource(R.drawable.dim);
        iv.setBackground(drawable);

        return f2View;

     /*   if (fm2View!=null && fm2View.getParent() != null) {
            ((ViewGroup) fm2View.getParent()).removeView(fm2View);
        }
        if(fm2View ==null) {
            //fm2View = inflater.inflate(R.layout.activity_fragment2, container, false);
            fm2View = inflater.inflate(R.layout.activity_fragment2, null);
        } else{

        }*/
        /*if(container!=null)
            container.addView(fm2View);*/
        //return fm2View;
    }


    public void onStart() {
        super.onStart();
    }

}
