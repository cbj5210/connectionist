package connectionist.ohtwo;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class Fragment2Meeting extends Fragment {

    public Fragment2Meeting(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View f2ViewMeeting = inflater.inflate(R.layout.activity_fragment2_meeting, container, false);

        ImageView iv = (ImageView) f2ViewMeeting.findViewById(R.id.imageViewF2Meeting);

        String imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/.connectionist/"+main_page.idUriMeeting[1]+"/user.jpg";
        Drawable drawable = Drawable.createFromPath(imgFilePath);

        iv.setImageResource(R.drawable.dim);
        iv.setBackground(drawable);

        return f2ViewMeeting;

     /*   if (fm2View!=null && fm2View.getParent() != null) {
            ((ViewGroup) fm2View.getParent()).removeView(fm2View);
        }
        if(fm2View ==null) {
            //fm2View = inflater.inflate(R.layout.activity_fragment2, container, false);
            fm2View = inflater.inflate(R.layout.activity_fragment2, null);
        } else{

        }*/
        /*if(container!=null)
            container.addView(fm2View);*/
        //return fm2View;
    }


    public void onStart() {
        super.onStart();
    }

}
