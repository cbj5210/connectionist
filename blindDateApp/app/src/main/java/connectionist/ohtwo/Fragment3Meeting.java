package connectionist.ohtwo;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class Fragment3Meeting extends Fragment {

    public Fragment3Meeting(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View f3ViewMeeting = inflater.inflate(R.layout.activity_fragment3_meeting, container, false);

        ImageView iv = (ImageView) f3ViewMeeting.findViewById(R.id.imageViewF3Meeting);

        String imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/.connectionist/"+main_page.idUriMeeting[2]+"/user.jpg";
        Drawable drawable = Drawable.createFromPath(imgFilePath);

        iv.setImageResource(R.drawable.dim);
        iv.setBackground(drawable);

        return f3ViewMeeting;

        /*if (fm3View!=null && fm3View.getParent() != null) {
            ((ViewGroup) fm3View.getParent()).removeView(fm3View);
        }
        if(fm3View ==null) {
            fm3View = inflater.inflate(R.layout.activity_fragment3, null);
        } else{

        }*/
        /*if(container!=null)
            container.addView(fm3View);*/
        //return fm3View;
    }


    public void onStart() {
        super.onStart();
    }

}
