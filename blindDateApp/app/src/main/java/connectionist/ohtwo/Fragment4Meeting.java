package connectionist.ohtwo;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class Fragment4Meeting extends Fragment {

    public Fragment4Meeting(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View f4ViewMeeting = inflater.inflate(R.layout.activity_fragment4_meeting, container, false);

        ImageView iv = (ImageView) f4ViewMeeting.findViewById(R.id.imageViewF4Meeting);

        String imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/.connectionist/"+main_page.idUriMeeting[3]+"/user.jpg";
        Drawable drawable = Drawable.createFromPath(imgFilePath);

        iv.setImageResource(R.drawable.dim);
        iv.setBackground(drawable);

        return f4ViewMeeting;
        /*if (fm4View!=null && fm4View.getParent() != null) {
            ((ViewGroup) fm4View.getParent()).removeView(fm4View);
        }
        if(fm4View ==null) {
            fm4View = inflater.inflate(R.layout.activity_fragment4, null);
        } else{

        }*/
        /*if(container!=null)
            container.addView(fm4View);*/
        //return fm4View;
    }


    public void onStart() {
        super.onStart();
    }

}
