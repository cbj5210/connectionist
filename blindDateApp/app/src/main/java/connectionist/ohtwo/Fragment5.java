package connectionist.ohtwo;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class Fragment5 extends Fragment {

    public Fragment5(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View f5View = inflater.inflate(R.layout.activity_fragment5, container, false);

        ImageView iv = (ImageView) f5View.findViewById(R.id.imageViewF5);

        String imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/.connectionist/"+main_page.idUri[4]+"/user.jpg";
        Drawable drawable = Drawable.createFromPath(imgFilePath);

        iv.setImageResource(R.drawable.dim);
        iv.setBackground(drawable);

        return f5View;

 /*       if (fm5View!=null && fm5View.getParent() != null) {
            ((ViewGroup) fm5View.getParent()).removeView(fm5View);
        }
        if(fm5View ==null) {
            fm5View = inflater.inflate(R.layout.activity_fragment5, null);
        } else{

        }
        *//*if(container!=null)
            container.addView(fm5View);*//*
        return fm5View;*/
    }


    public void onStart() {
        super.onStart();
    }

}
