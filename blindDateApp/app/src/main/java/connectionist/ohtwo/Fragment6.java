package connectionist.ohtwo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class Fragment6 extends Fragment {

    public Fragment6(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View f6View = inflater.inflate(R.layout.activity_fragment6, container, false);

        AdView mAdView = (AdView) f6View.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

/*        ImageView iv = (ImageView) f6View.findViewById(R.id.imageViewF6);

        String imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/.connectionist/"+"104"+"/user.jpg";
        Drawable drawable = Drawable.createFromPath(imgFilePath);

        iv.setImageResource(R.drawable.dim);
        iv.setBackground(drawable);*/



        return f6View;
        /*if (fm6View!=null && fm6View.getParent() != null) {
            ((ViewGroup) fm6View.getParent()).removeView(fm6View);
        }
        if(fm6View ==null) {
            fm6View = inflater.inflate(R.layout.activity_fragment6, null);
        } else{

        }*/

        /*if(container!=null)
            container.addView(fm6View);*/
        //return fm6View;
    }

/*
    @Override
    public View getView() {
        return fm6View;
    }
*/

    public void onStart() {
        super.onStart();
    }

}
