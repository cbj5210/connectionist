package connectionist.ohtwo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class FragmentNothing extends Fragment {

    public FragmentNothing(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View fnView = inflater.inflate(R.layout.activity_fragment_nothing, container, false);

        ImageView iv = (ImageView) fnView.findViewById(R.id.imageViewFN);

        /*String imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/.connectionist/"+main_page.idUri[0]+"/user.jpg";
        Drawable drawable = Drawable.createFromPath(imgFilePath);*/

        /*iv.setImageResource(R.drawable.dim);
        iv.setBackground(drawable);*/

        iv.setImageResource(R.drawable.default_user_image);

        return fnView;

        /*if (fm1View!=null && fm1View.getParent() != null) {
            ((ViewGroup) fm1View.getParent()).removeView(fm1View);
        }
        if(fm1View ==null) {
            fm1View = inflater.inflate(R.layout.activity_fragment1, null);
        } else{

        }*/
        /*if(container!=null)
            container.addView(fm1View);*/
        //return fm1View;
    }

    public void onStart() {
        super.onStart();
    }

}
