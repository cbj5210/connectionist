package connectionist.ohtwo;

public class FriendInfo {

    public String id;
    public String name;
    public boolean selected;

    public FriendInfo(String id, String name){
        this.id = id;
        this.name = name;
        this.selected=false;
    }
}
