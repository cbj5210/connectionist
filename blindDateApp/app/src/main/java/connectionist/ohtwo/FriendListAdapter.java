package connectionist.ohtwo;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class FriendListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView friendImage;
        TextView friendName;
        ImageView friendCheck;
        LinearLayout friendLayout;

        Drawable drawable;

        MyViewHolder(View view){
            super(view);
            friendImage = view.findViewById(R.id.friendListImage);
            friendName = view.findViewById(R.id.friendListName);
            friendLayout = view.findViewById(R.id.friendListLayout);
            friendCheck = view.findViewById(R.id.friendListCheck);

        }
    }

    public ArrayList<FriendInfo> friendInfoArrayList;

    FriendListAdapter(ArrayList<FriendInfo> friendInfoArrayList){
        this.friendInfoArrayList = friendInfoArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_row, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final MyViewHolder myViewHolder = (MyViewHolder) holder;

        String imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.connectionist/" + friendInfoArrayList.get(position).id + "/user.jpg";
        final Drawable drawable = Drawable.createFromPath(imgFilePath);

        myViewHolder.friendImage.post(new Runnable() {
            @Override
            public void run() {
                myViewHolder.friendImage.setImageDrawable(drawable);
            }
        });

        myViewHolder.friendName.setText(friendInfoArrayList.get(position).name);

        myViewHolder.friendCheck.setVisibility(View.INVISIBLE);

        myViewHolder.friendLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();

                if(friendInfoArrayList.get(position).selected){
                    friendInfoArrayList.get(position).selected = false;
                    //Toast.makeText(context, position + "번 선택해제하였습니다", Toast.LENGTH_LONG).show();
                    currentUser.member_Number--;
                    //myViewHolder.friendLayout.setBackgroundColor(Color.parseColor("#ffffff"));
                    myViewHolder.friendCheck.setVisibility(View.INVISIBLE);

                } else if(currentUser.member_Number >= 3){
                    Intent intent = new Intent(context, MeetingOneButton.class);
                    intent.putExtra("contents", "친구는 3명까지만\n선택할 수 있습니다.");
                    context.startActivity(intent);
                } else if (!friendInfoArrayList.get(position).selected && currentUser.member_Number < 3){
                    friendInfoArrayList.get(position).selected = true;
                    //Toast.makeText(context, position + "번 선택됨", Toast.LENGTH_LONG).show();
                    currentUser.member_Number++;
                    //myViewHolder.friendLayout.setBackgroundColor(Color.parseColor("#567845"));
                    myViewHolder.friendCheck.setVisibility(View.VISIBLE);
                }else{
                    //Toast.makeText(context, "더 이상 선택할 수 없습니다", Toast.LENGTH_LONG).show();
                }
                main_page.meetingNumberText.setText(currentUser.member_Number+"명");
            }
        });

    }


    @Override
    public int getItemCount(){
        return friendInfoArrayList.size();
    }
}

