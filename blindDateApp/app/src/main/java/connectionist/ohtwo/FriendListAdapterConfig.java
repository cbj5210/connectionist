package connectionist.ohtwo;

import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class FriendListAdapterConfig extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView friendImage;
        TextView friendName;
        ImageView friendCheck;
        LinearLayout friendLayout;

        Drawable drawable;

        MyViewHolder(View view){
            super(view);
            friendImage = view.findViewById(R.id.friendListImage);
            friendName = view.findViewById(R.id.friendListName);
            friendLayout = view.findViewById(R.id.friendListLayout);
            friendCheck = view.findViewById(R.id.friendListCheck);

        }
    }

    public ArrayList<FriendInfo> friendInfoArrayList;

    FriendListAdapterConfig(ArrayList<FriendInfo> friendInfoArrayList){
        this.friendInfoArrayList = friendInfoArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_row, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final MyViewHolder myViewHolder = (MyViewHolder) holder;

        String imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.connectionist/" + friendInfoArrayList.get(position).id + "/user.jpg";
        final Drawable drawable = Drawable.createFromPath(imgFilePath);

        myViewHolder.friendImage.post(new Runnable() {
            @Override
            public void run() {
                myViewHolder.friendImage.setImageDrawable(drawable);
            }
        });

        myViewHolder.friendName.setText(friendInfoArrayList.get(position).name);

        myViewHolder.friendCheck.setVisibility(View.INVISIBLE);

        myViewHolder.friendLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }


    @Override
    public int getItemCount(){
        return friendInfoArrayList.size();
    }
}

