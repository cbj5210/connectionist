package connectionist.ohtwo;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class HelpActivity extends AppCompatActivity {

    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        viewPager = findViewById(R.id.viewPagerHelp);
        MyViewPagerAdapterHelp adapter = new MyViewPagerAdapterHelp(getSupportFragmentManager());
        getFragmentManager().beginTransaction().commitNow();

        viewPager.setAdapter(adapter);





    }
}
