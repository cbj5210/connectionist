package connectionist.ohtwo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class MeetingProcessFirst extends Activity {

    TextView howmany;

    TextView meetingTwoButtonConfirm;
    TextView meetingTwoButtonCancel;

    static String response = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 타이틀바 제거
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 팝업이 올라오면 배경 블러처리
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount = 0.7f;
        getWindow().setAttributes(layoutParams);
        setContentView(R.layout.meeting_two_button);
        howmany = findViewById(R.id.howmany);

        Intent intent = getIntent();
        String meeting_number = intent.getStringExtra("contents");

        try {
            String meetingUrl = "http://125.176.119.133:18080/blindDateServer/getMeetingUser.jsp?id=" + currentUser.id + "&sex=" + currentUser.sex + "&lat=" + currentUser.latitude + "&lon=" + currentUser.longitude + "&mee=" + currentUser.meeting_join_number;
            callJsp.run(meetingUrl);
            response = callJsp.response;
            String waiting_number = response.substring(response.length()-1);

            //대기팀이 0팀이다.
            if(waiting_number.equals("0")){
                String inputString = currentUser.meeting_join_number + "대" + currentUser.meeting_join_number + "입니다" + "\n대기팀" + waiting_number + "팀 있습니다 \n 지역을 바꿔주세요";
                howmany.setText(inputString);
                meetingTwoButtonConfirm = findViewById(R.id.meetingTwoButtonConfirm);
                meetingTwoButtonConfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       Intent intent = new Intent();
                            intent.putExtra("result", "noTeam");
                            setResult(11, intent);
                            finish();
                    }
                });

            // 대기팀이 1팀이상 있다.
            }else{
                String inputString = currentUser.meeting_join_number + "대" + currentUser.meeting_join_number + "입니다" + "\n 5산소가 소모됩니다" + "\n대기팀" + waiting_number + "팀 있습니다";
                howmany.setText(inputString);
                meetingTwoButtonConfirm = findViewById(R.id.meetingTwoButtonConfirm);
                meetingTwoButtonConfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        boolean a = false;
                        try{
                            a = ohtwoUtil.checkFive();
                        }catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if(a){
                            Intent intent = new Intent();
                            intent.putExtra("result", "existTeam");
                            setResult(11, intent);
                            finish();
                        }else {
                            Intent intent = new Intent(getApplicationContext(), MeetingProcessTwo.class);
                            startActivityForResult(intent, 10);
                        }
                    }
                });
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        meetingTwoButtonCancel = findViewById(R.id.meetingTwoButtonCancel);
        meetingTwoButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("result", "cancel");
                setResult(12, intent);
                finish();
            }
        });


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //바깥레이어 클릭시 안닫히게
        if(event.getAction()== MotionEvent.ACTION_OUTSIDE){
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        //안드로이드 백버튼 막기
        return;
    }

}
