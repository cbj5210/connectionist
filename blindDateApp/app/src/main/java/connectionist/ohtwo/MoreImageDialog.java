package connectionist.ohtwo;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

public class MoreImageDialog extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 타이틀바 제거
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 팝업이 올라오면 배경 블러처리
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount = 0.7f;
        getWindow().setAttributes(layoutParams);
        setContentView(R.layout.more_image_dialog);

        ViewPager moreImagevViewPager = findViewById(R.id.moreImagevViewPager);
        MyViewPagerAdapterMoreImage adapter = new MyViewPagerAdapterMoreImage(getSupportFragmentManager());
        getFragmentManager().beginTransaction().commitNow();

        moreImagevViewPager.setAdapter(adapter);

    }

/*    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //바깥레이어 클릭시 안닫히게
        if(event.getAction()== MotionEvent.ACTION_OUTSIDE){
            return false;
        }
        return true;
    }*/

/*    @Override
    public void onBackPressed() {
        //안드로이드 백버튼 막기
        return;
    }*/

}
