package connectionist.ohtwo;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

public class MoreImageFragment2Meeting extends Fragment {

    public MoreImageFragment2Meeting(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View f2View = inflater.inflate(R.layout.more_image_fragment2_meeting, container, false);

        ImageView iv = (ImageView) f2View.findViewById(R.id.moreImageDialogImageView2Meeting);
        TextView tv = f2View.findViewById(R.id.moreImageDialogImageView2TextMeeting);

        String imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/.connectionist/"+main_page.idUriMeeting[main_page.referenceMoreImageMeeting]+"/user_sub_1.jpg";

        File tempFile = new File(imgFilePath);
        if(tempFile.exists()){
            Drawable drawable = Drawable.createFromPath(imgFilePath);

            tv.setBackgroundResource(R.drawable.dialog_dim);
            iv.setImageDrawable(drawable);
        } else{
            tv.setBackgroundResource(R.drawable.dialog_dim);
            iv.setImageResource(R.drawable.default_more_image);
        }

        return f2View;

        /*if (fm1View!=null && fm1View.getParent() != null) {
            ((ViewGroup) fm1View.getParent()).removeView(fm1View);
        }
        if(fm1View ==null) {
            fm1View = inflater.inflate(R.layout.activity_fragment1, null);
        } else{

        }*/
        /*if(container!=null)
            container.addView(fm1View);*/
        //return fm1View;
    }

    public void onStart() {
        super.onStart();
    }

}
