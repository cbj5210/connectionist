package connectionist.ohtwo;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.fsn.cauly.blackdragoncore.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService{
    @Override
    public void onNewToken(String newToken) {
        super.onNewToken(newToken);
        Log.e("token",newToken);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        SharedPreferences appConfig;
        appConfig = getSharedPreferences("appConfig", MODE_PRIVATE);

        boolean appPush;
        boolean appVibe;
        appPush = appConfig.getBoolean("appPush", true);
        appVibe = appConfig.getBoolean("appVibe", true);

        if(appPush){
            if (remoteMessage.getData().size() > 0) {
                String title = remoteMessage.getData().get("title").toString();
                String text = remoteMessage.getData().get("text").toString();
                String tag = remoteMessage.getData().get("tag").toString();
                //String clickAction = remoteMessage.getNotification().getClickAction();
                sendNotification(title, text, tag/*, clickAction*/);
                if(appVibe == true) {
                    ohtwoUtil.vibration(GlobalApplication.getGlobalApplicationContext());
                }
            }
        }
    }
    private void sendNotification(String title, String text, String tag/*, String clickAction*/) {

        Integer intTag = Integer.parseInt(tag);

        Intent intent = new Intent(this, ChatList.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setVibrate(new long[] { 0, 0})
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(intTag /* ID of notification */, notificationBuilder.build());
    }
}

