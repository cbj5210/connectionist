package connectionist.ohtwo;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class MyViewPagerAdapter extends FragmentPagerAdapter {

    public Fragment Fragment1;
    public Fragment Fragment2;
    public Fragment Fragment3;
    public Fragment Fragment4;
    public Fragment Fragment5;
    public Fragment Fragment6;
    public Fragment FragmentN1;
    public Fragment FragmentN2;
    public Fragment FragmentN3;
    public Fragment FragmentN4;
    public Fragment FragmentN5;

    String [] output = new String[5];

    public MyViewPagerAdapter(FragmentManager fm) {
        super(fm);

        Fragment1 = new Fragment1();
        Fragment2 = new Fragment2();
        Fragment3 = new Fragment3();
        Fragment4 = new Fragment4();
        Fragment5 = new Fragment5();
        Fragment6 = new Fragment6();
        FragmentN1=  new FragmentNothing();
        FragmentN2=  new FragmentNothing();
        FragmentN3=  new FragmentNothing();
        FragmentN4=  new FragmentNothing();
        FragmentN5=  new FragmentNothing();

        fm.executePendingTransactions();
    }

    @Override
    public Fragment getItem(int arg0) {

        if(arg0 >= 0 && arg0 <=4){

            if(main_page.idUri[arg0].equals("")){

                if (arg0 == 0) {
                    //Fragment1 f1 = new Fragment1();
                    return FragmentN1;
                } else if (arg0 == 1) {
                    //Fragment2 f2 = new Fragment2();
                    return FragmentN2;
                } else if (arg0 == 2) {
                    //Fragment3 f3 = new Fragment3();
                    return FragmentN3;
                } else if (arg0 == 3) {
                    //Fragment4 f4 = new Fragment4();
                    return FragmentN4;
                } else if (arg0 == 4) {
                    //Fragment5 f5 = new Fragment5();
                    return FragmentN5;
                }

            } else{
                if (arg0 == 0) {
                    //Fragment1 f1 = new Fragment1();
                    return Fragment1;
                } else if (arg0 == 1) {
                    //Fragment2 f2 = new Fragment2();
                    return Fragment2;
                } else if (arg0 == 2) {
                    //Fragment3 f3 = new Fragment3();
                    return Fragment3;
                } else if (arg0 == 3) {
                    //Fragment4 f4 = new Fragment4();
                    return Fragment4;
                } else if (arg0 == 4) {
                    //Fragment5 f5 = new Fragment5();
                    return Fragment5;
                }
            }

        } else if (arg0 == 5) {
            //Fragment6 f6 = new Fragment6();
            return Fragment6;
        }

        return null;
    }

    @Override
    public int getCount() {
        return 6;
    }

   /* @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }*/


}
