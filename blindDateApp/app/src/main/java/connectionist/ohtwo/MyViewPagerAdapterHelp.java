package connectionist.ohtwo;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class MyViewPagerAdapterHelp extends FragmentPagerAdapter {

    public Fragment Fragment1Help;
    public Fragment Fragment2Help;
    public Fragment Fragment3Help;

    public MyViewPagerAdapterHelp(FragmentManager fm) {
        super(fm);

        Fragment1Help = new Fragment1Help();
        Fragment2Help = new Fragment2Help();
        Fragment3Help = new Fragment3Help();

        fm.executePendingTransactions();
    }

    @Override
    public Fragment getItem(int arg0) {

        if (arg0 == 0) {
            //Fragment1 f1 = new Fragment1();
            return Fragment1Help;
        } else if (arg0 == 1) {
            //Fragment2 f2 = new Fragment2();
            return Fragment2Help;
        } else if (arg0 == 2) {
            //Fragment3 f3 = new Fragment3();
            return Fragment3Help;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

   /* @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }*/


}
