package connectionist.ohtwo;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class MyViewPagerAdapterMeeting extends FragmentPagerAdapter {

    public Fragment Fragment1Meeting;
    public Fragment Fragment2Meeting;
    public Fragment Fragment3Meeting;
    public Fragment Fragment4Meeting;

    String [] output = new String[5];

    public MyViewPagerAdapterMeeting(FragmentManager fm) {
        super(fm);

        Fragment1Meeting = new Fragment1Meeting();
        Fragment2Meeting = new Fragment2Meeting();
        Fragment3Meeting = new Fragment3Meeting();
        Fragment4Meeting = new Fragment4Meeting();

        fm.executePendingTransactions();
    }

    @Override
    public Fragment getItem(int arg0) {

        if(arg0==0){
            //Fragment1 f1 = new Fragment1();
            return Fragment1Meeting;
        } else if(arg0==1){
            //Fragment2 f2 = new Fragment2();
            return Fragment2Meeting;
        } else if(arg0==2){
            //Fragment3 f3 = new Fragment3();
            return Fragment3Meeting;
        } else if(arg0==3){
            //Fragment4 f4 = new Fragment4();
            return Fragment4Meeting;
        }
        return null;
    }

    @Override
    public int getCount() {
        return currentUser.meeting_join_number;
    }

   /* @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }*/


}
