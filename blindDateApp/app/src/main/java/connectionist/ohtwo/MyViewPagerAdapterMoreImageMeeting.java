package connectionist.ohtwo;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class MyViewPagerAdapterMoreImageMeeting extends FragmentPagerAdapter {

    public Fragment MoreImageFragment1;
    public Fragment MoreImageFragment2;
    public Fragment MoreImageFragment3;

    public MyViewPagerAdapterMoreImageMeeting(FragmentManager fm) {
        super(fm);

        MoreImageFragment1 = new MoreImageFragment1Meeting();
        MoreImageFragment2 = new MoreImageFragment2Meeting();
        MoreImageFragment3 = new MoreImageFragment3Meeting();

        fm.executePendingTransactions();
    }

    @Override
    public Fragment getItem(int arg0) {

        if(arg0==0){
            //Fragment1 f1 = new Fragment1();
            return MoreImageFragment1;
        } else if(arg0==1){
            //Fragment2 f2 = new Fragment2();
            return MoreImageFragment2;
        } else if(arg0==2){
            //Fragment3 f3 = new Fragment3();
            return MoreImageFragment3;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

   /* @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }*/


}
