package connectionist.ohtwo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NoticeActivity extends AppCompatActivity {

    ExpandableListView expandableListView;
    TextView noticeConfirmButton;

    private ArrayList<String> arrayGroup;
    ArrayList<String> arrayChildList[] = new ArrayList[5];

    private HashMap<String,ArrayList<String>> arrayChild;

    JSONObject[] outputJson;
    String[] title;
    String[] notice;
    String[] day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);

        arrayGroup = new ArrayList<String>();
        arrayChild = new HashMap<String, ArrayList<String>>();

        outputJson = new JSONObject[5];
        title = new String[5];
        notice = new String[5];
        day = new String[5];

        callJsp.getNotice();
        parseNotice();

        expandableListView = findViewById(R.id.expandableListView);
        expandableListView.setAdapter(new NoticeAdapter(this, arrayGroup, arrayChild, day));
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;
            @Override
            public void onGroupExpand(int i) {
                if(i != previousGroup)
                    expandableListView.collapseGroup(previousGroup);
                previousGroup = i;
            }
        });

        noticeConfirmButton = findViewById(R.id.noticeConfirmButton);
        noticeConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        /*

        arrayGroup.add("햄버거 매장");
        arrayGroup.add("피자 매장");
        arrayGroup.add("중국집");

        ArrayList<String> arrayHamberger = new ArrayList<String>();
        arrayHamberger.add("맥도날드");
        arrayHamberger.add("버거킹");
        arrayHamberger.add("롯데리아");

        ArrayList<String> arrayPizza = new ArrayList<String>();
        arrayPizza.add("피자헛");
        arrayPizza.add("피자스쿨");
        arrayPizza.add("미스터피자");
        arrayPizza.add("피자에땅");
        arrayPizza.add("피자알볼로");

        ArrayList<String> arrayChinese = new ArrayList<String>();
        arrayChinese.add("청마루");
        arrayChinese.add("차이차이");
        arrayChinese.add("홍콩반점");

        arrayChild.put(arrayGroup.get(0), arrayHamberger);
        arrayChild.put(arrayGroup.get(1), arrayPizza);
        arrayChild.put(arrayGroup.get(2), arrayChinese);*/


    }

    public void parseNotice() {

        try {
            String response = callJsp.response;
            String[] responseArray = response.split("\\},");

            responseArray[0] = responseArray[0] + "}";
            responseArray[1] = responseArray[1] + "}";
            responseArray[2] = responseArray[2] + "}";
            responseArray[3] = responseArray[3] + "}";
            responseArray[4] = responseArray[4] + "}";

            for (int i = 0; i < 5; i++) {
                outputJson[i] = new JSONObject(responseArray[i]);
                title[i] = outputJson[i].getString("title");
                notice[i] = outputJson[i].getString("notice");
                day[i] = outputJson[i].getString("day");

                if(!title[i].equals("")){
                    arrayGroup.add(title[i]);
                    if(!notice[i].equals("")) {
                        arrayChildList[i] = new ArrayList<String>();
                        arrayChildList[i].add(notice[i]);
                        arrayChild.put(arrayGroup.get(i), arrayChildList[i]);
                    }
                }



            }

        } catch (JSONException e) {
            e.printStackTrace();
        }



    }



}

