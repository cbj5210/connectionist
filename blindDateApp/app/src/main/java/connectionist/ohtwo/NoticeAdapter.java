package connectionist.ohtwo;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class NoticeAdapter extends BaseExpandableListAdapter {
    private Context context;
    private ArrayList<String> arrayGroup;
    private HashMap<String, ArrayList<String>> arrayChild;
    private String[] arrayDay;

    public NoticeAdapter(Context context, ArrayList<String> arrayGroup, HashMap<String, ArrayList<String>> arrayChild, String[] arrayDay){
        super();
        this.context = context;
        this.arrayGroup= arrayGroup;
        this.arrayChild= arrayChild;
        this.arrayDay = arrayDay;

    }



    @Override
    public int getGroupCount() {
        return arrayGroup.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if(arrayChild.get( arrayGroup.get( groupPosition ))==null)
            return 0;
        else
            return arrayChild.get( arrayGroup.get( groupPosition )).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return arrayGroup.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return arrayChild.get( arrayGroup.get( groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String groupName = arrayGroup.get(groupPosition);
        String groupDay = arrayDay[groupPosition];
        View v  = convertView;

        if( v == null){
            LayoutInflater inflater=  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = (RelativeLayout)inflater.inflate(R.layout.notice_list_view_item_group, null);
        }
        TextView textGroup = (TextView) v.findViewById(R.id.textGroup);
        textGroup.setText(groupName);

        TextView textGroupDay = (TextView) v.findViewById(R.id.textGroupDay);
        textGroupDay.setText(groupDay);

        ImageView noticeArrowIndicator = v.findViewById(R.id.noticeArrowIndicator);

        RelativeLayout textGroupLayout = v.findViewById(R.id.textGroupLayout);

        if(isExpanded){
            noticeArrowIndicator.setImageResource(R.drawable.notice_close);
            textGroup.setTextColor(Color.parseColor("#ffffff"));
            textGroupDay.setTextColor(Color.parseColor("#ffffff"));
            textGroupLayout.setBackgroundColor(Color.parseColor("#ef8733"));
        } else{
            noticeArrowIndicator.setImageResource(R.drawable.notice_open);
            textGroup.setTextColor(Color.parseColor("#333333"));
            textGroupDay.setTextColor(Color.parseColor("#999999"));
            textGroupLayout.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        String childName = arrayChild.get(arrayGroup.get(groupPosition)).get(childPosition);
        View v  = convertView;


        if( v == null){
            LayoutInflater inflater=  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = (RelativeLayout)inflater.inflate(R.layout.notice_list_view_item_child, null);
        }
        TextView textChild = (TextView) v.findViewById(R.id.textChild);
        textChild.setText(childName);

        return v;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }


}