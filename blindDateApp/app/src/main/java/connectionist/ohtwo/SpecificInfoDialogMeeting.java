package connectionist.ohtwo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class SpecificInfoDialogMeeting extends Activity {

    TextView specificInfoOutputMeeting;
    TextView specificInfoNameMeeting;
    TextView specificInfoAgeMeeting;
    TextView specificInfoSmokeMeeting;
    TextView specificInfoReligionMeeting;
    TextView specificInfoJobMeeting;
    TextView specificInfoLocationMeeting;
    TextView specificInfoPreferMeeting;

    TextView  specificInfoConfirmMeeting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 타이틀바 제거
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 팝업이 올라오면 배경 블러처리
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount = 0.7f;
        getWindow().setAttributes(layoutParams);
        setContentView(R.layout.specific_info_dialog_meeting);

        specificInfoOutputMeeting = findViewById(R.id.specificInfoOutputMeeting);
        specificInfoNameMeeting = findViewById(R.id.specificInfoNameMeeting);
        specificInfoAgeMeeting = findViewById(R.id.specificInfoAgeMeeting);
        specificInfoSmokeMeeting = findViewById(R.id.specificInfoSmokeMeeting);
        specificInfoReligionMeeting = findViewById(R.id.specificInfoReligionMeeting);
        specificInfoJobMeeting = findViewById(R.id.specificInfoJobMeeting);
        specificInfoLocationMeeting = findViewById(R.id.specificInfoLocationMeeting);
        specificInfoPreferMeeting = findViewById(R.id.specificInfoPreferMeeting);

        Intent intent = getIntent();

        String output = intent.getStringExtra("output");
        String name = intent.getStringExtra("name");
        String age = intent.getStringExtra("age");
        String smoke = intent.getStringExtra("smoke");
        String religion = intent.getStringExtra("religion");
        String job = intent.getStringExtra("job");
        String location = intent.getStringExtra("location");
        String prefer = intent.getStringExtra("prefer");

        if(output != null)
            specificInfoOutputMeeting.setText(output);
        if(name != null)
            specificInfoNameMeeting.setText(name);
        if(age != null)
            specificInfoAgeMeeting.setText(age);
        if(smoke != null)
            specificInfoSmokeMeeting.setText(smoke);
        if(religion != null)
            specificInfoReligionMeeting.setText(religion);
        if(job != null)
            specificInfoJobMeeting.setText(job);
        if(location != null)
            specificInfoLocationMeeting.setText(location);
        if(prefer != null)
            specificInfoPreferMeeting.setText(prefer);

        specificInfoConfirmMeeting = findViewById(R.id.specificInfoConfirmMeeting);
        specificInfoConfirmMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                //intent.putExtra("result", "Close Popup");
                setResult(31, intent);

                //액티비티(팝업) 닫기
                finish();
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //바깥레이어 클릭시 안닫히게
        if(event.getAction()== MotionEvent.ACTION_OUTSIDE){
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        //안드로이드 백버튼 막기
        return;
    }

}
