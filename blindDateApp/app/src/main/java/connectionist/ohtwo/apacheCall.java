package connectionist.ohtwo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import android.widget.ImageView;

import java.io.File;

public class apacheCall {

    public static AQuery aq;
    public static String aqUrl;
    public static File aqFile;

    public static void imageUpload(String id, Bitmap bitmap, int selection){

        uploadThread ut = new uploadThread(id, "image", bitmap, selection);
        ut.start();

    }

    public static void imageDownload(String id, final Activity activity){
        aq = new AQuery(activity);
        //http://125.176.119.133:18080/
        aqUrl = "http://125.176.119.133:18080/User/" + id + "/user.jpg";
        aqFile = new File(Environment.getExternalStorageDirectory() + "/.connectionist/" + id + "/user.jpg");

        aq.download(aqUrl, aqFile, new AjaxCallback<File>() {
            @Override
            public void callback(String url, File object, AjaxStatus status) {
                /*if (object != null) {
                    Toast.makeText(activity, "다운로드 성공", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(activity, "다운로드 실패", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

        aqUrl = "http://125.176.119.133:18080/User/" + id + "/user_sub_1.jpg";
        aqFile = new File(Environment.getExternalStorageDirectory() + "/.connectionist/" + id + "/user_sub_1.jpg");

        aq.download(aqUrl, aqFile, new AjaxCallback<File>() {
            @Override
            public void callback(String url, File object, AjaxStatus status) {
                /*if (object != null) {
                    Toast.makeText(activity, "다운로드 성공", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(activity, "다운로드 실패", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

        aqUrl = "http://125.176.119.133:18080/User/" + id + "/user_sub_2.jpg";
        aqFile = new File(Environment.getExternalStorageDirectory() + "/.connectionist/" + id + "/user_sub_2.jpg");

        aq.download(aqUrl, aqFile, new AjaxCallback<File>() {
            @Override
            public void callback(String url, File object, AjaxStatus status) {
                /*if (object != null) {
                    Toast.makeText(activity, "다운로드 성공", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(activity, "다운로드 실패", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

    }

    public static void imageViewDownload(ImageView imageView, String id, final Activity activity){
        aq = new AQuery(activity);
        //http://125.176.119.133:18080/
        aqUrl = "http://125.176.119.133:18080/User/" + id + "/user.jpg";
        aqFile = new File(Environment.getExternalStorageDirectory() + "/.connectionist/" + id + "/user.jpg");

        Glide.with(activity).load(aqUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(imageView);

        aq.download(aqUrl, aqFile, new AjaxCallback<File>() {
            @Override
            public void callback(String url, File object, AjaxStatus status) {
                /*if (object != null) {
                    Toast.makeText(activity, "다운로드 성공", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(activity, "다운로드 실패", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

    }

    public static void imageViewDownloadContext(ImageView imageView, String id, Context context){
        aq = new AQuery(context);
        //http://125.176.119.133:18080/
        aqUrl = "http://125.176.119.133:18080/User/" + id + "/user.jpg";
        aqFile = new File(Environment.getExternalStorageDirectory() + "/.connectionist/" + id + "/user.jpg");

        Glide.with(context).load(aqUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(imageView);

        aq.download(aqUrl, aqFile, new AjaxCallback<File>() {
            @Override
            public void callback(String url, File object, AjaxStatus status) {
                /*if (object != null) {
                    Toast.makeText(activity, "다운로드 성공", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(activity, "다운로드 실패", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

    }

    public static void voiceUpload(String id){

        uploadThread ut = new uploadThread(id, "voice");
        ut.start();

    }

    public static void voiceDownload(String id, final Activity activity){
        aq = new AQuery(activity);
        //http://125.176.119.133:18080/
        aqUrl = "http://125.176.119.133:18080/User/" + id + "/user.3gp";
        aqFile = new File(Environment.getExternalStorageDirectory() + "/.connectionist/" + id + "/user.3gp");

        aq.download(aqUrl, aqFile, new AjaxCallback<File>() {
            @Override
            public void callback(String url, File object, AjaxStatus status) {
                /*if (object != null) {
                    Toast.makeText(activity, "다운로드 성공", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(activity, "다운로드 실패", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

    }

    public static void removeUserData(String id){

        removeThread rt = new removeThread(id);
        rt.start();
    }



}
