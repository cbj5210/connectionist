package connectionist.ohtwo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class callJsp {

    static String id="";
    static String response="";
    static String resultText="";
    static JSONObject outputJson;


    public static boolean setCurrentUserInfo(String tos_id){

        id = tos_id;
        String targetUrl = "http://125.176.119.133:18080/blindDateServer/setCurrentUserInfo.jsp?id="+id;
        //String targetUrl = "http://125.176.119.133:18080/blindDateServer/userInfo.jsp?id=naver12345";
        try {
            run(targetUrl);
            response = response.substring(response.indexOf("[")+1,response.lastIndexOf("]"));


            if(!response.equals("")) {
                outputJson = new JSONObject(response);
                currentUser.signed = outputJson.getString("signed");
                currentUser.name = outputJson.getString("name");
                currentUser.sex = outputJson.getString("sex");
                currentUser.latitude = outputJson.getDouble("latitude");
                currentUser.longitude = outputJson.getDouble("longitude");
                currentUser.location = outputJson.getString("location");
                currentUser.age = outputJson.getString("age");
                currentUser.job = outputJson.getString("job");
                currentUser.smoke = outputJson.getString("smoke");
                currentUser.religion = outputJson.getString("religion");
                currentUser.cash = outputJson.getString("cash");
                currentUser.preferage = outputJson.getString("preferage");
                currentUser.dbPhone = outputJson.getString("phone");
                currentUser.isRefreshItem = outputJson.getString("refreshItem");
                currentUser.hasRefreshItem = ohtwoUtil.calRefresh();
                currentUser.lastLogin = outputJson.getString("lastLogin");
                currentUser.reportCount = outputJson.getString("reportCount");
                currentUser.reportDay = outputJson.getString("reportDay");
                currentUser.serverTime = outputJson.getString("serverTime");
                currentUser.serverTimeMinute = outputJson.getString("serverTimeMinute");

                currentUser.isReported = ohtwoUtil.callReported();
                currentUser.isTodayLogin = ohtwoUtil.calToday();


                return true;
            } else {
                currentUser.signed = "0";
                return false;
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

    }

    public static JSONObject searchUserInfo(String tos_id){

        id = tos_id;
        String targetUrl = "http://125.176.119.133:18080/blindDateServer/setCurrentUserInfo.jsp?id="+id;
        //String targetUrl = "http://125.176.119.133:18080/blindDateServer/userInfo.jsp?id=naver12345";
        try {
            run(targetUrl);
            response = response.substring(response.indexOf("[")+1,response.lastIndexOf("]"));

            if(!response.equals("")) {
                JSONObject outputJson = new JSONObject(response);
                return outputJson;
            } else {
                return null;
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;

    }

    //name sex age dodo city preferage job pr
    public static boolean setDBUserInfo(String action, String tos_id, String name, String sex, String age, double latitude, double longitude, String location, String preferage, String job, String smoke, String religion, String phone){
        id = tos_id;
        String targetUrl="";

        targetUrl= "http://125.176.119.133:18080/blindDateServer/setDBUserInfo.jsp?"+
                    "id="+id+"&"+
                    "name="+name+"&"+
                    "sex="+sex+"&"+
                    "age="+age+"&"+
                    "latitude="+latitude+"&"+
                    "longitude="+longitude+"&"+
                    "location="+location+"&"+
                    "preferage="+preferage+"&"+
                    "job="+job+"&"+
                    "smoke="+smoke+"&"+
                    "religion="+religion+"&"+
                    "phone="+phone;

        if(action.equals("insert")){
            targetUrl= targetUrl + "&action=insert";
        } else if(action.equals("update")){
            targetUrl= targetUrl + "&action=update";
        }

        //String targetUrl = "http://125.176.119.133:18080/blindDateServer/userInfo.jsp?id=naver12345";
        try {
            run(targetUrl);

            if(!response.equals("true")) {
                return true;
            } else {
                return false;
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean removeUserInfo(String tos_id){
        id = tos_id;
        String targetUrl="";

        targetUrl= "http://125.176.119.133:18080/blindDateServer/removeUserInfo.jsp?"+
                "id="+id;

        try {
            run(targetUrl);

            if(!response.equals("true")) {
                return true;
            } else {
                return false;
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static String existPhoneId(String tos_id){

        id = tos_id;
        String targetUrl = "http://125.176.119.133:18080/blindDateServer/existPhoneId.jsp?phone="+id;
        try {
            run(targetUrl);
            if(response.indexOf("[")>-1)
                response = response.substring(response.indexOf("[")+1,response.lastIndexOf("]"));


            if(!response.equals("")) {
                outputJson = new JSONObject(response);
                String id = outputJson.getString("id");
                return id;
            } else {
                return "needJoin";
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
            return "needJoin";
        } catch (JSONException e) {
            e.printStackTrace();
            return "needJoin";
        }

    }

    public static String requestName(String tos_id){

        id = tos_id;
        String targetUrl = "http://125.176.119.133:18080/blindDateServer/requestName.jsp?id="+id;
        try {
            run(targetUrl);
            if(response.indexOf("[")>-1)
                response = response.substring(response.indexOf("[")+1,response.lastIndexOf("]"));


            if(!response.equals("")) {
                outputJson = new JSONObject(response);
                String name = outputJson.getString("name");
                return name;
            } else {
                return "이름없음";
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
            return "이름없음";
        } catch (JSONException e) {
            e.printStackTrace();
            return "이름없음";
        }

    }

    public static void reportUser(String tos_id){
        id = tos_id;
        String targetUrl="";

        targetUrl= "http://125.176.119.133:18080/blindDateServer/reportUser.jsp?"+
                "userId="+id;

        try {
            run(targetUrl);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static JSONObject checkServer(){

        String targetUrl = "http://125.176.119.133:18080/blindDateServer/checkServer.jsp";
        try {
            run(targetUrl);
            if(!response.equals("") && response.indexOf("[")>-1 )
                response = response.substring(response.indexOf("[")+1,response.lastIndexOf("]"));


            if(!response.equals("") && response.indexOf("state")>-1 ) {
                outputJson = new JSONObject(response);
                return outputJson;
            } else {
                return null;
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static void getNotice(){

        String targetUrl = "http://125.176.119.133:18080/blindDateServer/getNotice.jsp";
        try {
            run(targetUrl);
            if(response.indexOf("[")>-1)
                response = response.substring(response.indexOf("[")+1,response.lastIndexOf("]"));

            if(response.indexOf("\\n")>-1){
                response = response.replaceAll("\\\\n", "\n");
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static boolean insertWait2(String id_1, String id_2, double latitude, double longitude, String location, String sex){

        String targetUrl = "http://125.176.119.133:18080/blindDateServer/waitInsert.jsp"+
                "id_1="+id_1+"&"+
                "id_2="+id_2+"&"+
                "id_3="+""+"&"+
                "id_4="+""+"&"+
                "latitude="+latitude+"&"+
                "longitude="+longitude+"&"+
                "location="+location+"&"+
                "sex="+sex+"&"+
                "action="+"two";

        try {

            run(targetUrl);
            //response = response.substring(response.indexOf("[")+1,response.lastIndexOf("]"));

            if(!response.equals("true")) {
                return true;
            } else {
                return false;
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;

    }

    public static boolean insertWait3(String id_1, String id_2, String id_3, double latitude, double longitude, String location, String sex){

        String targetUrl = "http://125.176.119.133:18080/blindDateServer/waitInsert.jsp"+
                "id_1="+id_1+"&"+
                "id_2="+id_2+"&"+
                "id_3="+id_3+"&"+
                "id_4="+""+"&"+
                "latitude="+latitude+"&"+
                "longitude="+longitude+"&"+
                "location="+location+"&"+
                "sex="+sex+"&"+
                "action="+"three";

        try {

            run(targetUrl);
            //response = response.substring(response.indexOf("[")+1,response.lastIndexOf("]"));

            if(!response.equals("true")) {
                return true;
            } else {
                return false;
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;

    }

    public static boolean insertWait4(String id_1, String id_2, String id_3, String id_4, double latitude, double longitude, String location, String sex){


        String targetUrl = "http://125.176.119.133:18080/blindDateServer/waitInsert.jsp"+
                "id_1="+id_1+"&"+
                "id_2="+id_2+"&"+
                "id_3="+id_3+"&"+
                "id_4="+id_4+"&"+
                "latitude="+latitude+"&"+
                "longitude="+longitude+"&"+
                "location="+location+"&"+
                "sex="+sex+"&"+
                "action="+"four";

        try {

            run(targetUrl);
            //response = response.substring(response.indexOf("[")+1,response.lastIndexOf("]"));

            if(!response.equals("true")) {
                return true;
            } else {
                return false;
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;

    }



    public static void run(final String targetUrl) throws InterruptedException {

        Thread t1 = new Thread(new Runnable() {
            public void run() {

                URL url = null;
                HttpURLConnection conn = null;
                InputStream is   = null;
                ByteArrayOutputStream baos = null;

                try {
                    url = new URL(targetUrl);

                    conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Cache-Control", "no-cache");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);
                    conn.connect();

                    int responseCode = conn.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        is = conn.getInputStream();
                        baos = new ByteArrayOutputStream();
                        byte[] byteBuffer = new byte[1024];
                        byte[] byteData = null;
                        int nLength = 0;
                        while ((nLength = is.read(byteBuffer, 0, byteBuffer.length)) != -1) {
                            baos.write(byteBuffer, 0, nLength);
                        }
                        byteData = baos.toByteArray();
                        response = new String(byteData); ////
                        conn.disconnect();

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        t1.start();

        t1.join();


    }

}
