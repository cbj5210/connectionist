package connectionist.ohtwo;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import com.google.gson.Gson;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class chatActivity extends AppCompatActivity {

    private String dstUid;
    private String chatName;

    private ImageView send_button;

    private EditText input_msg;

    private String uid;
    private String chatRoomUid;

    private RecyclerView recyclerView;
    private ImageView chat_out;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM.dd\nHH:mm");

    private UserModel dstUserModel;

    SharedPreferences appConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        currentUser currentUser = new currentUser();
        uid = currentUser.id;  //채팅을 요구 하는 아아디 즉 단말기에 로그인된 UID
        dstUid = getIntent().getStringExtra("otherID"); // 채팅을 당하는 아이디

        //chatName = getIntent().getStringExtra("otherName");
        appConfig = getSharedPreferences("appConfig", MODE_PRIVATE);
        String tempName = appConfig.getString(dstUid, "이름없음");
        chatName = tempName;


        send_button = (ImageView)findViewById(R.id.send_msg);
        input_msg = (EditText)findViewById(R.id.input_msg);
        input_msg.setHorizontallyScrolling(true);

        chat_out = findViewById(R.id.chat_out);

        recyclerView = (RecyclerView)findViewById(R.id.msg_recyclerView);


        chat_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DialogTwoButtonChatOut.class);
                intent.putExtra("contents", "상대방과의 모든 대화가 지워지고\n대화방이 삭제됩니다.\n그래도 진행하시겠습니까?");
                startActivityForResult(intent, 1);


            }
        });


        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChatModel chatModel = new ChatModel();
                chatModel.users.put(uid,true);
                chatModel.users.put(dstUid,true);

                if(chatRoomUid == null){
                    send_button.setEnabled(false);
                    FirebaseDatabase.getInstance().getReference().child("blindDate").push().setValue(chatModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            checkChatRoom();
                        }
                    });

                }else {

                    ChatModel.Message message = new ChatModel.Message();
                    message.uid = uid;

                    //글자수에 따른 개행
                    String tempString = "";
                    tempString = input_msg.getText().toString();
                    StringBuilder b = new StringBuilder(tempString);
                    for (int i = b.length()-1; i > 0; i--) {
                        if(i%16==0){
                            if(b.charAt(i)==' '){
                                b.deleteCharAt(i);
                            }
                            b.insert(i, "\n");
                        }
                    }
                    tempString = b.toString();

                    //message.message = input_msg.getText().toString();
                    message.message = tempString;
                    message.timestamp = ServerValue.TIMESTAMP;

                    if(message.message.length() != 0 && ohtwoUtil.checkWord(message.message)){

                        // 공백 전송 버그 수정
                        FirebaseDatabase.getInstance().getReference().child("blindDate").child(chatRoomUid).child("message").push().setValue(message).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                sendFcm();
                                input_msg.setText("");  // editText 초기화
                            }
                        });
                    }

                }


            }
        });
        checkChatRoom();


    }
    void sendFcm(){

        Gson gson = new Gson();

        NotificationModel notificationModel = new NotificationModel();
        notificationModel.to = dstUserModel.pushToken;
        notificationModel.notification.title = currentUser.name;
        notificationModel.notification.text = input_msg.getText().toString();
        notificationModel.notification.tag = chatRoomUid;
        notificationModel.data.title = currentUser.name;
        notificationModel.data.text = input_msg.getText().toString();
        notificationModel.data.tag = chatRoomUid;

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf8"),gson.toJson(notificationModel));

        Request request = new Request.Builder()
                .header("Content-Type", "application/json")
                .addHeader("Authorization", "key=AIzaSyDmm_tWYRY5v95sO8iYTSpbTV4ERzzzIZc")
                .url("https://fcm.googleapis.com/fcm/send")
                .post(requestBody)
                .build();

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
    }

    void  checkChatRoom(){

        FirebaseDatabase.getInstance().getReference().child("blindDate").orderByChild("users/"+uid).equalTo(true).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot item : dataSnapshot.getChildren()){
                    ChatModel  chatModel = item.getValue(ChatModel.class);
                    if(chatModel.users.containsKey(dstUid) && chatModel.users.size() == 2){
                        chatRoomUid = item.getKey();
                        send_button.setEnabled(true);
                        recyclerView.setLayoutManager(new LinearLayoutManager(chatActivity.this));
                        recyclerView.setAdapter(new RecyclerViewAdapter());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode==11){
            //Toast.makeText(getApplicationContext(), "Two Button Dialog OK", Toast.LENGTH_SHORT).show();

            if(chatRoomUid != null){
                FirebaseDatabase.getInstance().getReference().child("blindDate").child(chatRoomUid).removeValue();

                String reportString = "";
                reportString = data.getStringExtra("report");

                if(reportString.equals("true")){
                    //dstUid
                    callJsp.reportUser(dstUid);
                    Toast.makeText(getApplicationContext(), "정상적으로 상대방을 신고하였습니다.\n확인 후 제재가 가해집니다. 감사합니다.", Toast.LENGTH_LONG).show();
                }

                finish();
            }
            else{
                AlertDialog.Builder builder = new AlertDialog.Builder(chatActivity.this);
                builder.setTitle("나갈 채팅방이 존재하지 않습니다.")
                        .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).show();
            }

        } else if(resultCode==12){
            //Toast.makeText(getApplicationContext(), "Two Button Dialog Cancel", Toast.LENGTH_SHORT).show();
        } else if(resultCode==21){
            //Toast.makeText(getApplicationContext(), "One Button Dialog OK", Toast.LENGTH_SHORT).show();
        } else{
            super.onActivityResult(requestCode, resultCode, data);
        }

    }


    class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

        List<ChatModel.Message> chat_Messages;
        //List<String> keys = new ArrayList<>();
        // 이후 미팅 액티비티에 선택된 키값으로 연결

        String imgFilePath;


        public  RecyclerViewAdapter() {
            chat_Messages = new ArrayList<>();
            
            FirebaseDatabase.getInstance().getReference().child("users").child(dstUid).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    dstUserModel = dataSnapshot.getValue(UserModel.class);
                    //imageUri = Uri.parse("file:///" + Environment.getExternalStorageDirectory() + "/.connectionist/"+dstUid+"/user.jpg");
                    imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/.connectionist/"+dstUid+"/user.jpg";
                    getMessageList();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        private void getMessageList() {

            FirebaseDatabase.getInstance().getReference().child("blindDate").child(chatRoomUid).child("message").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    chat_Messages.clear();

                    for(DataSnapshot item : dataSnapshot.getChildren()){
                        chat_Messages.add(item.getValue(ChatModel.Message.class));
                    }
                    // 메세지 갱신
                    notifyDataSetChanged();
                    recyclerView.scrollToPosition(chat_Messages.size() -1);
                    // 스크롤을 가장 아래로
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message, parent, false);

            return new MessageViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            MessageViewHolder messageViewHolder = ((MessageViewHolder)holder);

            // 내가 보낸 msg
            if(chat_Messages.get(position).uid.equals((uid))){
                messageViewHolder.linearLayout_dst.setVisibility(View.INVISIBLE);
                messageViewHolder.textView_name.setText(""); // 상대방 이름 보이게 구현
                messageViewHolder.textView_msg.setText(chat_Messages.get(position).message);
                messageViewHolder.textView_msg.setBackgroundResource(R.drawable.chat_box);
                messageViewHolder.outerLinearLayout.setGravity(Gravity.RIGHT);
            }
            else {
                /*Glide.with(holder.itemView.getContext())
                        .load(imageUri)
                        .asBitmap()
                        .into(((MessageViewHolder) holder).other_user_img);*/
                Drawable drawable = Drawable.createFromPath(imgFilePath);
                ((MessageViewHolder) holder).other_user_img.setImageDrawable(drawable);

                messageViewHolder.linearLayout_dst.setVisibility(View.VISIBLE);
                messageViewHolder.textView_name.setText(chatName); // 상대방 이름 보이게 구현
                messageViewHolder.textView_msg.setText(chat_Messages.get(position).message);
                messageViewHolder.textView_msg.setBackgroundResource(R.drawable.chat_box);
                messageViewHolder.outerLinearLayout.setGravity(Gravity.LEFT);
            }
            long unixTime = (long) chat_Messages.get(position).timestamp;
            Date date = new Date(unixTime);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
            String time = simpleDateFormat.format(date);
            messageViewHolder.textView_time.setText(time);
        }

        @Override
        public int getItemCount() {
            return chat_Messages.size();
        }

        private class MessageViewHolder extends RecyclerView.ViewHolder {

            public TextView textView_msg;
            public TextView textView_name;
            public TextView textView_time;
            public ImageView other_user_img;
            public LinearLayout linearLayout_dst;
            public LinearLayout outerLinearLayout;

            public MessageViewHolder(View view) {
                super(view);

                textView_msg = (TextView)view.findViewById(R.id.msg_View);
                textView_name = (TextView)view.findViewById(R.id.msg_Name);
                textView_time = (TextView)view.findViewById(R.id.message_time);
                other_user_img = (ImageView)view.findViewById(R.id.other_user_img);
                linearLayout_dst = (LinearLayout)view.findViewById(R.id.msg_LinearLayout);
                outerLinearLayout = (LinearLayout)view.findViewById(R.id.msg_OuterLinearLayout);
            }
        }
    }
}
