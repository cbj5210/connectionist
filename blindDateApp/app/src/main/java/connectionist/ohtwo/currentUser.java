package connectionist.ohtwo;

import java.util.ArrayList;

public class currentUser {

    //db
    public static String id;
    public static String signed;
    public static String name;
    public static String sex;
    public static double latitude;
    public static double longitude;
    public static String location;
    public static String age;
    public static String job;
    public static String smoke;
    public static String religion;
    public static String cash;
    public static String preferage;
    public static String phone;

    public static String isRefreshItem;
    public static boolean hasRefreshItem;

    public static String lastLogin;
    public static boolean isTodayLogin;
    public static String serverTime;
    public static String serverTimeMinute;

    public static String reportCount;
    public static String reportDay;
    public static boolean isReported;

    public static String event;
    public static String message;

    public static boolean hasNotice;
    public static String noticeMessage;



    //temp
    public static String dbPhone;

    //meeting
    public static double meeting_latitude;
    public static double meeting_longitude;
    public static String meeting_location;
    public static int member_Number = 0;
    public static int meeting_join_number = 0;
    public static ArrayList<FriendInfo> meetingSelectedFriend;

    public static boolean isCaptain = false;
    public static boolean isWaiting = false;
    public static boolean isMeeting = false;
    public static boolean isComplete = false;

    public static String meetingRoom = "";


}
