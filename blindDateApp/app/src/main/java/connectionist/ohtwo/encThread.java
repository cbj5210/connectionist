package connectionist.ohtwo;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class encThread extends Thread {

    String action;
    String enc;

    private String jspUtilOne = "rlaguddms";
    private String jspUtilTwo = "chlqudwns";
    private String jspUtilThree = "thdcndrjs";
    private String jspUtilFor = "rladydgns";
    private String jspUtilFive = "dbguseka";

    private JSONObject outputJson;

    public encThread(String action, String enc){
        this.action = action;
        this.enc = enc;
    }

    @Override
    public void run() {

        if(action.equals("check") || action.equals("add") || action.equals("sub") ){
            ohtwoUtil.encThreadReturn = gotoAction();
        } else if(action.equals("refreshitem")){
            ohtwoUtil.encThreadReturn = refreshItem();
        } else{
            ohtwoUtil.encThreadReturn = "false";
        }

    }

    private String gotoAction(){

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("jspUtilOne", jspUtilOne));
        nameValuePairs.add(new BasicNameValuePair("jspUtilTwo", jspUtilTwo));
        nameValuePairs.add(new BasicNameValuePair("jspUtilThree", jspUtilThree));
        nameValuePairs.add(new BasicNameValuePair("jspUtilFor", jspUtilFor));
        nameValuePairs.add(new BasicNameValuePair("jspUtilFive", jspUtilFive));
        nameValuePairs.add(new BasicNameValuePair("userId", currentUser.id));
        nameValuePairs.add(new BasicNameValuePair("action", action));
        nameValuePairs.add(new BasicNameValuePair("enc", enc));

        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost("http://125.176.119.133:18080/blindDatePay/encAction.jsp");

            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = client.execute(post);
            //HttpEntity entity = response.getEntity();

            HttpEntity entityResponse = response.getEntity();
            InputStream is = entityResponse.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"),30);

            String total = "";
            String temp = "";

            while( (temp=reader.readLine())!=null ){
                if(temp != null)
                    total += temp;
            }

            total = total.substring(total.indexOf("[")+1,total.lastIndexOf("]"));

            if(!response.equals("")) {
                outputJson = new JSONObject(total);
                ohtwoUtil.encThreadResult = outputJson.getString("result");
                return outputJson.getString("return");
            } else {
                return "false";
            }

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            /*ohtwoUtil.cashError(currentUser.id, action+enc, "DB_down");
            System.out.println("dbdown!!!!!" );*/
            e.printStackTrace();
        }

        return "false";

    }


    private String refreshItem(){

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("userId", currentUser.id));

        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost("http://125.176.119.133:18080/blindDatePay/refreshItem.jsp");

            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = client.execute(post);
            //HttpEntity entity = response.getEntity();

            HttpEntity entityResponse = response.getEntity();
            InputStream is = entityResponse.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"),30);

            String total = "";
            String temp = "";

            while( (temp=reader.readLine())!=null ){
                if(temp != null)
                    total += temp;
            }

            total = total.substring(total.indexOf("[")+1,total.lastIndexOf("]"));

            if(!response.equals("")) {
                outputJson = new JSONObject(total);
                ohtwoUtil.encThreadResult = outputJson.getString("result");
                return outputJson.getString("return");
            } else {
                return "false";
            }

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "false";
    }


}
