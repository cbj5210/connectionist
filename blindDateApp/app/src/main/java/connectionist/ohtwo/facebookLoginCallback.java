package connectionist.ohtwo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

    public class facebookLoginCallback implements FacebookCallback<LoginResult> {

        // 로그인 성공 시 호출 됩니다. Access Token 발급 성공.
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.d("Callback :: ", "onSuccess");
            requestMe(loginResult.getAccessToken());
        }

        // 로그인 창을 닫을 경우, 호출됩니다.
        @Override
        public void onCancel() {
            Log.d("Callback :: ", "onCancel");
        }

        // 로그인 실패 시에 호출됩니다.
        @Override
        public void onError(FacebookException error) {
            Log.d("Callback :: ", "onError : " + error.getMessage());
        }

        // 사용자 정보 요청
        public void requestMe(AccessToken token) {
            GraphRequest graphRequest = GraphRequest.newMeRequest(token,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.d("facebookResult",object.toString());
                            try {

                                currentUser.id = "facebook"+ object.getString("id");
                                currentUser.name = object.getString("name");

                                //동일 핸드폰 번호로 다른 계정이 있는지 확인
                                String anotherAccount = callJsp.existPhoneId(currentUser.phone);
                                if(!anotherAccount.equals(currentUser.id) && !anotherAccount.equals("needJoin") && !currentUser.phone.equals("+15555215554") && !currentUser.phone.equals("15555215554")) {
                                    Toast.makeText(GlobalApplication.loadingActivity, "이미 다른 소셜 로그인 계정이 있습니다. \n해당 계정을 선택해주세요", Toast.LENGTH_SHORT).show();

                                    LoginManager.getInstance().logOut(); //facebook

                                    Intent intentReturn = new Intent(GlobalApplication.loadingActivity, loginActivity.class);
                                    GlobalApplication.loadingActivity.startActivity(intentReturn);

                                } else{
                                    //없다면 디비에서 값을 읽어옴
                                    boolean ok = callJsp.setCurrentUserInfo(currentUser.id);

                                    if(!ok){
                                        //goto join
                                        Intent intentJoin = new Intent(GlobalApplication.loadingActivity, tosActivity.class);
                                        GlobalApplication.loadingActivity.startActivity(intentJoin);
                                    } else if (!currentUser.signed.equals("1") ) {
                                        //goto join
                                        Intent intentJoin = new Intent(GlobalApplication.getGlobalApplicationContext(), tosActivity.class);
                                        GlobalApplication.getGlobalApplicationContext().startActivity(intentJoin);
                                    } else{
                                        if(currentUser.isReported){
                                            Intent intentMain = new Intent(GlobalApplication.loadingActivity, reportUserActivity.class);
                                            GlobalApplication.loadingActivity.startActivity(intentMain);
                                        } else{
                                            Intent intentMain = new Intent(GlobalApplication.loadingActivity, main_page.class);
                                            //Intent intentMain = new Intent(GlobalApplication.loadingActivity, OpenWaitActivity.class);
                                            intentMain.putExtra("isRefresh","false");
                                            intentMain.putExtra("isRefreshMeeting","false");
                                            GlobalApplication.loadingActivity.startActivity(intentMain);
                                        }
                                    }
                                }
                                /*Intent intent = new Intent(GlobalApplication.loadingActivity, main_page.class);
                                intent.putExtra("email", object.getString("email"));
                                intent.putExtra("name", object.getString("name"));
                                intent.putExtra("id", object.getString("id"));
                                GlobalApplication.loadingActivity.startActivity(intent);*/
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender,birthday");
            graphRequest.setParameters(parameters);
            graphRequest.executeAsync();
        }
    }

