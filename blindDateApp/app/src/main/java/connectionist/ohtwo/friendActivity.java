package connectionist.ohtwo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class friendActivity extends AppCompatActivity{

    public static friendActivity friendActivity;

    private ImageView friendCome;
    TextView friendComeText;

    //친구 요청 걸기
    private EditText fs;
    private ImageView fsb;

    //친구 리스트
    public static RecyclerView friend_recycler_view;
    public static RecyclerView.LayoutManager mLayoutManager;

    public static ArrayList<FriendInfo> FriendInfoArrayList;
    public static FriendListAdapterConfig friendListAdapter;

    TextView FriendConfigConfirmButton;

    static String response="";
    static String targetUrl = "http://125.176.119.133:18080/blindDateServer/friend";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend);
        friendActivity = this;

        friendCome = findViewById(R.id.friendCome);
        friendComeText = findViewById(R.id.friendComeText);

        fs = findViewById(R.id.friendSearch);
        fsb = findViewById(R.id.friendSearchButton);

        final Intent intent = new Intent(getApplicationContext(), friendActivity.class);

        fsb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fsString = fs.getText().toString();
                if (fsString.length() == 11) {
                    try {

                        callJsp.run(targetUrl + "Find.jsp?id=" + currentUser.id + "&newF=" + fsString);
                        response = callJsp.response;

                        if (response.contains("success")) {
                            Toast.makeText(getApplicationContext(), "성공적으로 요청을 보냈습니다", Toast.LENGTH_SHORT).show();
                        } else if (response.contains("fail")) {
                            Toast.makeText(getApplicationContext(), "요청 중 오류가 발생하였습니다 다시 해주세요", Toast.LENGTH_SHORT).show();
                        } else if (response.contains("false")) {
                            Toast.makeText(getApplicationContext(), "사용자를 찾을 수 없습니다.", Toast.LENGTH_SHORT).show();
                        }
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                    finish();
                    startActivity(intent);
                }else {
                    Toast.makeText(getApplicationContext(), "-빼고 11자리 모두입력해주세요", Toast.LENGTH_SHORT).show();
                }
            }
        });

        friendCome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(getApplicationContext(), friendComeActivity.class);
                startActivity(intent2);
            }
        });

        friendComeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(getApplicationContext(), friendComeActivity.class);
                startActivity(intent2);
            }
        });

    //++++++++++++친구리스트+++++++++++++++

        /*JSONObject[] outputJson = new JSONObject[20];
        //final String[] output = new String[20];
        String friendListString = "";

        try {
            callJsp.run(targetUrl+"List.jsp?id="+currentUser.id);
            response = callJsp.response;

            int count = 0; //도착한 친구요청 숫자
            for(int i=0; i<response.length(); i++)
            {
                if(response.charAt(i) == '}') {
                    count++;
                }
            }

            if (count==0){
                friendListString = "현재 친구 없음";
            }else if(count==1){
                response = response.substring(response.indexOf("[") + 1, response.lastIndexOf("]"));
                outputJson[0] = new JSONObject(response);
                friendListString = friendListString + outputJson[0].getString("name");
            }else {
                response = response.substring(response.indexOf("[") + 1, response.lastIndexOf("]"));
                String[] responseArray = response.split("\\},");

                for (int i = 0; i < responseArray.length; i++) {
                    responseArray[i] = responseArray[i] + "}";
                    outputJson[i] = new JSONObject(responseArray[i]);
                    friendListString = friendListString + outputJson[i].getString("name") + '\n';
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }*/


        FriendConfigConfirmButton = findViewById(R.id.FriendConfigConfirmButton);
        FriendConfigConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        friend_recycler_view = findViewById(R.id.friendConfigList);
        friend_recycler_view.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        friend_recycler_view.setLayoutManager(mLayoutManager);

        FriendInfoArrayList = new ArrayList<>();

        loadFriend();

        friendListAdapter = new FriendListAdapterConfig(FriendInfoArrayList);

        friend_recycler_view.setAdapter(friendListAdapter);


    }


    public static void loadFriend(){
        try {
            FriendInfoArrayList.clear();
            callJsp.run("http://125.176.119.133:18080/blindDateServer/friendList.jsp?id="+currentUser.id);
            response = callJsp.response;

            int count = 0; //도착한 친구요청 숫자
            for(int i=0; i<response.length(); i++)
            {
                if(response.charAt(i) == '}') {
                    count++;
                }
            }

            JSONObject[] outputJson = new JSONObject[20];
            String[] outputId = new String[20];
            String[] outputName = new String[20];

            if (count!=0){
                response = response.substring(response.indexOf("[") + 1, response.lastIndexOf("]"));
                String[] responseArray = response.split("\\},");

                for (int i = 0; i < responseArray.length; i++) {
                    responseArray[i] = responseArray[i] + "}";
                    outputJson[i] = new JSONObject(responseArray[i]);
                    outputId[i] = outputJson[i].getString("id");
                    outputName[i] = outputJson[i].getString("name");
                    apacheCall.imageDownload(outputId[i], friendActivity);
                    FriendInfoArrayList.add(new FriendInfo(outputId[i],outputName[i]));
                    main_page.appConfigEditor.putString(outputId[i], outputName[i]);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        main_page.appConfigEditor.apply();
        main_page.appConfigEditor.commit();

        friendListAdapter = new FriendListAdapterConfig(FriendInfoArrayList);
        friend_recycler_view.setAdapter(friendListAdapter);


    }

    /*@Override
    public void onBackPressed() {
        //안드로이드 백버튼 막기

    }*/

}
