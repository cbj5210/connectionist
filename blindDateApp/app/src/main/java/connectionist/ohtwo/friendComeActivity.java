package connectionist.ohtwo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


public class friendComeActivity extends Activity {

    //도착한 친구 요청
    private TextView fc1;
    private TextView fc2;
    private TextView fc3;

    //친구 요청 수락
    private TextView fa1;
    private TextView fa2;
    private TextView fa3;

    //친구 요청 거절
    private TextView fr1;
    private TextView fr2;
    private TextView fr3;


    static String response="";
    static String targetUrl = "http://125.176.119.133:18080/blindDateServer/friend";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 타이틀바 제거
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 팝업이 올라오면 배경 블러처리
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount = 0.7f;
        getWindow().setAttributes(layoutParams);
        setContentView(R.layout.activity_friendcome);

        fc1 = findViewById(R.id.friendCome1);
        fc2 = findViewById(R.id.friendCome2);
        fc3 = findViewById(R.id.friendCome3);
        fa1 = findViewById(R.id.friendAdmit1);
        fa2 = findViewById(R.id.friendAdmit2);
        fa3 = findViewById(R.id.friendAdmit3);
        fr1 = findViewById(R.id.friendReject1);
        fr2 = findViewById(R.id.friendReject2);
        fr3 = findViewById(R.id.friendReject3);

        JSONObject[] outputJson = new JSONObject[3];
        final String[] output = new String[3];

        try {
            callJsp.run(targetUrl+"Open.jsp?id="+currentUser.id);
            response = callJsp.response;

            int count = 0; //도착한 친구요청 숫자
            for(int i=0; i<response.length(); i++)
            {
                if(response.charAt(i) == '}') {
                    count++;
                }
            }

            if (count==0){
                response = "[{\"phone\":\"요청 없음\"},{\"phone\":\"요청 없음\"},{\"phone\":\"요청 없음\"}]";
                fa1.setVisibility(View.INVISIBLE);
                fa2.setVisibility(View.INVISIBLE);
                fa3.setVisibility(View.INVISIBLE);
                fr1.setVisibility(View.INVISIBLE);
                fr2.setVisibility(View.INVISIBLE);
                fr3.setVisibility(View.INVISIBLE);
            }

            response = response.substring(response.indexOf("[")+1,response.lastIndexOf("]"));

            if (count==1){
                response = response + ",{\"phone\":\"요청 없음\"},{\"phone\":\"요청 없음\"}";
                fa2.setVisibility(View.INVISIBLE);
                fa3.setVisibility(View.INVISIBLE);
                fr2.setVisibility(View.INVISIBLE);
                fr3.setVisibility(View.INVISIBLE);
            } else if (count==2){
                response = response + ",{\"phone\":\"요청 없음\"}";

                fa3.setVisibility(View.INVISIBLE);
                fr3.setVisibility(View.INVISIBLE);

            }

            String[] responseArray = response.split("\\},");
            responseArray[0] = responseArray[0] + "}";
            responseArray[1] = responseArray[1] + "}";
            for (int i = 0; i < 3; i++) {
                outputJson[i] = new JSONObject(responseArray[i]);
                output[i] = outputJson[i].getString("phone");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String[] out = new String[3];
        for(int i = 0; i<3; i++){
            if(!output[i].equals("요청 없음")){
                out[i] = "010"+output[i].substring(output[i].length()-8,output[i].length());
            }else{
                out[i] = output[i];
            }
        }

        fc1.setText(out[0]);
        fc2.setText(out[1]);
        fc3.setText(out[2]);

        final Intent intent = new Intent(getApplicationContext(), friendComeActivity.class);

        fa1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!output[0].equals("요청 없음")) {
                    try {

                        callJsp.run(targetUrl + "Accept.jsp?id=" + currentUser.id + "&newF=" + output[0]);
                        response = callJsp.response;
                        if (response.contains("true")) {
                            Toast.makeText(getApplicationContext(), "수락 완료", Toast.LENGTH_SHORT).show();
                        } else if (response.contains("false")) {
                            Toast.makeText(getApplicationContext(), "오류 발생", Toast.LENGTH_SHORT).show();
                        }

                        finish();
                        startActivity(intent);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        fa2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!output[1].equals("요청 없음")) {
                    try {

                        callJsp.run(targetUrl + "Accept.jsp?id=" + currentUser.id + "&newF=" + output[1]);
                        response = callJsp.response;
                        if (response.contains("true")) {
                            Toast.makeText(getApplicationContext(), "수락 완료", Toast.LENGTH_SHORT).show();
                        } else if (response.contains("false")) {
                            Toast.makeText(getApplicationContext(), "오류 발생", Toast.LENGTH_SHORT).show();
                        }

                        finish();
                        startActivity(intent);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        fa3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!output[2].equals("요청 없음")) {
                    try {

                        callJsp.run(targetUrl + "Accept.jsp?id=" + currentUser.id + "&newF=" + output[2]);
                        response = callJsp.response;
                        if (response.contains("true")) {
                            Toast.makeText(getApplicationContext(), "수락 완료", Toast.LENGTH_SHORT).show();
                        } else if (response.contains("false")) {
                            Toast.makeText(getApplicationContext(), "오류 발생", Toast.LENGTH_SHORT).show();
                        }
                        finish();
                        startActivity(intent);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        });

        fr1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!output[0].equals("요청 없음")) {
                    try {
                        callJsp.run(targetUrl + "Reject.jsp?id=" + currentUser.id + "&newF=" + output[0]);
                        response = callJsp.response;
                        if (response.contains("true")) {
                            Toast.makeText(getApplicationContext(), "거절 완료", Toast.LENGTH_SHORT).show();
                        } else if (response.contains("false")) {
                            Toast.makeText(getApplicationContext(), "오류 발생", Toast.LENGTH_SHORT).show();
                        }
                        finish();
                        startActivity(intent);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        fr2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!output[1].equals("요청 없음")) {
                    try {
                        callJsp.run(targetUrl + "Reject.jsp?id=" + currentUser.id + "&newF=" + output[1]);
                        response = callJsp.response;
                        if (response.contains("true")) {
                            Toast.makeText(getApplicationContext(), "거절 완료", Toast.LENGTH_SHORT).show();
                        } else if (response.contains("false")) {
                            Toast.makeText(getApplicationContext(), "오류 발생", Toast.LENGTH_SHORT).show();
                        }
                        finish();
                        startActivity(intent);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        fr3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!output[2].equals("요청 없음")) {
                    try {
                        callJsp.run(targetUrl + "Reject.jsp?id=" + currentUser.id + "&newF=" + output[2]);
                        response = callJsp.response;
                        if (response.contains("true")) {
                            Toast.makeText(getApplicationContext(), "거절 완료", Toast.LENGTH_SHORT).show();
                        } else if (response.contains("false")) {
                            Toast.makeText(getApplicationContext(), "오류 발생", Toast.LENGTH_SHORT).show();
                        }
                        finish();
                        startActivity(intent);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        /*dialogOneButtonConfirm = findViewById(R.id.dialogOneButtonConfirm);
        dialogOneButtonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(getApplicationContext(), friendActivity.class);

                finish();
                startActivity(intent2);
            }
        });*/
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //바깥레이어 클릭시 안닫히게
        if(event.getAction()== MotionEvent.ACTION_OUTSIDE){

        } else{
            friendActivity.loadFriend();
            friendActivity.friendListAdapter.notifyDataSetChanged();
        }
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        //안드로이드 백버튼 막기
        friendActivity.loadFriend();
        friendActivity.friendListAdapter.notifyDataSetChanged();
        finish();
    }

}
