package connectionist.ohtwo;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;
import com.kakao.usermgmt.callback.MeV2ResponseCallback;
import com.kakao.usermgmt.response.MeV2Response;
import com.kakao.util.exception.KakaoException;
import com.kakao.util.helper.log.Logger;

import java.util.ArrayList;
import java.util.List;

public class kakaoSessionCallback implements ISessionCallback {

    // 로그인에 성공한 상태
    @Override
    public void onSessionOpened() {
        requestMe();
    }

    // 로그인에 실패한 상태
    @Override
    public void onSessionOpenFailed(KakaoException exception) {
        Log.e("SessionCallback :: ", "onSessionOpenFailed : " + exception.getMessage());
    }

    // 사용자 정보 요청
    private void requestMe() {
        List<String> keys = new ArrayList<>();
        keys.add("properties.nickname");
        keys.add("properties.profile_image");
        keys.add("kakao_account.email");

        UserManagement.getInstance().me(keys, new MeV2ResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                String message = "failed to get user info. msg=" + errorResult;
                Logger.d(message);
            }


            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                redirectLoginActivity();
            }

            @Override
            public void onSuccess(MeV2Response response) {
                Logger.d("user id : " + response.getId());
                Logger.d("email: " + response.getKakaoAccount().getEmail());
                Logger.d("profile image: " + response.getProfileImagePath());
                gotoMainPage(response);
            }

        });
    }

    private void redirectLoginActivity() {
        /*Intent intent = new Intent(GlobalApplication.getGlobalApplicationContext(), loginActivity.class);
        GlobalApplication.loadingActivity.startActivity(intent);*/
    }

    private void gotoMainPage(MeV2Response response){

        currentUser.id = "kakao"+ response.getId();
        currentUser.name = response.getNickname();

        //동일 핸드폰 번호로 다른 계정이 있는지 확인
        String anotherAccount = callJsp.existPhoneId(currentUser.phone);
        if(!anotherAccount.equals(currentUser.id) && !anotherAccount.equals("needJoin") && !currentUser.phone.equals("+15555215554") && !currentUser.phone.equals("15555215554")) {
            Toast.makeText(GlobalApplication.loadingActivity, "이미 다른 소셜 로그인 계정이 있습니다. \n해당 계정을 선택해주세요", Toast.LENGTH_SHORT).show();

            if (Session.getCurrentSession().checkAndImplicitOpen()) {       //kakao

                UserManagement.getInstance().requestLogout(new LogoutResponseCallback() {
                    @Override
                    public void onCompleteLogout() {

                    }
                });

            }

            Intent intentReturn = new Intent(GlobalApplication.loadingActivity, loginActivity.class);
            GlobalApplication.loadingActivity.startActivity(intentReturn);
        } else {

            //없다면 디비에서 값을 읽어옴
            boolean ok = callJsp.setCurrentUserInfo(currentUser.id);

            if (!ok) {
                //goto join
                Intent intentJoin = new Intent(GlobalApplication.loadingActivity, tosActivity.class);
                GlobalApplication.loadingActivity.startActivity(intentJoin);
            }  else if (!currentUser.signed.equals("1") ) {
                //goto join
                Intent intentJoin = new Intent(GlobalApplication.getGlobalApplicationContext(), tosActivity.class);
                GlobalApplication.getGlobalApplicationContext().startActivity(intentJoin);
            } else {
                if(currentUser.isReported){
                    Intent intentMain = new Intent(GlobalApplication.loadingActivity, reportUserActivity.class);
                    GlobalApplication.loadingActivity.startActivity(intentMain);
                } else{
                    Intent intentMain = new Intent(GlobalApplication.loadingActivity, main_page.class);
                    //Intent intentMain = new Intent(GlobalApplication.loadingActivity, OpenWaitActivity.class);
                    intentMain.putExtra("isRefresh","false");
                    intentMain.putExtra("isRefreshMeeting","false");
                    GlobalApplication.loadingActivity.startActivity(intentMain);
                }

            }

        }

    }

}