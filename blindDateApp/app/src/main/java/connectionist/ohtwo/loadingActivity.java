package connectionist.ohtwo;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.kakao.auth.Session;
import com.nhn.android.naverlogin.OAuthLogin;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;

public class loadingActivity extends AppCompatActivity {

    private int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    //naver
    public static OAuthLogin naverOAuthLoginModule;
    public static String naverToken;

    //kakao
    kakaoSessionCallback callback;

    //facebook
    private facebookLoginCallback mLoginCallback;
    private CallbackManager mCallbackManager;
    public AccessToken FacebookToken;

    GlideDrawableImageViewTarget gifImage;

    ImageView connectionist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        connectionist = findViewById(R.id.connectionist);
        gifImage = new GlideDrawableImageViewTarget(connectionist, 1);
        Glide.with(this).load(R.drawable.connectionist).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(gifImage);

        Context mContext;
        mContext = getApplicationContext();
        getHashKey(mContext);
        GlobalApplication.loadingActivity = this;

        naverOAuthLoginModule = OAuthLogin.getInstance();
        naverOAuthLoginModule.init(this, "PGTH3X0RLceAe4aFrVHU", "gcqqyxz13X", "ConnectionistApp");


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    if (ContextCompat.checkSelfPermission(loadingActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(loadingActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(loadingActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(loadingActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(loadingActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(loadingActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                        //ActivityCompat.shouldShowRequestPermissionRationale(loginActivity.this, Manifest.permission.READ_PHONE_STATE);
                    }
                    else{
                        TelephonyManager phoneMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                        currentUser.phone = phoneMgr.getLine1Number();
                        startLoading();
                    }
                }
            }
        }, 3000);




    }

    private void startLoading() {

        //check network
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ninfo = cm.getActiveNetworkInfo();

        if(ninfo == null){
            Intent intent = new Intent(getApplicationContext(), DialogOneButton.class);
            intent.putExtra("contents", "네트워크가 연결되어 있지 않습니다.\n확인 후 다시 실행해주세요.");
            startActivityForResult(intent, 1);
        } else{

            //check server
            JSONObject serverResponse;
            String appVersion = "0.0.0.9";

            String version="";
            String state="";
            String stateMessage="";

            serverResponse = callJsp.checkServer();

            //Server Down
            if(serverResponse==null){
                Intent intent = new Intent(getApplicationContext(), DialogOneButton.class);
                intent.putExtra("contents", "긴급 점검중 입니다.\n 예상 소요시간 : 1시간");
                startActivityForResult(intent, 1);
            } else{
                try {
                    version = serverResponse.getString("version");
                    state = serverResponse.getString("state");
                    stateMessage = serverResponse.getString("stateMessage");
                    currentUser.event = serverResponse.getString("event");
                    currentUser.message = serverResponse.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Version not match
                if(!appVersion.equals(version)){
                    //need App Update Dialog
                    Intent intent = new Intent(getApplicationContext(), DialogTwoButton.class);
                    intent.putExtra("contents", "최신 버전이 아닙니다.\n업데이트를 진행하시겠습니까?");
                    startActivityForResult(intent, 1);
                //Server Check
                } else if(state.equals("0")){
                    //Server onCheck print stateMessage
                    Intent intent = new Intent(getApplicationContext(), DialogOneButton.class);
                    intent.putExtra("contents", stateMessage);
                    startActivityForResult(intent, 1);

                } else {
                    //naver

                    if(state.equals("2")){
                        currentUser.hasNotice=true;
                        currentUser.noticeMessage=stateMessage;
                    } else{
                        currentUser.hasNotice=false;
                    }

                    naverOAuthLoginModule = OAuthLogin.getInstance();
                    naverOAuthLoginModule.init(this, "PGTH3X0RLceAe4aFrVHU", "gcqqyxz13X", "ConnectionistApp");

                    naverToken = naverOAuthLoginModule.getAccessToken(this);
                    naverProfileTask task = new naverProfileTask(this, naverOAuthLoginModule, this);

                    if (naverToken != null) {
                        task.execute(naverToken);
                        //finish();
                    }

                    //kakao
                    callback = new kakaoSessionCallback();
                    if (Session.getCurrentSession().checkAndImplicitOpen()) {
                        callback.onSessionOpened();
                        //finish();
                    }

                    //facebook
                    //set facebook
                    FacebookToken = AccessToken.getCurrentAccessToken();
                    mLoginCallback = new facebookLoginCallback();

                    if (FacebookToken != null) {
                        mLoginCallback.requestMe(FacebookToken);
                        //finish();
                    }

                    Intent intent = new Intent(getBaseContext(), loginActivity.class);
                    startActivity(intent);
                    finish();
                }

            }

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_CONTACTS) {

            boolean isError = false;

            for(int i=0 ; i<grantResults.length; i++){
                if(grantResults[i] == PackageManager.PERMISSION_DENIED){
                    Toast.makeText(getApplicationContext(), "권한 획득에 실패하였습니다. 앱을 다시 실행하여 모든 권한을 수락해주세요.", Toast.LENGTH_SHORT).show();
                    isError = true;

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finishAffinity();
                            System.runFinalization();
                            System.exit(0);
                        }
                    }, 3000);

                }
            }
            if(!isError) {
                TelephonyManager phoneMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                } else {
                    currentUser.phone = phoneMgr.getLine1Number();
                }
                startLoading();
            }
        }
        // other 'case' lines to check for other
        // permissions this app might request
    }

    @Nullable
    public static String getHashKey(Context context) {
        final String TAG = "KeyHash";
        String keyHash = null;
        try {
            PackageInfo info =
                    context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                keyHash = new String(Base64.encode(md.digest(), 0));
                Log.d(TAG, keyHash);
            }
        } catch (Exception e) {
            Log.e("name not found", e.toString());
        }
        if (keyHash != null) {
            return keyHash;
        } else {
            return null;
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

            if(resultCode==11){

                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=connectionist.ohtwo"));
                startActivity(myIntent);

            } else if(resultCode==12 || resultCode==21){

                finishAffinity();
                System.runFinalization();
                System.exit(0);

            } else{
                super.onActivityResult(requestCode, resultCode, data);
            }

    }

}
