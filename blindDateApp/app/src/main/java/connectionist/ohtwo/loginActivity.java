package connectionist.ohtwo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.fsn.cauly.CaulyAdInfo;
import com.fsn.cauly.CaulyAdInfoBuilder;
import com.fsn.cauly.CaulyCloseAd;
import com.fsn.cauly.CaulyCloseAdListener;
import com.kakao.auth.Session;
import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.ui.view.OAuthLoginButton;

import java.util.Arrays;

public class loginActivity extends AppCompatActivity implements CaulyCloseAdListener {

    private final long FINISH_INTERVAL_TIME = 500;
    private long backPressedTime = 0;

    public static String myNum = "";

    //temp

    //naver
    public static OAuthLoginButton naverOAuthLoginButton;
    OAuthLogin naverOAuthLoginModule;
    ImageView naver_login;

    //kakao
    kakaoSessionCallback callback;
    private com.kakao.usermgmt.LoginButton btn_kakao_login;

    ImageView kakao_login;

    //facebook
    private LoginButton btn_facebook_login;
    private facebookLoginCallback mLoginCallback;
    private CallbackManager mCallbackManager;
    ImageView facebook_login;

    //////////// cauly

    //private static final String APP_CODE = "CAULY"; // 광고 요청 코드 ( 자신의 APP_CODE 쓸 것 )
    private static final String APP_CODE = "cYoXgrqI"; // 광고 요청 코드 ( 자신의 APP_CODE 쓸 것 )

    CaulyCloseAd mCloseAd ;                         // CloseAd광고 객체


    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        CaulyAdInfo closeAdInfo = new CaulyAdInfoBuilder(APP_CODE).build();
        mCloseAd = new CaulyCloseAd();
        mCloseAd.setButtonText("취소", "종료");
        mCloseAd.setDescriptionText("종료하시겠습니까?");
        mCloseAd.setAdInfo(closeAdInfo);
        mCloseAd.setCloseAdListener(this); // CaulyCloseAdListener 등록
        // 종료광고 노출 후 back버튼 사용을 막기 원할 경우 disableBackKey();을 추가한다
        // mCloseAd.disableBackKey();

        //set cell number
        if (ContextCompat.checkSelfPermission(loginActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            TelephonyManager temp = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            myNum = temp.getLine1Number();
        }



        //set Naver

        naverOAuthLoginModule = OAuthLogin.getInstance();
        naverOAuthLoginModule.init(this, "PGTH3X0RLceAe4aFrVHU", "gcqqyxz13X", "ConnectionistApp");

        naver_login = findViewById(R.id.naver_login_button);
        naver_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                naverOAuthLoginModule.startOauthLoginActivity(loginActivity.this, new naverHandler(loginActivity.this, naverOAuthLoginModule, loginActivity.this));
            }
        });

        /*naverOAuthLoginButton = findViewById(R.id.button_naverlogin);
        naverOAuthLoginButton.setOAuthLoginHandler(new naverHandler(this, naverOAuthLoginModule, this));*/

        //set kakao
        callback = new kakaoSessionCallback();
        Session.getCurrentSession().addCallback(callback);

        btn_kakao_login = (com.kakao.usermgmt.LoginButton) findViewById(R.id.btn_kakao_login);

        kakao_login = findViewById(R.id.kakao_login_button);
        kakao_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_kakao_login.performClick();
            }
        });


        //set facebook
        mLoginCallback = new facebookLoginCallback();

        mCallbackManager = CallbackManager.Factory.create();
        btn_facebook_login = (LoginButton) findViewById(R.id.facebook_login);
        btn_facebook_login.setReadPermissions(Arrays.asList("public_profile", "email"));
        btn_facebook_login.registerCallback(mCallbackManager, mLoginCallback);

        facebook_login = findViewById(R.id.facebook_login_button);
        facebook_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_facebook_login.performClick();
            }
        });



    }

    ////cauly

    // Back Key가 눌러졌을 때, CloseAd 호출
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 앱을 처음 설치하여 실행할 때, 필요한 리소스를 다운받았는지 여부.
            if (mCloseAd.isModuleLoaded())
            {
                mCloseAd.show(this);
            }
            else
            {
                // 광고에 필요한 리소스를 한번만  다운받는데 실패했을 때 앱의 종료팝업 구현
                showDefaultClosePopup();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showDefaultClosePopup()
    {
        new android.app.AlertDialog.Builder(this).setTitle("").setMessage("종료 하시겠습니까?")
                .setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        finishAffinity();
                        System.runFinalization();
                        System.exit(0);
                    }
                })
                .setNegativeButton("아니요",null)
                .show();
    }

    // CaulyCloseAdListener
    @Override
    public void onFailedToReceiveCloseAd(CaulyCloseAd ad, int errCode, String errMsg) {


    }
    // CloseAd의 광고를 클릭하여 앱을 벗어났을 경우 호출되는 함수이다.
    @Override
    public void onLeaveCloseAd(CaulyCloseAd ad) {
    }
    // CloseAd의 request()를 호출했을 때, 광고의 여부를 알려주는 함수이다.
    @Override
    public void onReceiveCloseAd(CaulyCloseAd ad, boolean isChargable) {

    }
    //왼쪽 버튼을 클릭 하였을 때, 원하는 작업을 수행하면 된다.
    @Override
    public void onLeftClicked(CaulyCloseAd ad) {

    }
    //오른쪽 버튼을 클릭 하였을 때, 원하는 작업을 수행하면 된다. (종료창에서 예 눌렀을때)
    //Default로는 오른쪽 버튼이 종료로 설정되어있다.
    @Override
    public void onRightClicked(CaulyCloseAd ad) {
        finishAffinity();
        System.runFinalization();
        System.exit(0);
    }
    @Override
    public void onShowedCloseAd(CaulyCloseAd ad, boolean isChargable) {
    }

    ////

    /*@Override
    public void onBackPressed() {
        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;

        if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime)
        {
            finishAffinity();
            System.runFinalization();
            System.exit(0);
        }
        else
        {
            backPressedTime = tempTime;
            Toast.makeText(getApplicationContext(), "\'뒤로\'버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();
        if (mCloseAd != null)
            mCloseAd.resume(this); // 필수 호출
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        }


        }

