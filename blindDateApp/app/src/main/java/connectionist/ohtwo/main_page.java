package connectionist.ohtwo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.fsn.cauly.CaulyAdInfo;
import com.fsn.cauly.CaulyAdInfoBuilder;
import com.fsn.cauly.CaulyCloseAd;
import com.fsn.cauly.CaulyCloseAdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;

import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class main_page extends AppCompatActivity implements RewardedVideoAdListener, CaulyCloseAdListener {

    String currentAction="";
    String uid = currentUser.id;
    String dstRoomKey;

    String checkString ="";
    String inputString = "";
    String waiting_number = "";
    Intent checkIntent;

    UserModel firstUserModel;
    UserModel secondUserModel;
    UserModel thirdUserModel;

    private RewardedVideoAd mRewardedVideoAd;

    //private static final String APP_CODE = "CAULY"; // 광고 요청 코드 ( 자신의 APP_CODE 쓸 것 )
    private static final String APP_CODE = "cYoXgrqI"; // 광고 요청 코드 ( 자신의 APP_CODE 쓸 것 )

    CaulyCloseAd mCloseAd ;                         // CloseAd광고 객체

    private TextView mTextMessage;

    ConnectivityManager cm;
    ConnectivityManager.NetworkCallback ncb;

    private final long FINISH_INTERVAL_TIME = 500;
    private long backPressedTime = 0;

    FrameLayout layout_1;
    FrameLayout layout_2;
    LinearLayout layout_3;

    public static SharedPreferences appConfig;
    public static SharedPreferences.Editor appConfigEditor;

    //layout_1

    public static TextView meetingLocationText;

    //layout_1_1
    LinearLayout layout_1_1;

    public static TextView ohtwoMainSansoMeeting;

    ImageView meetingMapButton;
    ImageView meetingFriendAdminButton;
    ImageView meetingFriendRefreshButton;
    TextView mettingConfirmButton;
    public static TextView meetingNumberText;

    RecyclerView metting_recycler_view;
    RecyclerView.LayoutManager mLayoutManager;

    ArrayList<FriendInfo> FriendInfoArrayList;
    FriendListAdapter friendListAdapter;

    //layout_1_2
    LinearLayout layout_1_2;
    TextView meetingCancelButton;
    ImageView meetingWaitImage;

    //layout_1_3
    FrameLayout layout_1_3;

    public static TextView ohtwoMainSansoMeeting2;

    LinearLayout buttonCollectionMeeting;

    ViewPager viewPagerMeeting = null;

    public String [] outputMeeting = new String[5];
    public static String [] idUriMeeting = new String[5];
    public String [] otherNameMeeting = new String[5];
    public String [] otherAgeMeeting = new String[5];
    public String [] otherSmokeMeeting = new String[5];
    public String [] otherReligionMeeting = new String[5];
    public String [] otherJobMeeting = new String[5];
    public String [] otherLocationMeeting = new String[5];
    public String [] otherPreferMeeting = new String[5];

    public TextView ohtwoMainTextMeeting;
    public TextView ohtwoMainUserNameMeeting;
    public TextView ohtwoMainPageNumMeeting;

    ImageView left_arMeeting;
    ImageView right_arMeeting;

    ImageButton ohtwoMainVoiceButtonMeeting;
    ImageButton ohtwoMainSpecificBottonMeeting;
    ImageButton ohtwoMainMoreImageBottonMeeting;
    ImageButton ohtwoMainChatButtonMeeting;

    MediaPlayer mediaPlayerMeeting;
    boolean togglePlayMeeting = false;

    public static int referenceMoreImageMeeting=0;

    ImageView refreshMeeting; // 리프레시
    ImageView exitMeeting;

    //layout_2

    ImageView whiteLine;

    LinearLayout buttonCollection;

    ViewPager viewPager = null;

    public String [] output = new String[5];
    public static String [] idUri = new String[5];
    public String [] otherName = new String[5];
    public String [] otherAge = new String[5];
    public String [] otherSmoke = new String[5];
    public String [] otherReligion = new String[5];
    public String [] otherJob = new String[5];
    public String [] otherLocation = new String[5];
    public String [] otherPrefer = new String[5];

    public static int referenceMoreImage=0;
    ViewPager moreImagevViewPager;
    MyViewPagerAdapterMoreImage moreImagevAdapter;

    public TextView ohtwoMainText;

    public static TextView ohtwoMainSanso;

    public TextView ohtwoMainUserName;
    public TextView ohtwoMainPageNum;

    ImageView left_ar;
    ImageView right_ar;

    ImageButton ohtwoMainVoiceButton;
    ImageButton ohtwoMainSpecificBotton;
    ImageButton ohtwoMainMoreImageBotton;
    ImageButton ohtwoMainChatButton;

    ImageView refreshButton;

    TextView refreshItemSubText;
    public static TextView refreshItemMoveText;

    public static SpannableString underLineText;

    public static ImageView refreshButton1;
    public static ImageView refreshButton2;

    MediaPlayer mediaPlayer;
    boolean togglePlay = false;

    static String response="";
    static boolean freeChat = true;
    static boolean [] chatOpen = new boolean [] {false,false,false,false,false};

    ImageView chatList; // 대화방 가기 버튼

    String isRefresh = "false";
    String isRefreshMeeting = "false";

    //layout_3

    LinearLayout updateButton;
    LinearLayout cashButton;
    LinearLayout friendButton;
    LinearLayout configButton;

    public static Activity main_page_activity;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_home:
                    layout_1.setVisibility(View.VISIBLE);
                    layout_2.setVisibility(View.INVISIBLE);
                    layout_3.setVisibility(View.INVISIBLE);
                    return true;

                case R.id.navigation_dashboard:
                    layout_1.setVisibility(View.INVISIBLE);
                    layout_2.setVisibility(View.VISIBLE);
                    layout_3.setVisibility(View.INVISIBLE);
                    return true;

                case R.id.navigation_notifications:
                    layout_1.setVisibility(View.INVISIBLE);
                    layout_2.setVisibility(View.INVISIBLE);
                    layout_3.setVisibility(View.VISIBLE);

                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page);

        appConfig = getSharedPreferences("appConfig", MODE_PRIVATE);
        appConfigEditor = appConfig.edit();

        main_page_activity = this;

        checkIntent = new Intent(getApplicationContext(), MeetingTwoButton.class);

        //무료 채팅 가능 여부
        //freeChat = true;

        // 핸드폰 번호가 바뀐 경우, DB 업데이트
        System.out.println("phone : " + currentUser.phone);
        System.out.println("dbphone : " + currentUser.dbPhone);
        if(!currentUser.phone.equals(currentUser.dbPhone) && currentUser.id!=null && !!currentUser.id.equals("null")){
            callJsp.setDBUserInfo("update", currentUser.id, currentUser.name, currentUser.sex, currentUser.age, currentUser.latitude, currentUser.longitude, currentUser.location, currentUser.preferage, currentUser.job, currentUser.smoke, currentUser.religion, currentUser.phone);
        }

        //real app id is : ca-app-pub-1966530831722514~7773187318
        MobileAds.initialize(this, "ca-app-pub-1966530831722514~7773187318");
        //MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");

        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);

        loadRewardedVideoAd();

        //////////// cauly
        CaulyAdInfo closeAdInfo = new CaulyAdInfoBuilder(APP_CODE).build();
        mCloseAd = new CaulyCloseAd();
        mCloseAd.setButtonText("취소", "종료");
        mCloseAd.setDescriptionText("종료하시겠습니까?");
        mCloseAd.setAdInfo(closeAdInfo);
        mCloseAd.setCloseAdListener(this); // CaulyCloseAdListener 등록
        // 종료광고 노출 후 back버튼 사용을 막기 원할 경우 disableBackKey();을 추가한다
        // mCloseAd.disableBackKey();

        layout_1 = findViewById(R.id.layout_1);
        layout_2 = findViewById(R.id.layout_2);
        layout_3 = findViewById(R.id.layout_3);

        layout_1.setVisibility(View.INVISIBLE);
        layout_2.setVisibility(View.VISIBLE);
        layout_3.setVisibility(View.INVISIBLE);

        currentUser.isComplete = appConfig.getBoolean("isComplete", false);
        currentUser.meeting_join_number = appConfig.getInt("meeting_join_number", 0);
        currentUser.isCaptain = appConfig.getBoolean("isCaptain", false);

        /*
        for(int i=0; i<currentUser.meeting_join_number-1; i++){
            currentUser.meetingSelectedFriend = new ArrayList<FriendInfo>();
            FriendInfo a = new FriendInfo("","");
            currentUser.meetingSelectedFriend.add(a);
            currentUser.meetingSelectedFriend.get(i).id = appConfig.getString("myFriendId"+i, "");
        }
        */

        String isPostSetting = "";
        isPostSetting = getIntent().getStringExtra("setting");
        if(isPostSetting!=null){
            if(!isPostSetting.equals("")) {
                layout_1.setVisibility(View.INVISIBLE);
                layout_2.setVisibility(View.INVISIBLE);
                layout_3.setVisibility(View.VISIBLE);
            }
        }

        mTextMessage = findViewById(R.id.message);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_dashboard);


        //network realtime check
        cm = (ConnectivityManager) getApplicationContext()
                .getSystemService( Context.CONNECTIVITY_SERVICE );

        ncb = new ConnectivityManager.NetworkCallback(){
            @Override
            public void onAvailable( Network network )
            {
                //네트워크 연결됨
            }

            @Override
            public void onLost( Network network )
            {
                //네트워크 끊어짐
                currentUser.hasNotice = false;
                Intent intent = new Intent(getApplicationContext(), DialogOneButton.class);
                intent.putExtra("contents", "네트워크 연결이 끊어졌습니다.\n확인 후 이용해 주세요.");
                startActivity(intent);

            }
        };

        NetworkRequest.Builder builder = new NetworkRequest.Builder();

        cm.registerNetworkCallback(
                builder.build(),
                ncb);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(main_page_activity, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String uid = currentUser.id;
                Map<String,Object> map = new HashMap<>();
                String newToken = instanceIdResult.getToken();
                map.put("pushToken", newToken);

                FirebaseDatabase.getInstance().getReference().child("users").child(uid).updateChildren(map);
            }
        });
        /*----------------------------------------------------------------------------------------------------*/
        /*----------------------------------------------Layout 1----------------------------------------------*/
        /*----------------------------------------------onCreate----------------------------------------------*/
        /*----------------------------------------------------------------------------------------------------*/

        FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                UserModel userModel = dataSnapshot.getValue(UserModel.class);

                if(userModel.isCaptain == true && userModel.isWaiting == true && userModel.isMeeting == false){

                    currentUser.isCaptain=true;
                    appConfigEditor.putBoolean("isCaptain", currentUser.isCaptain);

                    //대장이 매칭 시작한 경우 레이아웃
                    layout_1_1.setVisibility(View.INVISIBLE);
                    layout_1_2.setVisibility(View.VISIBLE);
                    layout_1_3.setVisibility(View.INVISIBLE);
                    appConfigEditor.putInt("meetingPageNum", 2);
                    appConfigEditor.apply();
                    appConfigEditor.commit();

                    JSONObject[] outputMeetingJson = new JSONObject[4];
                    try {
                        String meetingUrl = "http://125.176.119.133:18080/blindDateServer/getMeetingUser.jsp?id=" + currentUser.id + "&sex=" + currentUser.sex + "&lat=" + currentUser.meeting_latitude + "&lon=" + currentUser.meeting_longitude + "&mee=" + currentUser.meeting_join_number;
                        callJsp.run(meetingUrl);
                        response = callJsp.response;
                        int count = 0; //도착한 상대방 수
                        for(int i=0; i<response.length(); i++)
                        {
                            if(response.charAt(i) == '}') {
                                count++;
                            }
                        }
                        if (count==0){
                            response = "[{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"}]";
                        }
                        response = response.substring(response.indexOf("[") + 1, response.lastIndexOf("]"));
                        if(count==1){
                            response = response +",{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"}";
                        }
                        if(count==2){
                            response = response +",{\"id\":\"\"},{\"id\":\"\"}";
                        }
                        if(count==3){
                            response = response +",{\"id\":\"\"}";
                        }
                        String[] meetingResponseArray = response.split("\\},");
                        meetingResponseArray[0] = meetingResponseArray[0] + "}";
                        meetingResponseArray[1] = meetingResponseArray[1] + "}";
                        meetingResponseArray[2] = meetingResponseArray[2] + "}";

                        for (int i = 0; i < 4; i++) {
                            outputMeetingJson[i] = new JSONObject(meetingResponseArray[i]);
                            idUriMeeting[i] = outputMeetingJson[i].getString("id");
                            apacheCall.imageDownload(idUriMeeting[i],main_page_activity);

                            if (idUriMeeting[i].equals("")) {
                                outputMeeting[i] = "찾을 수 없음";
                                otherNameMeeting[i] = "상대방이 더 없습니다";
                            } else{
                                outputMeeting[i] = outputMeetingJson[i].getString("name") + "(" + outputMeetingJson[i].getString("age") + ")";
                                otherNameMeeting[i] = outputMeetingJson[i].getString("name");
                                otherAgeMeeting[i] = outputMeetingJson[i].getString("age");
                                otherSmokeMeeting[i] = outputMeetingJson[i].getString("smoke");
                                otherReligionMeeting[i] = outputMeetingJson[i].getString("religion");
                                otherJobMeeting[i] = outputMeetingJson[i].getString("job");
                                otherLocationMeeting[i] = outputMeetingJson[i].getString("location");
                                otherPreferMeeting[i] = outputMeetingJson[i].getString("preferage");
                                saveAnotherUserMeeting();
                                setCaptainMeeting(currentUser.id);
                                if(i==0)
                                    ohtwoUtil.subOne();
                            }
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if(userModel.isCaptain == false && userModel.isWaiting == true && userModel.isMeeting == false){
                    //대장이외 미팅 경우 레이아웃

                    currentUser.isCaptain=false;
                    appConfigEditor.putBoolean("isCaptain", currentUser.isCaptain);

                    layout_1_1.setVisibility(View.INVISIBLE);
                    layout_1_2.setVisibility(View.VISIBLE);
                    layout_1_3.setVisibility(View.INVISIBLE);
                    appConfigEditor.putInt("meetingPageNum", 2);
                    appConfigEditor.apply();
                    appConfigEditor.commit();
                }
                else if(userModel.isCaptain == true && userModel.isWaiting == false && userModel.isMeeting == true){
                    //미팅 성사시 작업 captain 일경우 레이아웃

                    currentUser.isCaptain=true;
                    appConfigEditor.putBoolean("isCaptain", currentUser.isCaptain);
                    appConfigEditor.apply();
                    appConfigEditor.commit();

                    if(currentUser.sex.equals("남자")) {
                        layout_1_1.setVisibility(View.INVISIBLE);
                        layout_1_2.setVisibility(View.INVISIBLE);
                        layout_1_3.setVisibility(View.VISIBLE);
                        appConfigEditor.putInt("meetingPageNum", 3);
                        appConfigEditor.apply();
                        appConfigEditor.commit();

                        refreshMeeting.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                boolean a = false;
                                try {
                                    a = ohtwoUtil.checkOne();
                                    if (a) {
                                        Intent intent = new Intent(getApplicationContext(), MeetingTwoButton.class);
                                        intent.putExtra("contents", "1산소로 새로고침할 수 있습니다.\n진행 하시겠습니까?");
                                        currentAction = "gotoWait";
                                        startActivityForResult(intent, 1);
                                    } else {
                                        Intent intent = new Intent(getApplicationContext(), MeetingTwoButton.class);
                                        intent.putExtra("contents", "산소가 부족합니다.\n충전 페이지로 이동하시겠습니까?");
                                        currentAction = "gotoCache";
                                        startActivityForResult(intent, 1);
                                    }
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                        });
                    }if(currentUser.sex.equals("여자")) {

                        currentUser.isComplete = true;

                        JSONObject[] outputMeetingJson = new JSONObject[4];
                        try {
                            String meetingUrl = "http://125.176.119.133:18080/blindDateServer/getSpecificMeetingUser.jsp?id=" + currentUser.id;
                            callJsp.run(meetingUrl);
                            response = callJsp.response;
                            int count = 0; //도착한 상대방 수
                            for(int i=0; i<response.length(); i++)
                            {
                                if(response.charAt(i) == '}') {
                                    count++;
                                }
                            }
                            if (count==0){
                                response = "[{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"}]";
                            }
                            response = response.substring(response.indexOf("[") + 1, response.lastIndexOf("]"));
                            if(count==1){
                                response = response +",{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"}";
                            }
                            if(count==2){
                                response = response +",{\"id\":\"\"},{\"id\":\"\"}";
                            }
                            if(count==3){
                                response = response +",{\"id\":\"\"}";
                            }
                            String[] meetingResponseArray = response.split("\\},");
                            meetingResponseArray[0] = meetingResponseArray[0] + "}";
                            meetingResponseArray[1] = meetingResponseArray[1] + "}";
                            meetingResponseArray[2] = meetingResponseArray[2] + "}";

                            for (int i = 0; i < 4; i++) {
                                outputMeetingJson[i] = new JSONObject(meetingResponseArray[i]);
                                idUriMeeting[i] = outputMeetingJson[i].getString("id");
                                apacheCall.imageDownload(idUriMeeting[i],main_page_activity);

                                if (idUriMeeting[i].equals("")) {
                                    outputMeeting[i] = "찾을 수 없음";
                                    otherNameMeeting[i] = "상대방이 더 없습니다";
                                } else{
                                    outputMeeting[i] = outputMeetingJson[i].getString("name") + "(" + outputMeetingJson[i].getString("age") + ")";
                                    otherNameMeeting[i] = outputMeetingJson[i].getString("name");
                                    otherAgeMeeting[i] = outputMeetingJson[i].getString("age");
                                    otherSmokeMeeting[i] = outputMeetingJson[i].getString("smoke");
                                    otherReligionMeeting[i] = outputMeetingJson[i].getString("religion");
                                    otherJobMeeting[i] = outputMeetingJson[i].getString("job");
                                    otherLocationMeeting[i] = outputMeetingJson[i].getString("location");
                                    otherPreferMeeting[i] = outputMeetingJson[i].getString("preferage");
                                    saveAnotherUserMeeting();
                                    //setOtherMeeting(currentUser.id);
                                    appConfigEditor.putInt("meeting_join_number", count);
                                }
                            }


                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        layout_1_1.setVisibility(View.INVISIBLE);
                        layout_1_2.setVisibility(View.INVISIBLE);
                        layout_1_3.setVisibility(View.VISIBLE);
                        appConfigEditor.putInt("meetingPageNum", 3);
                        appConfigEditor.apply();
                        appConfigEditor.commit();
                    }


                    MyViewPagerAdapterMeeting adapterMeeting = new MyViewPagerAdapterMeeting(getSupportFragmentManager());
                    getFragmentManager().beginTransaction().commitNow();

                    adapterMeeting.notifyDataSetChanged();
                    viewPagerMeeting.setAdapter(adapterMeeting);

                    ohtwoMainUserNameMeeting.setText(outputMeeting[0]);
                    ohtwoMainPageNumMeeting.setText("1/" + Integer.toString(currentUser.meeting_join_number));
                    referenceMoreImageMeeting = 0;


                }
                else if(userModel.isCaptain == false && userModel.isWaiting == false && userModel.isMeeting == true){
                    //미팅 성사시 작업 captain 아닐경우 레이아웃

                    currentUser.isCaptain=false;
                    appConfigEditor.putBoolean("isCaptain", currentUser.isCaptain);

                    layout_1_1.setVisibility(View.INVISIBLE);
                    layout_1_2.setVisibility(View.INVISIBLE);
                    layout_1_3.setVisibility(View.VISIBLE);

                    currentUser.isComplete = true;

                    appConfigEditor.putBoolean("isComplete", true);
                    appConfigEditor.putInt("meetingPageNum", 3);
                    appConfigEditor.apply();
                    appConfigEditor.commit();


                    JSONObject[] outputMeetingJson = new JSONObject[4];
                    try {
                        String meetingUrl = "http://125.176.119.133:18080/blindDateServer/getSpecificMeetingUser.jsp?id=" + currentUser.id;
                        callJsp.run(meetingUrl);
                        response = callJsp.response;
                        int count = 0; //도착한 상대방 수
                        for(int i=0; i<response.length(); i++)
                        {
                            if(response.charAt(i) == '}') {
                                count++;
                            }
                        }
                        if (count==0){
                            response = "[{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"}]";
                        }
                        response = response.substring(response.indexOf("[") + 1, response.lastIndexOf("]"));
                        if(count==1){
                            response = response +",{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"}";
                        }
                        if(count==2){
                            response = response +",{\"id\":\"\"},{\"id\":\"\"}";
                        }
                        if(count==3){
                            response = response +",{\"id\":\"\"}";
                        }
                        String[] meetingResponseArray = response.split("\\},");
                        meetingResponseArray[0] = meetingResponseArray[0] + "}";
                        meetingResponseArray[1] = meetingResponseArray[1] + "}";
                        meetingResponseArray[2] = meetingResponseArray[2] + "}";

                        for (int i = 0; i < 4; i++) {
                            outputMeetingJson[i] = new JSONObject(meetingResponseArray[i]);
                            idUriMeeting[i] = outputMeetingJson[i].getString("id");
                            apacheCall.imageDownload(idUriMeeting[i],main_page_activity);

                            if (idUriMeeting[i].equals("")) {
                                outputMeeting[i] = "찾을 수 없음";
                                otherNameMeeting[i] = "상대방이 더 없습니다";
                            } else{
                                outputMeeting[i] = outputMeetingJson[i].getString("name") + "(" + outputMeetingJson[i].getString("age") + ")";
                                otherNameMeeting[i] = outputMeetingJson[i].getString("name");
                                otherAgeMeeting[i] = outputMeetingJson[i].getString("age");
                                otherSmokeMeeting[i] = outputMeetingJson[i].getString("smoke");
                                otherReligionMeeting[i] = outputMeetingJson[i].getString("religion");
                                otherJobMeeting[i] = outputMeetingJson[i].getString("job");
                                otherLocationMeeting[i] = outputMeetingJson[i].getString("location");
                                otherPreferMeeting[i] = outputMeetingJson[i].getString("preferage");
                                saveAnotherUserMeeting();
                                //setOtherMeeting(currentUser.id);
                                appConfigEditor.putInt("meeting_join_number", count);
                            }
                        }

                        MyViewPagerAdapterMeeting adapterMeeting = new MyViewPagerAdapterMeeting(getSupportFragmentManager());
                        getFragmentManager().beginTransaction().commitNow();

                        adapterMeeting.notifyDataSetChanged();
                        viewPagerMeeting.setAdapter(adapterMeeting);

                        ohtwoMainUserNameMeeting.setText(outputMeeting[0]);
                        ohtwoMainPageNumMeeting.setText("1/" + Integer.toString(currentUser.meeting_join_number));
                        referenceMoreImageMeeting = 0;

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    layout_1_1.setVisibility(View.INVISIBLE);
                    layout_1_2.setVisibility(View.INVISIBLE);
                    layout_1_3.setVisibility(View.VISIBLE);
                    appConfigEditor.putInt("meetingPageNum", 3);
                    appConfigEditor.apply();
                    appConfigEditor.commit();
                }
                else if (userModel.isCaptain == false && userModel.isWaiting == false && userModel.isMeeting == false){
                    //미팅 진행중이 아닐때 레이아웃
                    currentUser.isComplete = false;
                    appConfigEditor.putBoolean("isCaptain", currentUser.isCaptain);
                    appConfigEditor.putBoolean("isComplete", false);
                    appConfigEditor.apply();
                    appConfigEditor.commit();

                    currentUser.isCaptain=false;

                    layout_1_1.setVisibility(View.VISIBLE);
                    layout_1_2.setVisibility(View.INVISIBLE);
                    layout_1_3.setVisibility(View.INVISIBLE);

                    int a =  appConfig.getInt("meetingPageNum", 1);

                    if((a == 2)||( a== 3)){
                        Intent intent = new Intent(getApplicationContext(), MeetingOneButton.class);
                        intent.putExtra("contents", "누군가에 의해 \n 미팅이 취소되었습니다.");
                        startActivity(intent);
                    }

                    appConfigEditor.putInt("meetingPageNum", 1);
                    appConfigEditor.apply();
                    appConfigEditor.commit();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        layout_1_1 = findViewById(R.id.layout_1_1);
        layout_1_2 = findViewById(R.id.layout_1_2);
        layout_1_3 = findViewById(R.id.layout_1_3);

        int meetingPageNum = appConfig.getInt("meetingPageNum",1);
        currentUser.meeting_join_number = appConfig.getInt("meeting_join_number",0);

        if(meetingPageNum==1){
            layout_1_1.setVisibility(View.VISIBLE);
            layout_1_2.setVisibility(View.INVISIBLE);
            layout_1_3.setVisibility(View.INVISIBLE);
        } else if(meetingPageNum==2){
            layout_1_1.setVisibility(View.INVISIBLE);
            layout_1_2.setVisibility(View.VISIBLE);
            layout_1_3.setVisibility(View.INVISIBLE);
        } else if(meetingPageNum==3){
            getAnotherUserMeeting();
            layout_1_1.setVisibility(View.INVISIBLE);
            layout_1_2.setVisibility(View.INVISIBLE);
            layout_1_3.setVisibility(View.VISIBLE);

        } else{
            layout_1_1.setVisibility(View.VISIBLE);
            layout_1_2.setVisibility(View.INVISIBLE);
            layout_1_3.setVisibility(View.INVISIBLE);
        }


        /* Layout 1_1 */


        ohtwoMainSansoMeeting = findViewById(R.id.ohtwoMainSansoMeeting);
        ohtwoMainSansoMeeting.setText("산소 " + currentUser.cash + "+");
        ohtwoMainSansoMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CashActivity.class);
                startActivity(intent);
            }
        });

        meetingLocationText = findViewById(R.id.meetingLocationText);

        meetingMapButton = (ImageView) findViewById(R.id.meetingMapButton);
        meetingMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), MapsActivity_meeting.class);
                startActivity(intent);
            }
        });

        meetingFriendAdminButton = (ImageView) findViewById(R.id.meetingFriendAdminButton);
        meetingFriendAdminButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), friendActivity.class);
                startActivity(intent);
            }
        });

        meetingFriendRefreshButton = (ImageView) findViewById(R.id.meetingFriendRefreshButton);
        meetingFriendRefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loadFriend();
                friendListAdapter.notifyDataSetChanged();

            }
        });

        meetingNumberText = findViewById(R.id.meetingNumberText);
        ///////recycler_view///////

        metting_recycler_view = findViewById(R.id.metting_recycler_view);
        metting_recycler_view.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        metting_recycler_view.setLayoutManager(mLayoutManager);

        FriendInfoArrayList = new ArrayList<>();

        loadFriend();

        friendListAdapter = new FriendListAdapter(FriendInfoArrayList);

        metting_recycler_view.setAdapter(friendListAdapter);

        /////////////////////////

        mettingConfirmButton = (TextView) findViewById(R.id.mettingConfirmButton);
        mettingConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(currentUser.meeting_location==null || currentUser.meeting_location.equals("")){
                    Toast.makeText(getApplicationContext(), "지역을 선택해주세요.", Toast.LENGTH_SHORT).show();
                } else if(currentUser.member_Number == 0){
                    Toast.makeText(getApplicationContext(), "함께할 친구를 선택해주세요.", Toast.LENGTH_SHORT).show();
                }else{
                    currentUser.meetingSelectedFriend = new ArrayList<FriendInfo>();

                    mettingConfirmButton.setEnabled(false);
                    //String temp="";
                    for (int iter = 0; iter < FriendInfoArrayList.size(); iter++) {
                        if (FriendInfoArrayList.get(iter).selected) {
                            //temp = temp + FriendInfoArrayList.get(iter).name +" ";
                            currentUser.meetingSelectedFriend.add(FriendInfoArrayList.get(iter));

                        }
                    }
                    for (int i = 0; i<currentUser.meetingSelectedFriend.size(); i++ ) {
                        appConfigEditor.putString("myFriendId" + i, currentUser.meetingSelectedFriend.get(i).id);
                        appConfigEditor.apply();
                        appConfigEditor.commit();
                    }

                    currentUser.meeting_join_number = currentUser.member_Number + 1;
                    appConfigEditor.putInt("meeting_join_number", currentUser.meeting_join_number);
                    appConfigEditor.apply();
                    appConfigEditor.commit();







                    try{

                        if(currentUser.sex.equals("남자")){
                            boolean a = ohtwoUtil.checkOne();

                            String meetingUrl = "http://125.176.119.133:18080/blindDateServer/getMeetingUser.jsp?id=" + currentUser.id + "&sex=" + currentUser.sex + "&lat=" + currentUser.meeting_latitude + "&lon=" + currentUser.meeting_longitude + "&mee=" + currentUser.meeting_join_number;
                            callJsp.run(meetingUrl);
                            response = callJsp.response;
                            waiting_number = response.substring(response.lastIndexOf("]")+1,response.length());

                            if(!a){
                                inputString = "산소가 부족합니다 충전하러 가시겠습니까?";
                                checkIntent.putExtra("contents", inputString);
                                currentAction = "gotoCache";
                                startActivityForResult(checkIntent, 1);

                                //대기팀이 0팀이다.
                            }else if(waiting_number.equals("0")){
                                inputString = currentUser.meeting_join_number + "대" + currentUser.meeting_join_number + "입니다" + "\n대기팀" + waiting_number + "팀 있습니다 \n 지역을 바꿔주세요";
                                checkIntent.putExtra("contents", inputString);

                                currentAction = "meetingBack";
                                startActivityForResult(checkIntent, 1);

                                // 대기팀이 1팀이상 있다.
                            }else{
                                if(currentUser.meeting_join_number == 2) {

                                    String firstID = currentUser.meetingSelectedFriend.get(0).id;

                                    FirebaseDatabase.getInstance().getReference().child("users").child(firstID).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            firstUserModel = dataSnapshot.getValue(UserModel.class);

                                            if (firstUserModel.isWaiting || firstUserModel.isMeeting) {
                                                checkString += currentUser.meetingSelectedFriend.get(0).name + "님은 이미 미팅에 참가중입니다";
                                            }

                                            if(checkString.equals("")){
                                                inputString = currentUser.meeting_join_number + "대" + currentUser.meeting_join_number + "입니다" + "\n 1산소가 소모됩니다" + "\n대기팀" + waiting_number + "팀 있습니다";
                                                checkIntent.putExtra("contents", inputString);
                                                currentAction = "meetingNext";
                                                startActivityForResult(checkIntent, 1);
                                            }else{
                                                checkIntent.putExtra("contents", checkString);
                                                currentAction = "meetingBack";
                                                startActivityForResult(checkIntent, 1);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                                    checkString = "";



                                }else if(currentUser.meeting_join_number == 3) {

                                    String firstID = currentUser.meetingSelectedFriend.get(0).id;
                                    String secondID = currentUser.meetingSelectedFriend.get(1).id;

                                    FirebaseDatabase.getInstance().getReference().child("users").child(firstID).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            firstUserModel = dataSnapshot.getValue(UserModel.class);

                                            if (firstUserModel.isWaiting || firstUserModel.isMeeting) {
                                                checkString += currentUser.meetingSelectedFriend.get(0).name + "님은 이미 미팅에 참가중입니다\n";
                                            }

                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });


                                    FirebaseDatabase.getInstance().getReference().child("users").child(secondID).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            secondUserModel = dataSnapshot.getValue(UserModel.class);

                                            if (secondUserModel.isWaiting || secondUserModel.isMeeting) {
                                                checkString += currentUser.meetingSelectedFriend.get(1).name + "님은 이미 미팅에 참가중입니다\n";
                                            }

                                            if(checkString.equals("")){
                                                inputString = currentUser.meeting_join_number + "대" + currentUser.meeting_join_number + "입니다" + "\n 1산소가 소모됩니다" + "\n대기팀" + waiting_number + "팀 있습니다";
                                                checkIntent.putExtra("contents", inputString);
                                                currentAction = "meetingNext";
                                                startActivityForResult(checkIntent, 1);
                                            }else{
                                                checkIntent.putExtra("contents", checkString);
                                                currentAction = "meetingBack";
                                                startActivityForResult(checkIntent, 1);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                                    checkString = "";

                                }else if(currentUser.meeting_join_number == 4) {
                                    String firstID = currentUser.meetingSelectedFriend.get(0).id;
                                    String secondID = currentUser.meetingSelectedFriend.get(1).id;
                                    String thirdID = currentUser.meetingSelectedFriend.get(2).id;

                                    FirebaseDatabase.getInstance().getReference().child("users").child(firstID).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            firstUserModel = dataSnapshot.getValue(UserModel.class);

                                            if (firstUserModel.isWaiting || firstUserModel.isMeeting) {
                                                checkString += currentUser.meetingSelectedFriend.get(0).name + "님은 이미 미팅에 참가중입니다\n";
                                            }

                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                                    checkString = "";

                                    FirebaseDatabase.getInstance().getReference().child("users").child(secondID).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            secondUserModel = dataSnapshot.getValue(UserModel.class);

                                            if (secondUserModel.isWaiting || secondUserModel.isMeeting) {
                                                checkString += currentUser.meetingSelectedFriend.get(1).name + "님은 이미 미팅에 참가중입니다\n";
                                            }

                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                                    checkString = "";

                                    FirebaseDatabase.getInstance().getReference().child("users").child(thirdID).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            thirdUserModel = dataSnapshot.getValue(UserModel.class);

                                            if (thirdUserModel.isWaiting || thirdUserModel.isMeeting) {
                                                checkString += currentUser.meetingSelectedFriend.get(0).name + "님은 이미 미팅에 참가중입니다\n";
                                            }

                                            if(checkString.equals("")){
                                                inputString = currentUser.meeting_join_number + "대" + currentUser.meeting_join_number + "입니다" + "\n 1산소가 소모됩니다" + "\n대기팀" + waiting_number + "팀 있습니다";
                                                checkIntent.putExtra("contents", inputString);
                                                currentAction = "meetingNext";
                                                startActivityForResult(checkIntent, 1);
                                            }else{
                                                checkIntent.putExtra("contents", checkString);
                                                currentAction = "meetingBack";
                                                startActivityForResult(checkIntent, 1);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                                    checkString = "";
                                }


                            }
                        } else { //여자인 경우
                            if(currentUser.meeting_join_number == 2) {

                                String firstID = currentUser.meetingSelectedFriend.get(0).id;

                                FirebaseDatabase.getInstance().getReference().child("users").child(firstID).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        firstUserModel = dataSnapshot.getValue(UserModel.class);

                                        if (firstUserModel.isWaiting || firstUserModel.isMeeting) {
                                            checkString += currentUser.meetingSelectedFriend.get(0).name + "님은 이미 미팅에 참가중입니다";
                                        }

                                        if(checkString.equals("")){
                                            inputString = currentUser.meeting_join_number + "대" + currentUser.meeting_join_number + "입니다";
                                            checkIntent.putExtra("contents", inputString);
                                            currentAction = "meetingNext";
                                            startActivityForResult(checkIntent, 1);
                                        }else{
                                            checkIntent.putExtra("contents", checkString);
                                            currentAction = "meetingBack";
                                            startActivityForResult(checkIntent, 1);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                                checkString = "";



                            }else if(currentUser.meeting_join_number == 3) {

                                String firstID = currentUser.meetingSelectedFriend.get(0).id;
                                String secondID = currentUser.meetingSelectedFriend.get(1).id;

                                FirebaseDatabase.getInstance().getReference().child("users").child(firstID).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        firstUserModel = dataSnapshot.getValue(UserModel.class);

                                        if (firstUserModel.isWaiting || firstUserModel.isMeeting) {
                                            checkString += currentUser.meetingSelectedFriend.get(0).name + "님은 이미 미팅에 참가중입니다\n";
                                        }


                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                                checkString = "";

                                FirebaseDatabase.getInstance().getReference().child("users").child(secondID).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        secondUserModel = dataSnapshot.getValue(UserModel.class);

                                        if (secondUserModel.isWaiting || secondUserModel.isMeeting) {
                                            checkString += currentUser.meetingSelectedFriend.get(1).name + "님은 이미 미팅에 참가중입니다\n";
                                        }

                                        if(checkString.equals("")){
                                            inputString = currentUser.meeting_join_number + "대" + currentUser.meeting_join_number + "입니다";
                                            checkIntent.putExtra("contents", inputString);
                                            currentAction = "meetingNext";
                                            startActivityForResult(checkIntent, 1);
                                        }else{
                                            checkIntent.putExtra("contents", checkString);
                                            currentAction = "meetingBack";
                                            startActivityForResult(checkIntent, 1);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                                checkString = "";

                            }else if(currentUser.meeting_join_number == 4) {
                                String firstID = currentUser.meetingSelectedFriend.get(0).id;
                                String secondID = currentUser.meetingSelectedFriend.get(1).id;
                                String thirdID = currentUser.meetingSelectedFriend.get(2).id;

                                FirebaseDatabase.getInstance().getReference().child("users").child(firstID).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        firstUserModel = dataSnapshot.getValue(UserModel.class);

                                        if (firstUserModel.isWaiting || firstUserModel.isMeeting) {
                                            checkString += currentUser.meetingSelectedFriend.get(0).name + "님은 이미 미팅에 참가중입니다\n";
                                        }

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                                checkString = "";

                                FirebaseDatabase.getInstance().getReference().child("users").child(secondID).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        secondUserModel = dataSnapshot.getValue(UserModel.class);

                                        if (secondUserModel.isWaiting || secondUserModel.isMeeting) {
                                            checkString += currentUser.meetingSelectedFriend.get(1).name + "님은 이미 미팅에 참가중입니다\n";
                                        }

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                                checkString = "";

                                FirebaseDatabase.getInstance().getReference().child("users").child(thirdID).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        thirdUserModel = dataSnapshot.getValue(UserModel.class);

                                        if (thirdUserModel.isWaiting || thirdUserModel.isMeeting) {
                                            checkString += currentUser.meetingSelectedFriend.get(0).name + "님은 이미 미팅에 참가중입니다\n";
                                        }

                                        if(checkString.equals("")){
                                            inputString = currentUser.meeting_join_number + "대" + currentUser.meeting_join_number + "입니다";
                                            checkIntent.putExtra("contents", inputString);
                                            currentAction = "meetingNext";
                                            startActivityForResult(checkIntent, 1);
                                        }else{
                                            checkIntent.putExtra("contents", checkString);
                                            currentAction = "meetingBack";
                                            startActivityForResult(checkIntent, 1);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                                checkString = "";
                            }

                        }


                    }catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //viewPager -> 1-2
                    viewPagerMeeting = findViewById(R.id.viewPagerMeeting);
                    MyViewPagerAdapterMeeting adapterMeeting = new MyViewPagerAdapterMeeting(getSupportFragmentManager());
                    getFragmentManager().beginTransaction().commitNow();

                    adapterMeeting.notifyDataSetChanged();
                    viewPagerMeeting.setAdapter(adapterMeeting);


                    viewPagerMeeting.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        }

                        @Override
                        public void onPageSelected(final int position) {
                            ohtwoMainUserNameMeeting.setText(outputMeeting[position]);
                            ohtwoMainPageNumMeeting.setText(Integer.toString(position + 1) + "/" + Integer.toString(currentUser.meeting_join_number));
                            referenceMoreImageMeeting = position;
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });

                }
            }
        });

        /* Layout 1_2 */

        meetingCancelButton = findViewById(R.id.meetingCancelButton);
        meetingCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentUser.sex.equals("여자")) { //wait에서 지워야함
                    try {
                        String meetingUrl = "http://125.176.119.133:18080/blindDateServer/meetDelete.jsp?id=" + currentUser.id + "&sex=" + currentUser.sex;
                        if (currentUser.meeting_join_number == 2) {
                            meetingUrl = meetingUrl + "&what=wait2";
                        } else if (currentUser.meeting_join_number == 3) {
                            meetingUrl = meetingUrl + "&what=wait3";
                        } else if (currentUser.meeting_join_number == 4) {
                            meetingUrl = meetingUrl + "&what=wait3";
                        }
                        callJsp.run(meetingUrl);
                        response = callJsp.response;

                        if (response.contains("true")) {
                            setNotMeeting(currentUser.id);
                            for (int i = 0; i < currentUser.meeting_join_number - 1; i++) {
                                setNotMeeting(currentUser.meetingSelectedFriend.get(i).id);
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else if(currentUser.sex.equals("남자")){
                    setNotMeeting(currentUser.id);
                    for (int i = 0; i < currentUser.meeting_join_number - 1; i++) {
                        setNotMeeting(currentUser.meetingSelectedFriend.get(i).id);
                    }
                }
            }
        });

        meetingWaitImage = findViewById(R.id.meetingWaitImage);
        GlideDrawableImageViewTarget gifImage = new GlideDrawableImageViewTarget(meetingWaitImage);
        Glide.with(this).load(R.drawable.meeting_wait).into(gifImage);

        /* Layout 1_3 */

        ohtwoMainSansoMeeting2 = findViewById(R.id.ohtwoMainSansoMeeting2);
        ohtwoMainSansoMeeting2.setText("산소 " + currentUser.cash + "+");
        ohtwoMainSansoMeeting2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CashActivity.class);
                startActivity(intent);
            }
        });

        buttonCollectionMeeting = findViewById(R.id.buttonCollectionMeeting);

        Drawable alphaMeeting = ((ImageView)findViewById(R.id.whiteLineMeeting)).getDrawable();
        alphaMeeting.setAlpha(40);

        refreshMeeting = findViewById(R.id.refreshMeeting);


        exitMeeting = findViewById(R.id.exitMeeting);
        exitMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentUser.isCaptain) {
                    currentAction = "meetExitComplete";
                    Intent intent = new Intent(getApplicationContext(), MeetingTwoButton.class);
                    intent.putExtra("contents", "상대방과의 모든 대화가 지워지고\n모든 미팅이 초기화 됩니다.\n그래도 진행하시겠습니까?");
                    startActivityForResult(intent, 1);
                }else{
                    Toast.makeText(getApplicationContext(), "각 팀의 대장만 미팅을 취소할 수 있습니다.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        left_arMeeting = findViewById(R.id.left_arMeeting);
        left_arMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int temp = viewPagerMeeting.getCurrentItem();
                if(temp>0){
                    viewPagerMeeting.setCurrentItem(temp-1);
                }

            }
        });

        right_arMeeting = findViewById(R.id.right_arMeeting);
        right_arMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int temp = viewPagerMeeting.getCurrentItem();
                if(temp<currentUser.meeting_join_number-1){
                    viewPagerMeeting.setCurrentItem(temp+1);
                }

            }
        });

        ohtwoMainTextMeeting = findViewById(R.id.ohtwoMainTextMeeting);
        ohtwoMainTextMeeting.setTextColor(Color.WHITE);

        ohtwoMainUserNameMeeting = findViewById(R.id.ohtwoMainUserNameMeeting);
        ohtwoMainUserNameMeeting.setTextColor(Color.WHITE);

        ohtwoMainPageNumMeeting = findViewById(R.id.ohtwoMainPageNumMeeting);
        ohtwoMainPageNumMeeting.setTextColor(Color.WHITE);

        getAnotherUserMeeting();

        isRefresh = getIntent().getStringExtra("isRefresh");
        isRefreshMeeting = getIntent().getStringExtra("isRefreshMeeting");
        //freeChat = getIntent().getBooleanExtra("freeChat", true);
        freeChat = appConfig.getBoolean("freeChat", true);

        ////////////////////////////////////////////

        if(isRefreshMeeting==null)
            isRefreshMeeting="false";

        if(isRefreshMeeting.equals("true") || idUriMeeting[0].equals("") ) {

            // 미팅 3 : 미팅 새로고침 -> idUriMeeting, otherNameMeeting, outputMeeting 값 새로 (맨 아래 별도 함수로 만들어서 호출할 것, 다른곳에서도 많이 써야하기 때문)

            /*String targetUrl = "http://125.176.119.133:18080/blindDateServer/getOtherUser.jsp?id=" + currentUser.id + "&sex=" + currentUser.sex + "&lat=" + currentUser.latitude + "&lon=" + currentUser.longitude;

            JSONObject[] outputJson = new JSONObject[5];

            try {
                callJsp.run(targetUrl);
                response = callJsp.response;
                response = response.substring(response.indexOf("[") + 1, response.lastIndexOf("]"));
                String[] responseArray = response.split("\\},");


                responseArray[0] = responseArray[0] + "}";
                responseArray[1] = responseArray[1] + "}";
                responseArray[2] = responseArray[2] + "}";
                responseArray[3] = responseArray[3] + "}";
                responseArray[4] = responseArray[4] + "}";

                for (int i = 0; i < 5; i++) {
                    outputJson[i] = new JSONObject(responseArray[i]);
                    int km = (int) Math.ceil(Double.parseDouble(outputJson[i].getString("km")));

                    output[i] = outputJson[i].getString("name") +"(" + outputJson[i].getString("age") +")";
                            *//*outputJson[i].getString("name") + "\n" +
                                    outputJson[i].getString("location") + "\t" +
                                    outputJson[i].getString("age") + "\t" +
                                    outputJson[i].getString("job") + "\t" +
                                    km + "km";*//*
                    idUri[i] = outputJson[i].getString("id");
                    otherName[i] = outputJson[i].getString("name");
                }

                setAnotherUserXmlFile(idUri, otherName, output);
                isRefresh = "false";


            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }*/

            saveAnotherUserMeeting();

            //todo
            //appConfigEditor.putInt("meeting_join_number", meeting_join_number);

            appConfigEditor.apply();
            appConfigEditor.commit();

        } else{

            getAnotherUserMeeting();

        }

        ohtwoMainUserNameMeeting.setText(outputMeeting[0]);
        ohtwoMainPageNumMeeting.setText("1/"+Integer.toString(currentUser.meeting_join_number));

        //viewPager
        viewPagerMeeting = findViewById(R.id.viewPagerMeeting);
        MyViewPagerAdapterMeeting adapterMeeting = new MyViewPagerAdapterMeeting(getSupportFragmentManager());
        getFragmentManager().beginTransaction().commitNow();

        adapterMeeting.notifyDataSetChanged();
        viewPagerMeeting.setAdapter(adapterMeeting);

        viewPagerMeeting.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(final int position) {
                ohtwoMainUserNameMeeting.setText(outputMeeting[position]);
                ohtwoMainPageNumMeeting.setText(Integer.toString(position + 1) + "/" + Integer.toString(currentUser.meeting_join_number));
                referenceMoreImageMeeting = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        ////////////////////////////

        for(int iter=0 ; iter<currentUser.meeting_join_number ; iter++){
            apacheCall.imageDownload(idUriMeeting[iter], main_page_activity);
            apacheCall.voiceDownload(idUriMeeting[iter], main_page_activity);
        }

        ohtwoMainVoiceButtonMeeting = findViewById(R.id.ohtwoMainVoiceButtonMeeting);
        ohtwoMainSpecificBottonMeeting = findViewById(R.id.ohtwoMainSpecificBottonMeeting);
        ohtwoMainMoreImageBottonMeeting = findViewById(R.id.ohtwoMainMoreImageBottonMeeting);
        ohtwoMainChatButtonMeeting = findViewById(R.id.ohtwoMainChatButtonMeeting);

        ohtwoMainVoiceButtonMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file = Environment.getExternalStorageDirectory();
                //갤럭시 S4기준으로 /storage/emulated/0/의 경로를 갖고 시작한다.
                String path = file.getAbsolutePath() + "/.connectionist/" + idUriMeeting[viewPager.getCurrentItem()] + "/user.3gp";
                File voiceFile = new File(path);

                if(!togglePlayMeeting){
                    try {
                        ohtwoMainVoiceButtonMeeting.setImageResource(R.drawable.voice_meeting_stop);
                        togglePlayMeeting = true;

                        if(voiceFile.exists()) {
                            mediaPlayerMeeting = new MediaPlayer();
                            mediaPlayerMeeting.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            mediaPlayerMeeting.setDataSource(getApplicationContext(), Uri.parse(path));
                            mediaPlayerMeeting.prepare();
                            mediaPlayerMeeting.start();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    ohtwoMainVoiceButtonMeeting.setImageResource(R.drawable.voice_meeting);
                    togglePlayMeeting = false;
                    if(voiceFile.exists()) {
                        mediaPlayerMeeting.stop();
                    }
                }
            }
        });

        ohtwoMainSpecificBottonMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String output = appConfig.getString("output" + Integer.toString(viewPagerMeeting.getCurrentItem()) + "Meeting","");
                String name = appConfig.getString("otherName" + Integer.toString(viewPagerMeeting.getCurrentItem()) + "Meeting","");
                String age = appConfig.getString("otherAge" + Integer.toString(viewPagerMeeting.getCurrentItem()) + "Meeting","");
                String smoke = appConfig.getString("otherSmoke" + Integer.toString(viewPagerMeeting.getCurrentItem()) + "Meeting","");
                String religion = appConfig.getString("otherReligion" + Integer.toString(viewPagerMeeting.getCurrentItem()) + "Meeting","");
                String job = appConfig.getString("otherJob" + Integer.toString(viewPagerMeeting.getCurrentItem()) + "Meeting","");
                String location = appConfig.getString("otherLocation" + Integer.toString(viewPagerMeeting.getCurrentItem()) + "Meeting","");
                String prefer = appConfig.getString("otherPrefer" + Integer.toString(viewPagerMeeting.getCurrentItem()) + "Meeting","");

                Intent intent = new Intent(getApplicationContext(), SpecificInfoDialogMeeting.class);
                intent.putExtra("output", output);
                intent.putExtra("name", name);
                intent.putExtra("age", age);
                intent.putExtra("smoke", smoke);
                intent.putExtra("religion", religion);
                intent.putExtra("job", job);
                intent.putExtra("location", location);
                intent.putExtra("prefer", prefer);
                startActivityForResult(intent, 1);

            }
        });

        ohtwoMainMoreImageBottonMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MoreImageDialogMeeting.class);
                startActivityForResult(intent, 1);
            }
        });

        ohtwoMainChatButtonMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean a = false;
                if(currentUser.isComplete){
                    //채팅방 입장 메서드

                    FirebaseDatabase.getInstance().getReference().child("meeting").orderByChild("users/"+uid).equalTo(true).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            for(DataSnapshot item : dataSnapshot.getChildren()){
                                ChatModel  chatModel = item.getValue(ChatModel.class);
                                if(chatModel.users.containsKey(uid)){
                                    dstRoomKey = item.getKey();
                                }
                            }
                            Intent meetIntent = new Intent(getApplicationContext(),meetChatActivity.class);
                            meetIntent.putExtra("dstRoomID", dstRoomKey);
                            startActivity(meetIntent);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }else {
                    try {
                        a = ohtwoUtil.checkOne();
                        if (a) {
                            Intent intent1 = new Intent(getApplicationContext(), MeetingTwoButton.class);
                            intent1.putExtra("contents", "1산소가 소모됩니다. \n미팅을 시작하시겠습니까?");
                            currentAction = "meetChatStart";
                            startActivityForResult(intent1, 1);

                        } else {
                            Intent intent = new Intent(getApplicationContext(), MeetingTwoButton.class);
                            intent.putExtra("contents", "산소가 부족합니다. \n충전하러가시겠습니까?");
                            currentAction = "gotoCache";
                            startActivityForResult(intent, 1);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        /*----------------------------------------------------------------------------------------------------*/
        /*----------------------------------------------Layout 2----------------------------------------------*/
        /*----------------------------------------------onCreate----------------------------------------------*/
        /*----------------------------------------------------------------------------------------------------*/


        buttonCollection = findViewById(R.id.buttonCollection);

        Drawable alpha = ((ImageView)findViewById(R.id.whiteLine)).getDrawable();
        alpha.setAlpha(40);

        chatList = findViewById(R.id.chat_list);
        chatList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chatIntent = new Intent(main_page.this, ChatList.class);
                startActivity(chatIntent);
            }

        });

        left_ar = findViewById(R.id.left_ar);
        left_ar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int temp = viewPager.getCurrentItem();
                if(temp>0){
                    viewPager.setCurrentItem(temp-1);
                }

            }
        });

        right_ar = findViewById(R.id.right_ar);
        right_ar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int temp = viewPager.getCurrentItem();
                if(temp<5){
                    viewPager.setCurrentItem(temp+1);
                }

            }
        });

        whiteLine = findViewById(R.id.whiteLine);

        refreshButton = findViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(currentUser.hasRefreshItem){
                    freeChat = true;
                    appConfigEditor.putBoolean("freeChat", true);
                    for(int y=0; y<5; y++) {
                        chatOpen[y] = false;
                        appConfigEditor.putBoolean("chatOpen"+y, false);
                    }
                    appConfigEditor.apply();
                    appConfigEditor.commit();

                    Intent intent = new Intent(getApplicationContext(), main_page.class);
                    intent.putExtra("isRefresh","true");
                    intent.putExtra("isRefreshMeeting","false");
                    startActivity(intent);
                    finish();
                } else{
                    if (mRewardedVideoAd.isLoaded()) {
                        mRewardedVideoAd.show();
                    }
                }

            }
        });

        refreshButton1 = findViewById(R.id.refreshButton1);
        refreshButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRewardedVideoAd.isLoaded()) {
                    mRewardedVideoAd.show();
                }
            }
        });

        refreshButton2 = findViewById(R.id.refreshButton2);
        refreshButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentAction = "subOne";
                Intent intent = new Intent(getApplicationContext(), MeetingTwoButton.class);
                intent.putExtra("contents", "새로고침을 위해 1산소가 소모됩니다.\n계속 하시겠습니까?");
                startActivityForResult(intent, 1);


            }
        });

        refreshItemSubText = findViewById(R.id.refreshItemSubText);

        refreshItemMoveText = findViewById(R.id.refreshItemMoveText);
        refreshItemMoveText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), CashActivity.class);
                startActivity(intent);

            }
        });

        ohtwoMainText = findViewById(R.id.ohtwoMainText);
        ohtwoMainText.setTextColor(Color.WHITE);

        ohtwoMainSanso = findViewById(R.id.ohtwoMainSanso);
        ohtwoMainSanso.setText("산소 " + currentUser.cash + "+");
        ohtwoMainSanso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CashActivity.class);
                startActivity(intent);
            }
        });

        ohtwoMainUserName = findViewById(R.id.ohtwoMainUserName);
        ohtwoMainUserName.setTextColor(Color.WHITE);

        ohtwoMainPageNum = findViewById(R.id.ohtwoMainPageNum);
        ohtwoMainPageNum.setTextColor(Color.WHITE);

        //read previous information
        getAnotherUser();

        isRefresh = getIntent().getStringExtra("isRefresh");
        isRefreshMeeting = getIntent().getStringExtra("isRefreshMeeting");
        //freeChat = getIntent().getBooleanExtra("freeChat", true);
        freeChat = appConfig.getBoolean("freeChat", true);


        if(isRefresh.equals("true") || idUri[0].equals("") ) {

            String targetUrl = "http://125.176.119.133:18080/blindDateServer/getOtherUser.jsp?id=" + currentUser.id + "&sex=" + currentUser.sex + "&lat=" + currentUser.latitude + "&lon=" + currentUser.longitude;

            JSONObject[] outputJson = new JSONObject[5];

            try {
                callJsp.run(targetUrl);
                response = callJsp.response;
                int count = 0; //도착한 친구요청 숫자
                for(int i=0; i<response.length(); i++)
                {
                    if(response.charAt(i) == '}') {
                        count++;
                    }
                }
                if (count==0){
                    response = "[{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"}]";
                }
                response = response.substring(response.indexOf("[") + 1, response.lastIndexOf("]"));
                if(count==1){
                    response = response +",{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"}";
                }
                if(count==2){
                    response = response +",{\"id\":\"\"},{\"id\":\"\"},{\"id\":\"\"}";
                }
                if(count==3){
                    response = response +",{\"id\":\"\"},{\"id\":\"\"}";
                }
                if(count==4){
                    response = response +",{\"id\":\"\"}";
                }

                String[] responseArray = response.split("\\},");


                responseArray[0] = responseArray[0] + "}";
                responseArray[1] = responseArray[1] + "}";
                responseArray[2] = responseArray[2] + "}";
                responseArray[3] = responseArray[3] + "}";
                responseArray[4] = responseArray[4] + "}";

                for (int i = 0; i < 5; i++) {
                    outputJson[i] = new JSONObject(responseArray[i]);
                    idUri[i] = outputJson[i].getString("id");
                    if (idUri.equals("")) {
                        output[i] = "찾을 수 없음";
                        otherName[i] = "상대방이 더 없습니다";
                    } else {
                        int km = (int) Math.ceil(Double.parseDouble(outputJson[i].getString("km")));
                        output[i] = outputJson[i].getString("name") + "(" + outputJson[i].getString("age") + ")";
                            /*outputJson[i].getString("name") + "\n" +
                                    outputJson[i].getString("location") + "\t" +
                                    outputJson[i].getString("age") + "\t" +
                                    outputJson[i].getString("job") + "\t" +
                                    km + "km";*/
                        otherName[i] = outputJson[i].getString("name");
                        otherAge[i] = outputJson[i].getString("age");
                        otherSmoke[i] = outputJson[i].getString("smoke");
                        otherReligion[i] = outputJson[i].getString("religion");
                        otherJob[i] = outputJson[i].getString("job");
                        otherLocation[i] = outputJson[i].getString("location");
                        otherPrefer[i] = outputJson[i].getString("preferage");

                        appConfigEditor.putString(idUri[i], otherName[i]);


                    }
                }

                saveAnotherUser();
                appConfigEditor.apply();
                appConfigEditor.commit();
                isRefresh = "false";


            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        ohtwoMainUserName.setText(output[0]);
        ohtwoMainPageNum.setText("1/6");
        refreshButton.setVisibility(View.INVISIBLE);

        refreshButton.setVisibility(View.INVISIBLE);
        refreshItemSubText.setVisibility(View.INVISIBLE);
        refreshItemMoveText.setVisibility(View.INVISIBLE);
        refreshButton1.setVisibility(View.INVISIBLE);
        refreshButton2.setVisibility(View.INVISIBLE);


        //for Test

        /*idUri[2] = "naver60496274";
        otherName[2] = "유현담";
        output[2] = "test 유현담";*/

        /*
        idUri[3] = "naver25259465";
        otherName[3] = "최병준";
        output[3] = "test 최병준";
        */

        /*idUri[3] = "naver25259465";
        otherName[3] = "최병준";
        output[3] = "test 최병준";*/

        /*idUri[4] = "naver78046897";
        otherName[4] = "김용훈";
        output[4] = "test 김용훈";*/

        /*idUri[4] = "";
        otherName[4] = "김용훈";
        output[4] = "test 김용훈";*/

        for(int iter=0 ; iter<5 ; iter++){
            apacheCall.imageDownload(idUri[iter], main_page_activity);
            apacheCall.voiceDownload(idUri[iter],main_page_activity);
        }

        //viewPager
        viewPager = findViewById(R.id.viewPager);
        MyViewPagerAdapter adapter = new MyViewPagerAdapter(getSupportFragmentManager());
        getFragmentManager().beginTransaction().commitNow();

        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(final int position) {

                if(position>=0 && position<5) {

                    if(idUri[position].equals("")){

                        ohtwoMainUserName.setText("광고 페이지");
                        ohtwoMainPageNum.setText(Integer.toString(position + 1) + "/6");
                        buttonCollection.setVisibility(View.INVISIBLE);
                        ohtwoMainUserName.setVisibility(View.INVISIBLE);
                        ohtwoMainPageNum.setVisibility(View.INVISIBLE);
                        refreshButton.setVisibility(View.INVISIBLE);
                        ohtwoMainText.setTextColor(Color.parseColor("#33aaee"));
                        ohtwoMainSanso.setTextColor(Color.parseColor("#33aaee"));
                        whiteLine.setImageResource(R.drawable.blue);
                        chatList.setImageResource(R.drawable.chat_list_blue);
                        refreshItemSubText.setVisibility(View.INVISIBLE);
                        refreshItemMoveText.setVisibility(View.INVISIBLE);
                        refreshButton1.setVisibility(View.INVISIBLE);
                        refreshButton2.setVisibility(View.INVISIBLE);
                    } else{
                        ohtwoMainUserName.setText(output[position]);
                        ohtwoMainPageNum.setText(Integer.toString(position + 1) + "/6");
                        buttonCollection.setVisibility(View.VISIBLE);
                        ohtwoMainUserName.setVisibility(View.VISIBLE);
                        ohtwoMainPageNum.setVisibility(View.VISIBLE);
                        refreshButton.setVisibility(View.INVISIBLE);
                        referenceMoreImage = position;
                        ohtwoMainText.setTextColor(Color.parseColor("#ffffff"));
                        ohtwoMainSanso.setTextColor(Color.parseColor("#ffffff"));
                        chatList.setImageResource(R.drawable.chat_list);
                        whiteLine.setImageResource(R.drawable.white);
                        Drawable alpha = ((ImageView)findViewById(R.id.whiteLine)).getDrawable();
                        alpha.setAlpha(40);
                        refreshItemSubText.setVisibility(View.INVISIBLE);
                        refreshItemMoveText.setVisibility(View.INVISIBLE);
                        refreshButton1.setVisibility(View.INVISIBLE);
                        refreshButton2.setVisibility(View.INVISIBLE);
                    }

                } else if(position==5){
                    ohtwoMainUserName.setText("광고 페이지");
                    ohtwoMainPageNum.setText(Integer.toString(position + 1) + "/6");
                    buttonCollection.setVisibility(View.INVISIBLE);
                    ohtwoMainUserName.setVisibility(View.INVISIBLE);
                    ohtwoMainPageNum.setVisibility(View.INVISIBLE);
                    refreshButton.setVisibility(View.VISIBLE);
                    referenceMoreImage = position;
                    ohtwoMainText.setTextColor(Color.parseColor("#33aaee"));
                    ohtwoMainSanso.setTextColor(Color.parseColor("#33aaee"));
                    chatList.setImageResource(R.drawable.chat_list_blue);
                    whiteLine.setImageResource(R.drawable.blue);
                    refreshItemSubText.setVisibility(View.VISIBLE);
                    refreshItemMoveText.setVisibility(View.VISIBLE);

                    if(currentUser.hasRefreshItem){
                        underLineText = new SpannableString("남은 기간 : " + currentUser.isRefreshItem);
                        underLineText.setSpan(new UnderlineSpan(), 0, underLineText.length(), 0);
                        refreshItemMoveText.setText(underLineText);
                        refreshButton1.setVisibility(View.INVISIBLE);
                        refreshButton2.setVisibility(View.INVISIBLE);
                    } else{
                        underLineText = new SpannableString("사용권 구매하러 가기");
                        underLineText.setSpan(new UnderlineSpan(), 0, underLineText.length(), 0);
                        refreshItemMoveText.setText(underLineText);
                        refreshButton1.setVisibility(View.VISIBLE);
                        refreshButton2.setVisibility(View.VISIBLE);
                    }

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        ohtwoMainVoiceButton = findViewById(R.id.ohtwoMainVoiceButton);
        ohtwoMainSpecificBotton = findViewById(R.id.ohtwoMainSpecificBotton);
        ohtwoMainMoreImageBotton = findViewById(R.id.ohtwoMainMoreImageBotton);
        ohtwoMainChatButton = findViewById(R.id.ohtwoMainChatButton);

        ohtwoMainVoiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file = Environment.getExternalStorageDirectory();
                //갤럭시 S4기준으로 /storage/emulated/0/의 경로를 갖고 시작한다.
                String path = file.getAbsolutePath() + "/.connectionist/" + idUri[viewPager.getCurrentItem()] + "/user.3gp";
                File voiceFile = new File(path);

                if(!togglePlay){
                    try {
                        ohtwoMainVoiceButton.setImageResource(R.drawable.voice_stop);
                        togglePlay = true;
                        if(voiceFile.exists()) {
                            mediaPlayer = new MediaPlayer();
                            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            mediaPlayer.setDataSource(getApplicationContext(), Uri.parse(path));
                            mediaPlayer.prepare();
                            mediaPlayer.start();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    ohtwoMainVoiceButton.setImageResource(R.drawable.voice);
                    togglePlay = false;
                    if(voiceFile.exists()) {
                        mediaPlayer.stop();
                    }
                }
            }
        });

        ohtwoMainSpecificBotton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String output = appConfig.getString("output" + Integer.toString(viewPager.getCurrentItem()),"");
                String name = appConfig.getString("otherName" + Integer.toString(viewPager.getCurrentItem()),"");
                String age = appConfig.getString("otherAge" + Integer.toString(viewPager.getCurrentItem()),"");
                String smoke = appConfig.getString("otherSmoke" + Integer.toString(viewPager.getCurrentItem()),"");
                String religion = appConfig.getString("otherReligion" + Integer.toString(viewPager.getCurrentItem()),"");
                String job = appConfig.getString("otherJob" + Integer.toString(viewPager.getCurrentItem()),"");
                String location = appConfig.getString("otherLocation" + Integer.toString(viewPager.getCurrentItem()),"");
                String prefer = appConfig.getString("otherPrefer" + Integer.toString(viewPager.getCurrentItem()),"");

                Intent intent = new Intent(getApplicationContext(), SpecificInfoDialog.class);
                intent.putExtra("output", output);
                intent.putExtra("name", name);
                intent.putExtra("age", age);
                intent.putExtra("smoke", smoke);
                intent.putExtra("religion", religion);
                intent.putExtra("job", job);
                intent.putExtra("location", location);
                intent.putExtra("prefer", prefer);
                startActivityForResult(intent, 1);

            }
        });

        ohtwoMainMoreImageBotton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), MoreImageDialog.class);
                startActivityForResult(intent, 1);
            }
        });

        for(int y =0; y<5; y++ ) {
            chatOpen[y] = appConfig.getBoolean("chatOpen"+y, false);
        }

        ohtwoMainChatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), chatActivity.class);
                /* for test
                intent.putExtra("otherID", idUri[0]);
                intent.putExtra("otherName", otherName[0]);
                */
                if(chatOpen[viewPager.getCurrentItem()]==true) {
                    intent.putExtra("otherID", idUri[viewPager.getCurrentItem()]);
                    intent.putExtra("otherName", otherName[viewPager.getCurrentItem()]);
                    startActivity(intent);

                }else if (freeChat == true) {
                    chatOpen[viewPager.getCurrentItem()]=true;
                    appConfigEditor.putBoolean("chatOpen"+viewPager.getCurrentItem(), true);
                    freeChat = false;
                    appConfigEditor.putBoolean("freeChat",false);
                    appConfigEditor.apply();
                    appConfigEditor.commit();

                    intent.putExtra("otherID", idUri[viewPager.getCurrentItem()]);
                    intent.putExtra("otherName", otherName[viewPager.getCurrentItem()]);
                    /*intent.putExtra("otherID", "naver25259465");
                    intent.putExtra("otherName", "최병준");*/
                    startActivity(intent);
                }else {
                    try {
                        boolean check = ohtwoUtil.checkOne();
                        if (check) {
                            boolean result = ohtwoUtil.subOne();
                            if (result) {
                                currentUser.cash = ohtwoUtil.encThreadResult;
                                Toast.makeText(getApplicationContext(), "1산소를 썼어요", Toast.LENGTH_SHORT).show();
                                chatOpen[viewPager.getCurrentItem()]=true;
                                appConfigEditor.putBoolean("chatOpen"+viewPager.getCurrentItem(), true);
                                appConfigEditor.apply();
                                appConfigEditor.commit();
                                intent.putExtra("otherID", idUri[viewPager.getCurrentItem()]);
                                intent.putExtra("otherName", otherName[viewPager.getCurrentItem()]);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getApplicationContext(), "산소가 안 써졌어요. 다시 입장해주세요", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "1 산소가 없어요", Toast.LENGTH_SHORT).show();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });



        /*----------------------------------------------------------------------------------------------------*/
        /*----------------------------------------------Layout 3----------------------------------------------*/
        /*----------------------------------------------onCreate----------------------------------------------*/
        /*----------------------------------------------------------------------------------------------------*/

        updateButton = findViewById(R.id.button_3_1);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), userUpdateImageActivity.class);
                startActivity(intent);
            }
        });

        configButton = findViewById(R.id.button_3_2);
        configButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(main_page.this,AppConfigActivity.class);
                startActivity(intent);
            }
        });

        cashButton = findViewById(R.id.button_3_3);
        cashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), CashActivity.class);
                startActivity(intent);
            }
        });

        friendButton = findViewById(R.id.button_3_4);
        friendButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent myIntent = new Intent(main_page.this,friendActivity.class);
                startActivityForResult(myIntent, 2);
            }
        });

        /*userImageView = (ImageView) findViewById(R.id.userImageView);
        apacheCall.imageViewDownload(userImageView, currentUser.id, main_page_activity);*/

        if(currentUser.isTodayLogin){
            //give one sanso

            boolean result = false;
            int sanso = Integer.parseInt(currentUser.event);

            try {
                if(sanso == 1)
                    result = ohtwoUtil.addOne();
                else if(sanso == 3)
                    result = ohtwoUtil.addThree();
                else if(sanso == 5)
                    result = ohtwoUtil.addFive();
                else if(sanso == 10)
                    result = ohtwoUtil.addTen();
                else
                    result = false;

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(result){
                currentUser.isTodayLogin=false;
                currentUser.cash = ohtwoUtil.encThreadResult;

                Intent intent = new Intent(getApplicationContext(), DialogOneButton.class);

                if(currentUser.hasNotice)
                    intent.putExtra("contents", currentUser.message + "\n" + currentUser.noticeMessage);
                else
                    intent.putExtra("contents", currentUser.message);

                startActivityForResult(intent, 1);
            }

        } else if(currentUser.hasNotice){
            currentUser.hasNotice = false;
            Intent intent = new Intent(getApplicationContext(), DialogOneButton.class);
            intent.putExtra("contents", currentUser.noticeMessage);
            startActivityForResult(intent, 1);
        }




    }
/*
    @Override
    public void onBackPressed() {
        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;

        if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime)
        {
            finishAffinity();
            System.runFinalization();
            System.exit(0);


        }
        else
        {
            backPressedTime = tempTime;
            Toast.makeText(getApplicationContext(), "\'뒤로\'버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
        }
    }
*/

    ////cauly

    // Back Key가 눌러졌을 때, CloseAd 호출
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 앱을 처음 설치하여 실행할 때, 필요한 리소스를 다운받았는지 여부.
            if (mCloseAd.isModuleLoaded())
            {
                mCloseAd.show(this);
            }
            else
            {
                // 광고에 필요한 리소스를 한번만  다운받는데 실패했을 때 앱의 종료팝업 구현
                showDefaultClosePopup();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showDefaultClosePopup()
    {
        new AlertDialog.Builder(this).setTitle("").setMessage("종료 하시겠습니까?")
                .setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        finishAffinity();
                        System.runFinalization();
                        System.exit(0);
                    }
                })
                .setNegativeButton("아니요",null)
                .show();
    }

    // CaulyCloseAdListener
    @Override
    public void onFailedToReceiveCloseAd(CaulyCloseAd ad, int errCode,String errMsg) {


    }
    // CloseAd의 광고를 클릭하여 앱을 벗어났을 경우 호출되는 함수이다.
    @Override
    public void onLeaveCloseAd(CaulyCloseAd ad) {
    }
    // CloseAd의 request()를 호출했을 때, 광고의 여부를 알려주는 함수이다.
    @Override
    public void onReceiveCloseAd(CaulyCloseAd ad, boolean isChargable) {

    }
    //왼쪽 버튼을 클릭 하였을 때, 원하는 작업을 수행하면 된다.
    @Override
    public void onLeftClicked(CaulyCloseAd ad) {

    }
    //오른쪽 버튼을 클릭 하였을 때, 원하는 작업을 수행하면 된다. (종료창에서 예 눌렀을때)
    //Default로는 오른쪽 버튼이 종료로 설정되어있다.
    @Override
    public void onRightClicked(CaulyCloseAd ad) {
        cm.unregisterNetworkCallback(ncb);
        finishAffinity();
        System.runFinalization();
        System.exit(0);
    }
    @Override
    public void onShowedCloseAd(CaulyCloseAd ad, boolean isChargable) {
    }

    ////

    ////reward videos

    private void loadRewardedVideoAd() {
        //real ad-unit id is : ca-app-pub-1966530831722514/4763880596
        mRewardedVideoAd.loadAd("ca-app-pub-1966530831722514/4763880596", new AdRequest.Builder().build());
        //mRewardedVideoAd.loadAd("ca-app-pub-3940256099942544/5224354917", new AdRequest.Builder().build());
    }

    @Override
    public void onRewarded(RewardItem reward) {
        //Toast.makeText(this, "onRewarded! currency: " + reward.getType() + "  amount: " + reward.getAmount(), Toast.LENGTH_SHORT).show();
        // Reward the user.

        freeChat = true;
        appConfigEditor.putBoolean("freeChat", true);
        for(int y=0; y<5; y++) {
            chatOpen[y] = false;
            appConfigEditor.putBoolean("chatOpen"+viewPager.getCurrentItem(), false);
        }
        appConfigEditor.apply();
        appConfigEditor.commit();

        Intent intent = new Intent(getApplicationContext(), main_page.class);
        intent.putExtra("isRefresh","true");
        intent.putExtra("isRefreshMeeting","false");
        //intent.putExtra("freeChat",freeChat);
        startActivity(intent);
        finish();
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Toast.makeText(this, "onRewardedVideoAdLeftApplication",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdClosed() {
        loadRewardedVideoAd();
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int errorCode) {

        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);

        loadRewardedVideoAd();
    }

    @Override
    public void onRewardedVideoAdLoaded() {

    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    public void onRewardedVideoCompleted() {

    }

    @Override
    public void onResume() {
        mRewardedVideoAd.resume(this);
        super.onResume();
        if (mCloseAd != null)
            mCloseAd.resume(this); // 필수 호출
    }

    @Override
    public void onPause() {
        mRewardedVideoAd.pause(this);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mRewardedVideoAd.destroy(this);
        super.onDestroy();
    }

    //// another User

    public void saveAnotherUser(){

        appConfigEditor.putString("output0", output[0]);
        appConfigEditor.putString("output1", output[1]);
        appConfigEditor.putString("output2", output[2]);
        appConfigEditor.putString("output3", output[3]);
        appConfigEditor.putString("output4", output[4]);

        appConfigEditor.putString("idUri0", idUri[0]);
        appConfigEditor.putString("idUri1", idUri[1]);
        appConfigEditor.putString("idUri2", idUri[2]);
        appConfigEditor.putString("idUri3", idUri[3]);
        appConfigEditor.putString("idUri4", idUri[4]);

        appConfigEditor.putString("otherName0", otherName[0]);
        appConfigEditor.putString("otherName1", otherName[1]);
        appConfigEditor.putString("otherName2", otherName[2]);
        appConfigEditor.putString("otherName3", otherName[3]);
        appConfigEditor.putString("otherName4", otherName[4]);

        appConfigEditor.putString("otherAge0", otherAge[0]);
        appConfigEditor.putString("otherAge1", otherAge[1]);
        appConfigEditor.putString("otherAge2", otherAge[2]);
        appConfigEditor.putString("otherAge3", otherAge[3]);
        appConfigEditor.putString("otherAge4", otherAge[4]);

        appConfigEditor.putString("otherSmoke0", otherSmoke[0]);
        appConfigEditor.putString("otherSmoke1", otherSmoke[1]);
        appConfigEditor.putString("otherSmoke2", otherSmoke[2]);
        appConfigEditor.putString("otherSmoke3", otherSmoke[3]);
        appConfigEditor.putString("otherSmoke4", otherSmoke[4]);

        appConfigEditor.putString("otherReligion0", otherReligion[0]);
        appConfigEditor.putString("otherReligion1", otherReligion[1]);
        appConfigEditor.putString("otherReligion2", otherReligion[2]);
        appConfigEditor.putString("otherReligion3", otherReligion[3]);
        appConfigEditor.putString("otherReligion4", otherReligion[4]);

        appConfigEditor.putString("otherJob0", otherJob[0]);
        appConfigEditor.putString("otherJob1", otherJob[1]);
        appConfigEditor.putString("otherJob2", otherJob[2]);
        appConfigEditor.putString("otherJob3", otherJob[3]);
        appConfigEditor.putString("otherJob4", otherJob[4]);

        appConfigEditor.putString("otherLocation0", otherLocation[0]);
        appConfigEditor.putString("otherLocation1", otherLocation[1]);
        appConfigEditor.putString("otherLocation2", otherLocation[2]);
        appConfigEditor.putString("otherLocation3", otherLocation[3]);
        appConfigEditor.putString("otherLocation4", otherLocation[4]);

        appConfigEditor.putString("otherPrefer0", otherPrefer[0]);
        appConfigEditor.putString("otherPrefer1", otherPrefer[1]);
        appConfigEditor.putString("otherPrefer2", otherPrefer[2]);
        appConfigEditor.putString("otherPrefer3", otherPrefer[3]);
        appConfigEditor.putString("otherPrefer4", otherPrefer[4]);

        appConfigEditor.apply();
        appConfigEditor.commit();

    };

    public void getAnotherUser(){


        output[0] = appConfig.getString("output0","");
        output[1] = appConfig.getString("output1","");
        output[2] = appConfig.getString("output2","");
        output[3] = appConfig.getString("output3","");
        output[4] = appConfig.getString("output4","");

        idUri[0] = appConfig.getString("idUri0","");
        idUri[1] = appConfig.getString("idUri1","");
        idUri[2] = appConfig.getString("idUri2","");
        idUri[3] = appConfig.getString("idUri3","");
        idUri[4] = appConfig.getString("idUri4","");

        otherName[0] = appConfig.getString("otherName0","");
        otherName[1] = appConfig.getString("otherName1","");
        otherName[2] = appConfig.getString("otherName2","");
        otherName[3] = appConfig.getString("otherName3","");
        otherName[4] = appConfig.getString("otherName4","");

        otherAge[0] = appConfig.getString("otherAge0","");
        otherAge[1] = appConfig.getString("otherAge1","");
        otherAge[2] = appConfig.getString("otherAge2","");
        otherAge[3] = appConfig.getString("otherAge3","");
        otherAge[4] = appConfig.getString("otherAge4","");

        otherSmoke[0] = appConfig.getString("otherSmoke0","");
        otherSmoke[1] = appConfig.getString("otherSmoke1","");
        otherSmoke[2] = appConfig.getString("otherSmoke2","");
        otherSmoke[3] = appConfig.getString("otherSmoke3","");
        otherSmoke[4] = appConfig.getString("otherSmoke4","");

        otherReligion[0] = appConfig.getString("otherReligion0","");
        otherReligion[1] = appConfig.getString("otherReligion1","");
        otherReligion[2] = appConfig.getString("otherReligion2","");
        otherReligion[3] = appConfig.getString("otherReligion3","");
        otherReligion[4] = appConfig.getString("otherReligion4","");

        otherJob[0] = appConfig.getString("otherJob0","");
        otherJob[1] = appConfig.getString("otherJob1","");
        otherJob[2] = appConfig.getString("otherJob2","");
        otherJob[3] = appConfig.getString("otherJob3","");
        otherJob[4] = appConfig.getString("otherJob4","");

        otherLocation[0] = appConfig.getString("otherLocation0","");
        otherLocation[1] = appConfig.getString("otherLocation1","");
        otherLocation[2] = appConfig.getString("otherLocation2","");
        otherLocation[3] = appConfig.getString("otherLocation3","");
        otherLocation[4] = appConfig.getString("otherLocation4","");

        otherPrefer[0] = appConfig.getString("otherPrefer0","");
        otherPrefer[1] = appConfig.getString("otherPrefer1","");
        otherPrefer[2] = appConfig.getString("otherPrefer2","");
        otherPrefer[3] = appConfig.getString("otherPrefer3","");
        otherPrefer[4] = appConfig.getString("otherPrefer4","");

    };

    public void saveAnotherUserMeeting(){
        appConfigEditor.putString("output0Meeting", outputMeeting[0]);
        appConfigEditor.putString("output1Meeting", outputMeeting[1]);
        appConfigEditor.putString("output2Meeting", outputMeeting[2]);
        appConfigEditor.putString("output3Meeting", outputMeeting[3]);
        appConfigEditor.putString("output4Meeting", outputMeeting[4]);

        appConfigEditor.putString("idUri0Meeting", idUriMeeting[0]);
        appConfigEditor.putString("idUri1Meeting", idUriMeeting[1]);
        appConfigEditor.putString("idUri2Meeting", idUriMeeting[2]);
        appConfigEditor.putString("idUri3Meeting", idUriMeeting[3]);
        appConfigEditor.putString("idUri4Meeting", idUriMeeting[4]);

        appConfigEditor.putString("otherName0Meeting", otherNameMeeting[0]);
        appConfigEditor.putString("otherName1Meeting", otherNameMeeting[1]);
        appConfigEditor.putString("otherName2Meeting", otherNameMeeting[2]);
        appConfigEditor.putString("otherName3Meeting", otherNameMeeting[3]);
        appConfigEditor.putString("otherName4Meeting", otherNameMeeting[4]);

        appConfigEditor.putString("otherAge0Meeting", otherAgeMeeting[0]);
        appConfigEditor.putString("otherAge1Meeting", otherAgeMeeting[1]);
        appConfigEditor.putString("otherAge2Meeting", otherAgeMeeting[2]);
        appConfigEditor.putString("otherAge3Meeting", otherAgeMeeting[3]);
        appConfigEditor.putString("otherAge4Meeting", otherAgeMeeting[4]);

        appConfigEditor.putString("otherSmoke0Meeting", otherSmokeMeeting[0]);
        appConfigEditor.putString("otherSmoke1Meeting", otherSmokeMeeting[1]);
        appConfigEditor.putString("otherSmoke2Meeting", otherSmokeMeeting[2]);
        appConfigEditor.putString("otherSmoke3Meeting", otherSmokeMeeting[3]);
        appConfigEditor.putString("otherSmoke4Meeting", otherSmokeMeeting[4]);

        appConfigEditor.putString("otherReligion0Meeting", otherReligionMeeting[0]);
        appConfigEditor.putString("otherReligion1Meeting", otherReligionMeeting[1]);
        appConfigEditor.putString("otherReligion2Meeting", otherReligionMeeting[2]);
        appConfigEditor.putString("otherReligion3Meeting", otherReligionMeeting[3]);
        appConfigEditor.putString("otherReligion4Meeting", otherReligionMeeting[4]);

        appConfigEditor.putString("otherJob0Meeting", otherJobMeeting[0]);
        appConfigEditor.putString("otherJob1Meeting", otherJobMeeting[1]);
        appConfigEditor.putString("otherJob2Meeting", otherJobMeeting[2]);
        appConfigEditor.putString("otherJob3Meeting", otherJobMeeting[3]);
        appConfigEditor.putString("otherJob4Meeting", otherJobMeeting[4]);

        appConfigEditor.putString("otherLocation0Meeting", otherLocationMeeting[0]);
        appConfigEditor.putString("otherLocation1Meeting", otherLocationMeeting[1]);
        appConfigEditor.putString("otherLocation2Meeting", otherLocationMeeting[2]);
        appConfigEditor.putString("otherLocation3Meeting", otherLocationMeeting[3]);
        appConfigEditor.putString("otherLocation4Meeting", otherLocationMeeting[4]);

        appConfigEditor.putString("otherPrefer0Meeting", otherPreferMeeting[0]);
        appConfigEditor.putString("otherPrefer1Meeting", otherPreferMeeting[1]);
        appConfigEditor.putString("otherPrefer2Meeting", otherPreferMeeting[2]);
        appConfigEditor.putString("otherPrefer3Meeting", otherPreferMeeting[3]);
        appConfigEditor.putString("otherPrefer4Meeting", otherPreferMeeting[4]);

        appConfigEditor.putInt("meeting_join_number", currentUser.meeting_join_number);

        if(idUriMeeting[0]!=null && otherNameMeeting[0]!=null)
            appConfigEditor.putString(idUriMeeting[0], otherNameMeeting[0]);

        if(idUriMeeting[1]!=null && otherNameMeeting[1]!=null)
            appConfigEditor.putString(idUriMeeting[1], otherNameMeeting[1]);

        if(idUriMeeting[2]!=null && otherNameMeeting[2]!=null)
            appConfigEditor.putString(idUriMeeting[2], otherNameMeeting[2]);

        if(idUriMeeting[3]!=null && otherNameMeeting[3]!=null)
            appConfigEditor.putString(idUriMeeting[3], otherNameMeeting[3]);

        appConfigEditor.apply();
        appConfigEditor.commit();

    };

    public void getAnotherUserMeeting(){


        outputMeeting[0] = appConfig.getString("output0Meeting","");
        outputMeeting[1] = appConfig.getString("output1Meeting","");
        outputMeeting[2] = appConfig.getString("output2Meeting","");
        outputMeeting[3] = appConfig.getString("output3Meeting","");
        outputMeeting[4] = appConfig.getString("output4Meeting","");

        idUriMeeting[0] = appConfig.getString("idUri0Meeting","");
        idUriMeeting[1] = appConfig.getString("idUri1Meeting","");
        idUriMeeting[2] = appConfig.getString("idUri2Meeting","");
        idUriMeeting[3] = appConfig.getString("idUri3Meeting","");
        idUriMeeting[4] = appConfig.getString("idUri4Meeting","");

        otherNameMeeting[0] = appConfig.getString("otherName0Meeting","");
        otherNameMeeting[1] = appConfig.getString("otherName1Meeting","");
        otherNameMeeting[2] = appConfig.getString("otherName2Meeting","");
        otherNameMeeting[3] = appConfig.getString("otherName3Meeting","");
        otherNameMeeting[4] = appConfig.getString("otherName4Meeting","");

        otherAgeMeeting[0] = appConfig.getString("otherAge0Meeting","");
        otherAgeMeeting[1] = appConfig.getString("otherAge1Meeting","");
        otherAgeMeeting[2] = appConfig.getString("otherAge2Meeting","");
        otherAgeMeeting[3] = appConfig.getString("otherAge3Meeting","");
        otherAgeMeeting[4] = appConfig.getString("otherAge4Meeting","");

        otherSmokeMeeting[0] = appConfig.getString("otherSmoke0Meeting","");
        otherSmokeMeeting[1] = appConfig.getString("otherSmoke1Meeting","");
        otherSmokeMeeting[2] = appConfig.getString("otherSmoke2Meeting","");
        otherSmokeMeeting[3] = appConfig.getString("otherSmoke3Meeting","");
        otherSmokeMeeting[4] = appConfig.getString("otherSmoke4Meeting","");

        otherReligionMeeting[0] = appConfig.getString("otherReligion0Meeting","");
        otherReligionMeeting[1] = appConfig.getString("otherReligion1Meeting","");
        otherReligionMeeting[2] = appConfig.getString("otherReligion2Meeting","");
        otherReligionMeeting[3] = appConfig.getString("otherReligion3Meeting","");
        otherReligionMeeting[4] = appConfig.getString("otherReligion4Meeting","");

        otherJobMeeting[0] = appConfig.getString("otherJob0Meeting","");
        otherJobMeeting[1] = appConfig.getString("otherJob1Meeting","");
        otherJobMeeting[2] = appConfig.getString("otherJob2Meeting","");
        otherJobMeeting[3] = appConfig.getString("otherJob3Meeting","");
        otherJobMeeting[4] = appConfig.getString("otherJob4Meeting","");

        otherLocationMeeting[0] = appConfig.getString("otherLocation0Meeting","");
        otherLocationMeeting[1] = appConfig.getString("otherLocation1Meeting","");
        otherLocationMeeting[2] = appConfig.getString("otherLocation2Meeting","");
        otherLocationMeeting[3] = appConfig.getString("otherLocation3Meeting","");
        otherLocationMeeting[4] = appConfig.getString("otherLocation4Meeting","");

        otherPreferMeeting[0] = appConfig.getString("otherPrefer0Meeting","");
        otherPreferMeeting[1] = appConfig.getString("otherPrefer1Meeting","");
        otherPreferMeeting[2] = appConfig.getString("otherPrefer2Meeting","");
        otherPreferMeeting[3] = appConfig.getString("otherPrefer3Meeting","");
        otherPreferMeeting[4] = appConfig.getString("otherPrefer4Meeting","");
        
        currentUser.meeting_join_number = appConfig.getInt("meeting_join_number",0);


    };

    public void meetingDatabaseChanged(){
        if(!currentUser.isCaptain){

            if(currentUser.isMeeting){
                // layout 1_3

                //미팅 5
                //DB에서 매칭 상대 정보 가져와야함

            } else if(currentUser.isWaiting){
                // layout 1_2
            } else {
                // layout 1_1
                // 기존에 매칭된 상대가 있었다면 매칭이 파기되었다고 알림 떠야함
                // 또한 지금 가지고 있는 매칭정보 초기화
            }

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode==11){
            //Toast.makeText(getApplicationContext(), "Two Button Dialog OK", Toast.LENGTH_SHORT).show();

            mettingConfirmButton.setEnabled(true);
            if(currentAction.equals("subOne")){
                try {
                    boolean check = ohtwoUtil.checkOne();
                    if(check){
                        boolean result = ohtwoUtil.subOne();
                        if(result){
                            freeChat = true;
                            appConfigEditor.putBoolean("freeChat", true);
                            for(int y=0; y<5; y++) {
                                chatOpen[y] = false;
                                appConfigEditor.putBoolean("chatOpen"+y, false);
                            }
                            appConfigEditor.apply();
                            appConfigEditor.commit();

                            Intent intent = new Intent(getApplicationContext(), main_page.class);
                            intent.putExtra("isRefresh","true");
                            intent.putExtra("isRefreshMeeting","false");
                            //intent.putExtra("freeChat",freeChat);
                            startActivity(intent);
                            finish();
                        } else{
                            Toast.makeText(getApplicationContext(), "문제가 발생했습니다. 다시 시도해주세요", Toast.LENGTH_SHORT).show();
                        }
                    } else{
                        currentAction = "gotoCache";
                        Intent intent = new Intent(getApplicationContext(), DialogTwoButton.class);
                        intent.putExtra("contents", "산소가 부족합니다.\n충전 페이지로 이동하시겠습니까?");
                        startActivityForResult(intent, 1);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else if(currentAction.equals("gotoCache")){
                Intent intent = new Intent(getApplicationContext(), CashActivity.class);
                startActivity(intent);
            }else if(currentAction.equals("meetingBack")) {
                //지역을 다시선택해야함
            }else if(currentAction.equals("gotoWait")){
                try {
                    boolean result = ohtwoUtil.subOne();
                    if(result) {
                        setCaptainWaiting(currentUser.id);
                        Toast.makeText(getApplicationContext(), "새로고침에 \n1산소가 차감되었습니다", Toast.LENGTH_SHORT).show();
                    }
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
            } else if(currentAction.equals("meetingNext")) {
                if(currentUser.sex.equals("여자")) {
                    try {
                        String meetingUrl = "http://125.176.119.133:18080/blindDateServer/waitInsert.jsp?id_1=" + currentUser.id + "&sex=" + currentUser.sex + "&latitude=" + currentUser.meeting_latitude + "&longitude=" + currentUser.meeting_longitude + "&location=" + currentUser.meeting_location;
                        if (currentUser.meeting_join_number == 2) {
                            meetingUrl = meetingUrl + "&action=two&id_2=" + currentUser.meetingSelectedFriend.get(0).id;
                        } else if (currentUser.meeting_join_number == 3) {
                            meetingUrl = meetingUrl + "&action=three&id_2=" + currentUser.meetingSelectedFriend.get(0).id + "&id_3=" + currentUser.meetingSelectedFriend.get(1).id;
                        } else if (currentUser.meeting_join_number == 4) {
                            meetingUrl = meetingUrl + "&action=four&id_2=" + currentUser.meetingSelectedFriend.get(0).id + "&id_3=" + currentUser.meetingSelectedFriend.get(1).id + "&id_4=" + currentUser.meetingSelectedFriend.get(2).id;
                        }
                        callJsp.run(meetingUrl);
                        response = callJsp.response;

                        if (response.contains("true")) {
                            setCaptainWaiting(currentUser.id);
                            for (int i = 0; i < currentUser.meeting_join_number - 1; i++) {
                                setOtherWaiting(currentUser.meetingSelectedFriend.get(i).id);
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else if(currentUser.sex.equals("남자")){
                    setCaptainWaiting(currentUser.id);
                    for (int i = 0; i < currentUser.meeting_join_number - 1; i++) {
                        setOtherWaiting(currentUser.meetingSelectedFriend.get(i).id);
                    }
                }
            }else if(currentAction.equals("meetChatStart")) {
                try {
                    String meetingUrl = "http://125.176.119.133:18080/blindDateServer/meetInsert.jsp?id=" + currentUser.id + "&sex=" + currentUser.sex + "&mee=" + currentUser.meeting_join_number + "&wid=" + appConfig.getString("idUri0Meeting", "");
                    //Toast.makeText(getApplicationContext(), appConfig.getString("idUri0Meeting", ""), Toast.LENGTH_SHORT).show();
                    if (currentUser.meeting_join_number == 2) {
                        meetingUrl = meetingUrl + "&mid2=" + appConfig.getString("myFriendId0", "");
                    } else if (currentUser.meeting_join_number == 3) {
                        meetingUrl = meetingUrl + "&mid2=" + appConfig.getString("myFriendId0", "") + "&mid3=" + appConfig.getString("myFriendId1", "");
                    } else if (currentUser.meeting_join_number == 4) {
                        meetingUrl = meetingUrl + "&mid2=" + appConfig.getString("myFriendId0", "") + "&mid3=" + appConfig.getString("myFriendId1", "") + "&mid4=" + appConfig.getString("myFriendId2", "");
                    }

                    callJsp.run(meetingUrl);
                    response = callJsp.response;
                    if (response.contains("true")) {
                        ohtwoUtil.subOne();
                        Toast.makeText(getApplicationContext(), "1산소가 차감되었습니다", Toast.LENGTH_SHORT).show();
                        currentUser.isComplete = true;
                        appConfigEditor.putBoolean("isComplete", true);
                        appConfigEditor.apply();
                        appConfigEditor.commit();


                        ChatModel test = new ChatModel();
                        if(currentUser.meeting_join_number==2) {
                            test.users.put(currentUser.id, true);
                            test.users.put(appConfig.getString("myFriendId0", ""), true);
                            test.users.put(appConfig.getString("idUri0Meeting", ""), true);
                            test.users.put(appConfig.getString("idUri1Meeting", ""), true);
                        }else if(currentUser.meeting_join_number==3) {
                            test.users.put(currentUser.id, true);
                            test.users.put(appConfig.getString("myFriendId0", ""), true);
                            test.users.put(appConfig.getString("myFriendId1", ""), true);
                            test.users.put(appConfig.getString("idUri0Meeting", ""), true);
                            test.users.put(appConfig.getString("idUri1Meeting", ""), true);
                            test.users.put(appConfig.getString("idUri2Meeting", ""), true);
                        }else if(currentUser.meeting_join_number==4) {
                            test.users.put(currentUser.id, true);
                            test.users.put(appConfig.getString("myFriendId0", ""), true);
                            test.users.put(appConfig.getString("myFriendId1", ""), true);
                            test.users.put(appConfig.getString("myFriendId2", ""), true);
                            test.users.put(appConfig.getString("idUri0Meeting", ""), true);
                            test.users.put(appConfig.getString("idUri1Meeting", ""), true);
                            test.users.put(appConfig.getString("idUri2Meeting", ""), true);
                            test.users.put(appConfig.getString("idUri3Meeting", ""), true);
                        }

                        FirebaseDatabase.getInstance().getReference().child("meeting").push().setValue(test);
                        for (int b = 0; b < appConfig.getInt("meeting_join_number", 0) - 1; b++) {
                            setOtherMeeting(appConfig.getString("myFriendId"+b, ""));
                        }

                        if(currentUser.meeting_join_number==2){
                            setWomanCaptainMeeting(appConfig.getString("idUri0Meeting", ""));
                            setOtherMeeting(appConfig.getString("idUri1Meeting", ""));
                            //setOtherMeeting(idUriMeeting[0]);
                            //setOtherMeeting(idUriMeeting[1]);
                        }else if(currentUser.meeting_join_number==3){
                            setWomanCaptainMeeting(appConfig.getString("idUri0Meeting", ""));
                            setOtherMeeting(appConfig.getString("idUri1Meeting", ""));
                            setOtherMeeting(appConfig.getString("idUri2Meeting", ""));
                            //setOtherMeeting(idUriMeeting[0]);
                            //setOtherMeeting(idUriMeeting[1]);
                            //setOtherMeeting(idUriMeeting[2]);
                        }else if(currentUser.meeting_join_number==4){
                            setWomanCaptainMeeting(appConfig.getString("idUri0Meeting", ""));
                            setOtherMeeting(appConfig.getString("idUri1Meeting", ""));
                            setOtherMeeting(appConfig.getString("idUri2Meeting", ""));
                            setOtherMeeting(appConfig.getString("idUri3Meeting", ""));
                            //setOtherMeeting(idUriMeeting[0]);
                            //setOtherMeeting(idUriMeeting[1]);
                            //setOtherMeeting(idUriMeeting[2]);
                            //setOtherMeeting(idUriMeeting[3]);
                        }

                        //채팅방 입장 메서드
                        FirebaseDatabase.getInstance().getReference().child("meeting").orderByChild("users/"+uid).equalTo(true).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                for(DataSnapshot item : dataSnapshot.getChildren()){
                                    ChatModel  chatModel = item.getValue(ChatModel.class);
                                    if(chatModel.users.containsKey(uid)){
                                        dstRoomKey = item.getKey();
                                    }
                                }
                                Intent meetIntent = new Intent(getApplicationContext(),meetChatActivity.class);
                                meetIntent.putExtra("dstRoomID", dstRoomKey);
                                startActivity(meetIntent);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    } else if (response.contains("false")) {
                        Intent intent = new Intent(getApplicationContext(), MeetingOneButton.class);
                        intent.putExtra("contents", "그새 다른팀이 채갔습니다.\n새로고침됩니다.");
                        currentAction = "gotoWait";
                        startActivityForResult(intent,1);

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else if(currentAction.equals("meetExitComplete")){
                if(currentUser.isComplete){ //미팅중 -> meet테이블에서 지우기
                    try {
                        String meetingUrl = "http://125.176.119.133:18080/blindDateServer/meetDelete.jsp?id=" + currentUser.id + "&sex=" + currentUser.sex + "&what=meet";

                        callJsp.run(meetingUrl);
                        response = callJsp.response;

                        if (response.contains("true")) {
                            setNotMeeting(currentUser.id);
                            currentUser.isComplete = false;
                            appConfigEditor.putBoolean("isComplete", false);
                            appConfigEditor.apply();
                            appConfigEditor.commit();

                            for (int i = 0; i < appConfig.getInt("meeting_join_number",0) - 1; i++) {
                                setNotMeeting(appConfig.getString("myFriendId"+i,""));
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //상대팀도 1-1화면으로
                    for(int i=0; i<currentUser.meeting_join_number; i++) {
                        setNotMeeting(appConfig.getString("idUri"+i+"Meeting",""));
                    }

                    currentUser.meeting_join_number=0;

                    appConfigEditor.putInt("meeting_join_number", 0);
                    appConfigEditor.apply();
                    appConfigEditor.commit();

                    FirebaseDatabase.getInstance().getReference().child("meeting").child(currentUser.meetingRoom).removeValue();

                }else if(!currentUser.isComplete){ //대기중에 파토
                    if(currentUser.sex.equals("여자")) { //wait에서 지워야함
                        try {
                            String meetingUrl = "http://125.176.119.133:18080/blindDateServer/meetDelete.jsp?id=" + currentUser.id + "&sex=" + currentUser.sex;
                            if (currentUser.meeting_join_number == 2) {
                                meetingUrl = meetingUrl + "&what=wait2";
                            } else if (currentUser.meeting_join_number == 3) {
                                meetingUrl = meetingUrl + "&what=wait3";
                            } else if (currentUser.meeting_join_number == 4) {
                                meetingUrl = meetingUrl + "&what=wait3";
                            }
                            callJsp.run(meetingUrl);
                            response = callJsp.response;

                            if (response.contains("true")) {
                                setNotMeeting(currentUser.id);
                                for (int i = 0; i < appConfig.getInt("meeting_join_number", 0) - 1; i++) {
                                    setNotMeeting(appConfig.getString("myFriendId" + i, ""));
                                }
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }else if(currentUser.sex.equals("남자")){
                        setNotMeeting(currentUser.id);
                        for (int i = 0; i < appConfig.getInt("meeting_join_number", 0) - 1; i++) {
                            setNotMeeting(appConfig.getString("myFriendId" + i, ""));
                        }
                    }
                }
                //currentUser.member_Number=0;
            }


        } else if(resultCode==12){
            //Toast.makeText(getApplicationContext(), "Two Button Dialog Cancel", Toast.LENGTH_SHORT).show();
            mettingConfirmButton.setEnabled(true);

        } else if(resultCode==21){
            if(currentAction.equals("gotoWait")){
                setCaptainWaiting(currentUser.id);
            }else{

            }
            //Toast.makeText(getApplicationContext(), "One Button Dialog OK", Toast.LENGTH_SHORT).show();
        } else{
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    //font
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void loadFriend(){
        try {
            FriendInfoArrayList.clear();
            callJsp.run("http://125.176.119.133:18080/blindDateServer/friendList.jsp?id="+currentUser.id);
            response = callJsp.response;

            int count = 0; //도착한 친구요청 숫자
            for(int i=0; i<response.length(); i++)
            {
                if(response.charAt(i) == '}') {
                    count++;
                }
            }

            JSONObject[] outputJson = new JSONObject[20];
            String[] outputId = new String[20];
            String[] outputName = new String[20];

            if (count!=0){
                response = response.substring(response.indexOf("[") + 1, response.lastIndexOf("]"));
                String[] responseArray = response.split("\\},");

                for (int i = 0; i < responseArray.length; i++) {
                    responseArray[i] = responseArray[i] + "}";
                    outputJson[i] = new JSONObject(responseArray[i]);
                    outputId[i] = outputJson[i].getString("id");
                    outputName[i] = outputJson[i].getString("name");
                    apacheCall.imageDownload(outputId[i], main_page_activity);
                    FriendInfoArrayList.add(new FriendInfo(outputId[i],outputName[i]));
                    appConfigEditor.putString(outputId[i], outputName[i]);

                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        appConfigEditor.apply();
        appConfigEditor.commit();

        friendListAdapter = new FriendListAdapter(FriendInfoArrayList);
        metting_recycler_view.setAdapter(friendListAdapter);
        currentUser.member_Number = 0;
        meetingNumberText.setText(currentUser.member_Number+"명");

    }

    public void setCaptainWaiting(String uid) {

        Map<String,Object> map = new HashMap<>();
        map.put("isCaptain", true);
        map.put("isWaiting", true);
        map.put("isMeeting", false);
        FirebaseDatabase.getInstance().getReference().child("users").child(uid).updateChildren(map);

    }

    public void setOtherWaiting(String uid) {

        Map<String,Object> map = new HashMap<>();
        map.put("isCaptain", false);
        map.put("isWaiting", true);
        map.put("isMeeting", false);
        FirebaseDatabase.getInstance().getReference().child("users").child(uid).updateChildren(map);

    }

    public void setCaptainMeeting(String uid) {

        Map<String,Object> map = new HashMap<>();
        map.put("isCaptain", true);
        map.put("isWaiting", false);
        map.put("isMeeting", true);
        FirebaseDatabase.getInstance().getReference().child("users").child(uid).updateChildren(map);

    }

    public void setOtherMeeting(String uid) {

        Map<String,Object> map = new HashMap<>();
        map.put("isCaptain", false);
        map.put("isWaiting", false);
        map.put("isMeeting", true);
        FirebaseDatabase.getInstance().getReference().child("users").child(uid).updateChildren(map);

    }

    public void setWomanCaptainMeeting(String uid) {

        Map<String,Object> map = new HashMap<>();
        map.put("isCaptain", true);
        map.put("isWaiting", false);
        map.put("isMeeting", true);
        FirebaseDatabase.getInstance().getReference().child("users").child(uid).updateChildren(map);

    }

    public void setNotMeeting(String uid) {

        Map<String,Object> map = new HashMap<>();
        map.put("isCaptain", false);
        map.put("isWaiting", false);
        map.put("isMeeting", false);
        FirebaseDatabase.getInstance().getReference().child("users").child(uid).updateChildren(map);

    }


    public boolean getOtherIsCaptain(String uid){

        final boolean[] isCaptain = new boolean[1];

        FirebaseDatabase.getInstance().getReference().child("users").child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserModel userModel = dataSnapshot.getValue(UserModel.class);
                isCaptain[0] = userModel.isCaptain;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return isCaptain[0];

    }

    public boolean getOtherIsWaiting(String uid){

        final boolean[] isWaiting = new boolean[1];

        FirebaseDatabase.getInstance().getReference().child("users").child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserModel userModel = dataSnapshot.getValue(UserModel.class);
                isWaiting[0] = userModel.isWaiting;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return isWaiting[0];

    }
    public boolean getOtherIsMeeting(String uid){

        final boolean[] isMeeting = new boolean[1];

        FirebaseDatabase.getInstance().getReference().child("users").child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserModel userModel = dataSnapshot.getValue(UserModel.class);
                isMeeting[0] = userModel.isMeeting;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return isMeeting[0];

    }

}
