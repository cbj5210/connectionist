package connectionist.ohtwo;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static connectionist.ohtwo.main_page.appConfigEditor;
import static connectionist.ohtwo.main_page.idUriMeeting;

public class meetChatActivity extends AppCompatActivity {

    Map<String, UserModel> users = new HashMap<>();

    List<ChatModel.Message> chat_Messages = new ArrayList<>();

    private String dstRoom;
    private String uid = currentUser.id;
    private EditText input_msg_meeting;
    private RecyclerView recyclerView_meeting;
    private ImageView chat_out_meeting;
    private ValueEventListener valueEventListener;
    private DatabaseReference databaseReference;

    String pushToken;
    String userID_token;

    ImageView send_msg_meeting;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");

    SharedPreferences appConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meet_chat);

        appConfig = getSharedPreferences("appConfig", MODE_PRIVATE);

        dstRoom = getIntent().getStringExtra("dstRoomID"); // 미팅방 키값 가져오기
        currentUser.meetingRoom = new String(dstRoom);
        input_msg_meeting = (EditText)findViewById(R.id.input_msg_meeting);


        FirebaseDatabase.getInstance().getReference().child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                users = (Map<String, UserModel>) dataSnapshot.getValue();
                meetChat();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        FirebaseDatabase.getInstance().getReference().child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot item : dataSnapshot.getChildren()) {
                    users.put(item.getKey(),item.getValue(UserModel.class));
                }
                meetChat();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        recyclerView_meeting = (RecyclerView)findViewById(R.id.msg_recyclerView_meeting);
        recyclerView_meeting.setAdapter(new MeetChatRecyclerViewAdapter());
        recyclerView_meeting.setLayoutManager(new LinearLayoutManager(meetChatActivity.this));

        chat_out_meeting = findViewById(R.id.chat_out_meeting);

        chat_out_meeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent intent = new Intent(getApplicationContext(), MeetingTwoButton.class);
                intent.putExtra("contents", "상대방과의 모든 대화가 지워지고\n대화방이 삭제됩니다.\n그래도 진행하시겠습니까?");
                startActivityForResult(intent, 1);*/

                Toast.makeText(getApplicationContext(), "매칭 화면으로 돌아가면 미팅방을 삭제할 수 있습니다.", Toast.LENGTH_SHORT).show();

                //todo 방 폭파

            }
        });

    }
    void meetChat(){
        send_msg_meeting = findViewById(R.id.send_msg_meeting);
        send_msg_meeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatModel.Message message = new ChatModel.Message();
                message.uid = uid;
                message.message = input_msg_meeting.getText().toString();
                message.timestamp = ServerValue.TIMESTAMP;

                if(message.message.length() != 0 && ohtwoUtil.checkWord(message.message)){

                    // 공백 전송 버그 수정
                    FirebaseDatabase.getInstance().getReference().child("meeting").child(dstRoom).child("message").push().setValue(message).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            sendFcm(input_msg_meeting.getText().toString());
                            input_msg_meeting.setText("");  // editText 초기화
                        }});
                }
            }
        });
    }
    void sendFcm(final String inputMsg){

        FirebaseDatabase.getInstance().getReference().child("meeting").orderByChild("users/"+uid).equalTo(true).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot item : dataSnapshot.getChildren()) {
                    ChatModel chatModel = item.getValue(ChatModel.class);
                    Map<String, Boolean> chatUsers = new HashMap<>();
                    chatUsers = chatModel.users;
                    for (Map.Entry<String, Boolean> entry : chatUsers.entrySet()) {

                        userID_token = entry.getKey();

                        if(userID_token != currentUser.id){

                            FirebaseDatabase.getInstance().getReference().child("users").child(userID_token).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    UserModel pushUserModel = dataSnapshot.getValue(UserModel.class);
                                    pushToken = pushUserModel.pushToken;

                                    Gson gson = new Gson();
                                    NotificationModel notificationModel = new NotificationModel();
                                    notificationModel.to = pushToken;
                                    notificationModel.notification.title = currentUser.name;
                                    notificationModel.notification.text = inputMsg;
                                    notificationModel.notification.tag = "52";
                                    notificationModel.data.title = currentUser.name;
                                    notificationModel.data.text = inputMsg;
                                    notificationModel.data.tag = "52";

                                    RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf8"),gson.toJson(notificationModel));

                                    Request request = new Request.Builder()
                                            .header("Content-Type", "application/json")
                                            .addHeader("Authorization", "key=AIzaSyDmm_tWYRY5v95sO8iYTSpbTV4ERzzzIZc")
                                            .url("https://fcm.googleapis.com/fcm/send")
                                            .post(requestBody)
                                            .build();

                                    OkHttpClient okHttpClient = new OkHttpClient();
                                    okHttpClient.newCall(request).enqueue(new Callback() {
                                        @Override
                                        public void onFailure(Call call, IOException e) {

                                        }

                                        @Override
                                        public void onResponse(Call call, Response response) throws IOException {

                                        }
                                    });
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode==11){
            //Toast.makeText(getApplicationContext(), "Two Button Dialog OK", Toast.LENGTH_SHORT).show();

            if(dstRoom != null){

                try {
                    String meetingUrl = "http://125.176.119.133:18080/blindDateServer/meetDelete.jsp?id=" + currentUser.id + "&sex=" + currentUser.sex + "&what=meet";

                    String response = "";
                    callJsp.run(meetingUrl);
                    response = callJsp.response;

                    if (response.contains("true")) {
                        setNotMeeting(currentUser.id);
                        currentUser.isComplete = false;
                        appConfigEditor.putBoolean("isComplete", false);
                        appConfigEditor.apply();
                        appConfigEditor.commit();


                        for (int i = 0; i < currentUser.meeting_join_number - 1; i++) {
                            setNotMeeting(currentUser.meetingSelectedFriend.get(i).id);
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //상대팀도 1-1화면으로
                for(int i=0; i<currentUser.meeting_join_number-1; i++) {
                    setNotMeeting(currentUser.meetingSelectedFriend.get(i).id);
                    if(i==currentUser.meeting_join_number-1) {
                        setNotMeeting(idUriMeeting[i]);
                        setNotMeeting(idUriMeeting[i + 1]);
                    }
                    else {
                        setNotMeeting(idUriMeeting[i]);
                    }
                }

                currentUser.meeting_join_number=0;

                appConfigEditor.putInt("meeting_join_number", 0);
                appConfigEditor.apply();
                appConfigEditor.commit();
                FirebaseDatabase.getInstance().getReference().child("meeting").child(dstRoom).removeValue();
                finish();
            }
            else{
                AlertDialog.Builder builder = new AlertDialog.Builder(meetChatActivity.this);
                builder.setTitle("나갈 채팅방이 존재하지 않습니다.")
                        .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).show();
            }

        } else if(resultCode==12){
            //Toast.makeText(getApplicationContext(), "Two Button Dialog Cancel", Toast.LENGTH_SHORT).show();
        } else if(resultCode==21){
            //Toast.makeText(getApplicationContext(), "One Button Dialog OK", Toast.LENGTH_SHORT).show();
        } else{
            super.onActivityResult(requestCode, resultCode, data);
        }

    }


    class MeetChatRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

        public MeetChatRecyclerViewAdapter(){
            getMessageList();
        }
        private void getMessageList(){

            databaseReference = FirebaseDatabase.getInstance().getReference().child("meeting").child(dstRoom).child("message");
            valueEventListener = databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    chat_Messages.clear();
                    Map<String, Object> readUserMap = new HashMap<>();
                    for(DataSnapshot item : dataSnapshot.getChildren()){
                        String key = item.getKey();
                        chat_Messages.add(item.getValue(ChatModel.Message.class));
                    }
                    // 메세지 갱신
                    notifyDataSetChanged();
                    recyclerView_meeting.scrollToPosition(chat_Messages.size() -1);
                    // 스크롤을 가장 아래로
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message,parent,false);

            return new MeetChatViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

            MeetChatViewHolder messageViewHolder = ((MeetChatViewHolder)holder);

            // 내가 보낸 msg
            if(chat_Messages.get(position).uid.equals((uid))){
                messageViewHolder.textView_msg.setText(chat_Messages.get(position).message);
                messageViewHolder.textView_msg.setBackgroundResource(R.drawable.chat_box);
                messageViewHolder.textView_name.setText(""); // 상대방 이름 보이게 구현
                messageViewHolder.linearLayout_dst.setVisibility(View.INVISIBLE);
                messageViewHolder.outerLinearLayout.setGravity(Gravity.RIGHT);
            }
            else {
                /*
                Glide.with(holder.itemView.getContext())
                        .load(imageUri)
                        .asBitmap()
                        .into(((MessageViewHolder) holder).other_user_img);
                */
                messageViewHolder.linearLayout_dst.setVisibility(View.VISIBLE);

                String imgFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/.connectionist/"+chat_Messages.get(position).uid+"/user.jpg";
                Drawable drawable = Drawable.createFromPath(imgFilePath);
                ((MeetChatViewHolder) holder).other_user_img.setImageDrawable(drawable);

                //messageViewHolder.textView_name.setText(users.get(chat_Messages.get(position).uid).userName); // 상대방 이름 보이게 구현
                String tempName = appConfig.getString(chat_Messages.get(position).uid, "이름없음");
                messageViewHolder.textView_name.setText(tempName);

                messageViewHolder.textView_msg.setText(chat_Messages.get(position).message);
                messageViewHolder.textView_msg.setBackgroundResource(R.drawable.chat_box);
                messageViewHolder.outerLinearLayout.setGravity(Gravity.LEFT);
            }
            long unixTime = (long) chat_Messages.get(position).timestamp;
            Date date = new Date(unixTime);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
            String time = simpleDateFormat.format(date);
            messageViewHolder.textView_time.setText(time);
        }

        @Override
        public int getItemCount() {
            return chat_Messages.size();
        }

        private class MeetChatViewHolder extends RecyclerView.ViewHolder {

            public TextView textView_msg;
            public TextView textView_name;
            public TextView textView_time;
            public ImageView other_user_img;
            public LinearLayout linearLayout_dst;
            public LinearLayout outerLinearLayout;

            public MeetChatViewHolder(View view) {
                super(view);
                textView_msg = (TextView)view.findViewById(R.id.msg_View);
                textView_name = (TextView)view.findViewById(R.id.msg_Name);
                textView_time = (TextView)view.findViewById(R.id.message_time);
                other_user_img = (ImageView)view.findViewById(R.id.other_user_img);
                linearLayout_dst = (LinearLayout)view.findViewById(R.id.msg_LinearLayout);
                outerLinearLayout = (LinearLayout)view.findViewById(R.id.msg_OuterLinearLayout);

            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(valueEventListener != null){
            databaseReference.removeEventListener(valueEventListener);
        }
        finish();
    }

    public void setNotMeeting(String uid) {

        Map<String,Object> map = new HashMap<>();
        map.put("isCaptain", false);
        map.put("isWaiting", false);
        map.put("isMeeting", false);
        FirebaseDatabase.getInstance().getReference().child("users").child(uid).updateChildren(map);

    }
}
