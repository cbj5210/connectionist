package connectionist.ohtwo;

import android.content.Context;
import android.widget.Toast;

import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.OAuthLoginHandler;

public class naverHandler extends OAuthLoginHandler {

    private Context mContext;
    private loginActivity activity;
    private OAuthLogin mOAuthLoginModule;


    naverHandler(Context mContext, OAuthLogin mOAuthLoginModule, loginActivity activity) {
        this.mContext = mContext;
        this.mOAuthLoginModule = mOAuthLoginModule;
        this.activity = activity;
    }

    @Override
    public void run(boolean success) {
        if (success) {

            if(mOAuthLoginModule==null){
                System.out.println("mContext : " + mContext);
                System.out.println("mOAuthLoginModule : " + mOAuthLoginModule);
            } else {
                String accessToken = mOAuthLoginModule.getAccessToken(mContext);
                // 얘가 있어야 유저 정보를 가져올 수 있습니다.
                naverProfileTask task = new naverProfileTask(mContext, mOAuthLoginModule, GlobalApplication.loadingActivity);
                // 이 클래스가 유저정보를 가져오는 업무를 담당합니다.
                task.execute(accessToken);
            }

        } else {
            String errorCode = mOAuthLoginModule.getLastErrorCode(mContext).getCode();
            String errorDesc = mOAuthLoginModule.getLastErrorDesc(mContext);
            Toast.makeText(mContext, "errorCode:" + errorCode
                    + ", errorDesc:" + errorDesc, Toast.LENGTH_SHORT).show();
        }
    }

}
