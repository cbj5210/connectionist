package connectionist.ohtwo;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.nhn.android.naverlogin.OAuthLogin;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

class naverProfileTask extends AsyncTask<String, Void, String> {
    String result;

    private Context mContext;
    private loadingActivity activity;
    private OAuthLogin mOAuthLoginModule;

    naverProfileTask(Context mContext, OAuthLogin mOAuthLoginModule, loadingActivity activity) {
        this.mContext = mContext;
        this.mOAuthLoginModule = mOAuthLoginModule;
        this.activity = activity;
    }

    @Override
    protected String doInBackground(String... strings) {
        String token = strings[0];// 네이버 로그인 접근 토큰;
        String header = "Bearer " + token; // Bearer 다음에 공백 추가
        try {
            String apiURL = "https://openapi.naver.com/v1/nid/me";
            URL url = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Authorization", header);
            int responseCode = con.getResponseCode();
            BufferedReader br;
            if(responseCode==200) { // 정상 호출
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } else {  // 에러 발생
                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = br.readLine()) != null) {
                response.append(inputLine);
            }
            result = response.toString();
            br.close();
            System.out.println(response.toString());
        } catch (Exception e) {
            System.out.println(e);
        }
        //result -> JSONObject
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try {
            JSONObject object = new JSONObject(result);
            if(object.getString("resultcode").equals("00")) {
                JSONObject jsonObject = new JSONObject(object.getString("response"));
                Log.d("jsonObject", jsonObject.toString());

                currentUser.id = "naver"+ jsonObject.getString("id");

                //동일 핸드폰 번호로 다른 계정이 있는지 확인
                String anotherAccount = callJsp.existPhoneId(currentUser.phone);
                if(!anotherAccount.equals(currentUser.id) && !anotherAccount.equals("needJoin") && !currentUser.phone.equals("+15555215554") && !currentUser.phone.equals("15555215554")) {
                    Toast.makeText(GlobalApplication.loadingActivity, "이미 다른 소셜 로그인 계정이 있습니다. \n해당 계정을 선택해주세요", Toast.LENGTH_SHORT).show();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            loadingActivity.naverOAuthLoginModule.logoutAndDeleteToken(mContext); //naver
                        }
                    }).start();

                    Intent intentReturn = new Intent(GlobalApplication.getGlobalApplicationContext(), loginActivity.class);
                    GlobalApplication.getGlobalApplicationContext().startActivity(intentReturn);
                } else {

                    //없다면 디비에서 값을 읽어옴
                    boolean ok = callJsp.setCurrentUserInfo(currentUser.id);

                    if(!ok){
                        Intent intentJoin = new Intent(GlobalApplication.getGlobalApplicationContext(), tosActivity.class);
                        GlobalApplication.getGlobalApplicationContext().startActivity(intentJoin);
                    } else if (!currentUser.signed.equals("1") ) {
                        //goto join
                        Intent intentJoin = new Intent(GlobalApplication.getGlobalApplicationContext(), tosActivity.class);
                        GlobalApplication.getGlobalApplicationContext().startActivity(intentJoin);
                    } else {
                        if(currentUser.isReported){
                            Intent intentMain = new Intent(GlobalApplication.loadingActivity, reportUserActivity.class);
                            GlobalApplication.loadingActivity.startActivity(intentMain);
                        } else{
                            Intent intentMain = new Intent(GlobalApplication.loadingActivity, main_page.class);
                            //Intent intentMain = new Intent(GlobalApplication.loadingActivity, OpenWaitActivity.class);
                            intentMain.putExtra("isRefresh","false");
                            intentMain.putExtra("isRefreshMeeting","false");
                            GlobalApplication.loadingActivity.startActivity(intentMain);
                        }
                    }

                }
/*                Intent intent = new Intent(activity, main_page.class);
                intent.putExtra("email", jsonObject.getString("email"));
                intent.putExtra("name", jsonObject.getString("name"));
                intent.putExtra("nickname", jsonObject.getString("nickname"));
                intent.putExtra("profile", jsonObject.getString("profile_image"));
                activity.startActivity(intent);*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}