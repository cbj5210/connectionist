package connectionist.ohtwo;

import android.content.Context;
import android.location.Location;
import android.os.Vibrator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.VIBRATOR_SERVICE;

public class ohtwoUtil {

    public static String encThreadReturn = "false";
    public static String encThreadResult = "false";

    public static double getDistance(double lat1 , double lng1 , double lat2 , double lng2 ){
        double distance;

        Location locationA = new Location("point A");
        locationA.setLatitude(lat1);
        locationA.setLongitude(lng1);

        Location locationB = new Location("point B");
        locationB.setLatitude(lat2);
        locationB.setLongitude(lng2);

        distance = locationA.distanceTo(locationB);

        return distance;
    }


    /////cash action/////

    public static boolean checkOne() throws InterruptedException {
        encThread eT = new encThread("check", "one");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean checkFive() throws InterruptedException {
        encThread eT = new encThread("check", "five");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean checkThree() throws InterruptedException {
        encThread eT = new encThread("check", "three");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean checkTen() throws InterruptedException {
        encThread eT = new encThread("check", "ten");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean addOne() throws InterruptedException {
        encThread eT = new encThread("add", "one");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean addThree() throws InterruptedException {
        encThread eT = new encThread("add", "three");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean addFive() throws InterruptedException {
        encThread eT = new encThread("add", "five");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean addTen() throws InterruptedException {
        encThread eT = new encThread("add", "ten");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean addTwenty() throws InterruptedException {
        encThread eT = new encThread("add", "twenty");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean addHundred() throws InterruptedException {
        encThread eT = new encThread("add", "hundred");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean addTwoHundred() throws InterruptedException {
        encThread eT = new encThread("add", "twohundred");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean addFiveHundred() throws InterruptedException {
        encThread eT = new encThread("add", "fivehundred");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean subOne() throws InterruptedException {
        encThread eT = new encThread("sub", "one");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean subThree() throws InterruptedException {
        encThread eT = new encThread("sub", "three");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }

    public static boolean subTen() throws InterruptedException {
        encThread eT = new encThread("sub", "ten");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true")){
            currentUser.cash = ohtwoUtil.encThreadResult;
            main_page.ohtwoMainSanso.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            main_page.ohtwoMainSansoMeeting2.setText("산소 " + ohtwoUtil.encThreadResult + "+");
            if(CashActivity.cacheSansoText!=null)
                CashActivity.cacheSansoText.setText("내 산소 " + ohtwoUtil.encThreadResult);
            return true;
        } else
            return false;
    }
    /////////////////////
    public static boolean refreshItem() throws InterruptedException {
        encThread eT = new encThread("refreshitem", "null");
        eT.start();
        eT.join();

        if(encThreadReturn.equals("true"))
            return true;
        else
            return false;
    }


    public static boolean calRefresh(){
        if(currentUser.isRefreshItem.equals("")){
            return false;
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");

            Calendar cal = Calendar.getInstance();
            String server = sdf.format(cal.getTime());

            Date serverDate = null;
            Date curDate = null;

            try {
                serverDate = sdf.parse(server);
                curDate = sdf.parse(currentUser.isRefreshItem);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return false;
            }

            if(serverDate.getTime()<=curDate.getTime())
                return true;
            else
                return false;

        }

    }

    public static boolean calToday(){
        if(currentUser.lastLogin.equals("")){
            return true;
        } else {


            if(currentUser.serverTime!=null && !currentUser.serverTime.equals("") && !currentUser.serverTime.equals(currentUser.lastLogin))
                return true;
            else
                return false;

        }

    }

    public static boolean callReported(){
        if(currentUser.reportDay.equals("")){
            return false;
        } else {

            Date reportDay;
            Date serverDay;
            //cal
            try {
                reportDay = new SimpleDateFormat("yyyy/MM/dd HH:mm").parse(currentUser.reportDay);
                serverDay = new SimpleDateFormat("yyyy/MM/dd HH:mm").parse(currentUser.serverTimeMinute);

                if(serverDay.compareTo(reportDay) == 1){
                    return false;
                } else {
                    return true;
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return true;

    }

    public static void cleanCurrentUser(){
        currentUser.id="";
        currentUser. signed="";
        currentUser.name="";
        currentUser.sex="";
        currentUser.latitude=0;
        currentUser.longitude=0;
        currentUser.location="";
        currentUser.age="";
        currentUser.job="";
        currentUser.smoke="";
        currentUser.religion="";
        currentUser.cash="";
        currentUser.preferage="";
        currentUser.phone="";

        currentUser.isRefreshItem="";
        currentUser.hasRefreshItem=false;

        currentUser.lastLogin="";
        currentUser.isTodayLogin=false;

        currentUser.hasNotice=false;
        currentUser.noticeMessage="";


        currentUser.dbPhone="";
    }

    public static String filterWord[] = {"간나새끼", "갈보", "개간년", "개나리", "개년", "개돼지", "개씹", "개좆", "개좇", "걸레년", "과연", "광년", "김치녀", "느개비", "느금마", "딸딸이", "메갈", "메갈리아", "보슬", "보슬아치", "보지", "불알", "부랄", "빨통", "시발", "씨발", "시팔", "씨팔", "색스", "쌕스", "샊스", "색쓰", "쌖스", "쌕쓰", "쌖쓰", "색수", "쌕수", "샊수", "색쑤", "쌖수", "쌕쑤", "쌖쑤", "섹스", "쎅스", "섺스", "섹쓰", "쎆스", "쎅쓰", "쎅쓰", "섹수", "쎅수", "섺수", "섹쑤", "쎆수", "쎅쑤", "쎆쑤",  "씨부랄", "시부랄", "시브랄", "씨브랄", "씹새", "씹창", "씹년", "아가리", "애자", "애비", "애미", "엠창", "엄창", "자지", "자위", "좆물", "좇물", "정액", "좇대가리", "좇대가리", "좆집", "좇집", "창녀", "창놈", "한남", "한녀", "후장", "후빨", "sex"};

    public static boolean checkWord(String inputString) {

        for (int i = 0; i < filterWord.length; i++){

            if(inputString.indexOf(filterWord[i])>-1)
                return false;
        }

        return true;

    }

    public static void vibration(Context context){

        Vibrator vib = (Vibrator)context.getSystemService(VIBRATOR_SERVICE);
        vib.vibrate(1000);

    }

    public static void cashError(String id, String item, String bill){

        reportThread rt = new reportThread(id, item, bill);
        rt.start();

    }

}
