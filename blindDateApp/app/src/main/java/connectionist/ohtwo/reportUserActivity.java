package connectionist.ohtwo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class reportUserActivity extends AppCompatActivity {

    TextView reportText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_user);

        reportText = findViewById(R.id.reportText);
        reportText.setText(currentUser.reportDay);

    }
}
