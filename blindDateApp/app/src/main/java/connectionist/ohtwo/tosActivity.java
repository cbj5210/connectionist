package connectionist.ohtwo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fsn.cauly.CaulyAdInfo;
import com.fsn.cauly.CaulyAdInfoBuilder;
import com.fsn.cauly.CaulyCloseAd;
import com.fsn.cauly.CaulyCloseAdListener;

public class tosActivity extends AppCompatActivity implements CaulyCloseAdListener {

    private final long FINISH_INTERVAL_TIME = 500;
    private long backPressedTime = 0;

    ImageView tos_button_1;
    ImageView tos_button_2;

    ImageView tos_checkBoxALL;
    ImageView tos_checkBox1;
    ImageView tos_checkBox2;
    ImageView tos_checkBox3;
    ImageView tos_checkBox4;

    boolean isCheckAll = false;
    boolean isCheck1 = false;
    boolean isCheck2 = false;
    boolean isCheck3 = false;
    boolean isCheck4 = false;
    TextView tos_confirm_button;

    CaulyCloseAd mCloseAd ;

    private static final String APP_CODE = "cYoXgrqI"; // 광고 요청 코드 ( 자신의 APP_CODE 쓸 것 )

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tos);



        CaulyAdInfo closeAdInfo = new CaulyAdInfoBuilder(APP_CODE).build();
        mCloseAd = new CaulyCloseAd();
        mCloseAd.setButtonText("취소", "종료");
        mCloseAd.setDescriptionText("종료하시겠습니까?");
        mCloseAd.setAdInfo(closeAdInfo);
        mCloseAd.setCloseAdListener(this); // CaulyCloseAdListener 등록
        // 종료광고 노출 후 back버튼 사용을 막기 원할 경우 disableBackKey();을 추가한다
        // mCloseAd.disableBackKey();

        tos_button_1 = findViewById(R.id.tos_button_1);
        tos_button_2 = findViewById(R.id.tos_button_2);

        tos_checkBoxALL = findViewById(R.id.tos_checkBoxALL);

        tos_checkBoxALL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isCheckAll){
                    tos_checkBoxALL.setImageResource(R.drawable.check);
                    tos_checkBox1.setImageResource(R.drawable.check);
                    tos_checkBox2.setImageResource(R.drawable.check);
                    tos_checkBox3.setImageResource(R.drawable.check);
                    tos_checkBox4.setImageResource(R.drawable.check);

                    isCheckAll=true;
                    isCheck1=true;
                    isCheck2=true;
                    isCheck3=true;
                    isCheck4=true;

                } else{

                    tos_checkBoxALL.setImageResource(R.drawable.check_no);
                    tos_checkBox1.setImageResource(R.drawable.check_no);
                    tos_checkBox2.setImageResource(R.drawable.check_no);
                    tos_checkBox3.setImageResource(R.drawable.check_no);
                    tos_checkBox4.setImageResource(R.drawable.check_no);

                    isCheckAll=false;
                    isCheck1=false;
                    isCheck2=false;
                    isCheck3=false;
                    isCheck4=false;

                }
            }
        });

        tos_checkBox1 = findViewById(R.id.tos_checkBox1);

        tos_checkBox1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!isCheck1){
                    tos_checkBox1.setImageResource(R.drawable.check);
                    isCheck1=true;
                } else{
                    tos_checkBox1.setImageResource(R.drawable.check_no);
                    isCheck1=false;
                }

            }
        });

        tos_checkBox2 = findViewById(R.id.tos_checkBox2);

        tos_checkBox2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!isCheck2){
                    tos_checkBox2.setImageResource(R.drawable.check);
                    isCheck2=true;
                } else{
                    tos_checkBox2.setImageResource(R.drawable.check_no);
                    isCheck2=false;
                }

            }
        });


        tos_checkBox3 = findViewById(R.id.tos_checkBox3);

        tos_checkBox3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!isCheck3){
                    tos_checkBox3.setImageResource(R.drawable.check);
                    isCheck3=true;
                } else{
                    tos_checkBox3.setImageResource(R.drawable.check_no);
                    isCheck3=false;
                }

            }
        });


        tos_checkBox4 = findViewById(R.id.tos_checkBox4);

        tos_checkBox4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!isCheck4){
                    tos_checkBox4.setImageResource(R.drawable.check);
                    isCheck4=true;
                } else{
                    tos_checkBox4.setImageResource(R.drawable.check_no);
                    isCheck4=false;
                }

            }
        });

        tos_confirm_button = findViewById(R.id.tos_confirm_button);

        tos_button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://125.176.119.133:18080/Connectionist_termsofservice.htm"));
                startActivity(myIntent);
            }
        });

        tos_button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://125.176.119.133:18080/Connectionist_individual.html"));
                startActivity(myIntent);
            }
        });

        tos_confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isCheck1 && isCheck2 && isCheck3 && isCheck4){
                    Intent intent = new Intent(getBaseContext(), userJoinImageActivity.class);
                    startActivity(intent);
                } else{
                    Toast.makeText(getApplicationContext(), "이용약관등에 모두 동의해주세요!", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }
////cauly

    // Back Key가 눌러졌을 때, CloseAd 호출
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
            // 앱을 처음 설치하여 실행할 때, 필요한 리소스를 다운받았는지 여부.
            if (mCloseAd.isModuleLoaded())
            {
                mCloseAd.show(this);
            }
            else
            {
                // 광고에 필요한 리소스를 한번만  다운받는데 실패했을 때 앱의 종료팝업 구현
                showDefaultClosePopup();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showDefaultClosePopup()
    {
        new AlertDialog.Builder(this).setTitle("").setMessage("종료 하시겠습니까?")
                .setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        finishAffinity();
                        System.runFinalization();
                        System.exit(0);
                    }
                })
                .setNegativeButton("아니요",null)
                .show();
    }

    // CaulyCloseAdListener
    @Override
    public void onFailedToReceiveCloseAd(CaulyCloseAd ad, int errCode, String errMsg) {


    }
    // CloseAd의 광고를 클릭하여 앱을 벗어났을 경우 호출되는 함수이다.
    @Override
    public void onLeaveCloseAd(CaulyCloseAd ad) {
    }
    // CloseAd의 request()를 호출했을 때, 광고의 여부를 알려주는 함수이다.
    @Override
    public void onReceiveCloseAd(CaulyCloseAd ad, boolean isChargable) {

    }
    //왼쪽 버튼을 클릭 하였을 때, 원하는 작업을 수행하면 된다.
    @Override
    public void onLeftClicked(CaulyCloseAd ad) {

    }
    //오른쪽 버튼을 클릭 하였을 때, 원하는 작업을 수행하면 된다. (종료창에서 예 눌렀을때)
    //Default로는 오른쪽 버튼이 종료로 설정되어있다.
    @Override
    public void onRightClicked(CaulyCloseAd ad) {
        finishAffinity();
        System.runFinalization();
        System.exit(0);
    }
    @Override
    public void onShowedCloseAd(CaulyCloseAd ad, boolean isChargable) {
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCloseAd != null)
            mCloseAd.resume(this); // 필수 호출
    }
    ////


    /*@Override
    public void onBackPressed() {
        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;

        if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime)
        {
            finishAffinity();
            System.runFinalization();
            System.exit(0);

        }
        else
        {
            backPressedTime = tempTime;
            Toast.makeText(getApplicationContext(), "\'뒤로\'버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
        }
    }*/

}
