package connectionist.ohtwo;

import android.graphics.Bitmap;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class uploadThread extends Thread{

    String id;
    String action="";
    Bitmap bitmap;
    int selection=0;
    File voiceFile;

    Handler handler = new Handler();

    public uploadThread(String id, String action) {
        this.id = id;
        this.action = action;
    }

    public uploadThread(String id, String action, Bitmap bitmap, int selection) {
        this.id = id;
        this.action = action;
        this.bitmap = bitmap;
        this.selection = selection;
    }

    @Override
    public void run() {
        if(action.equals("image"))
            imageUpload();
        else
            voiceUpload();
    }

    public void imageUpload(){

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);
        byte [] ba = bao.toByteArray();
        String ba1 = Base64.encodeToString(ba, Base64.DEFAULT);
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("image", ba1));
        nameValuePairs.add(new BasicNameValuePair("id", id));
        nameValuePairs.add(new BasicNameValuePair("selection", Integer.toString(selection)));
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost("http://125.176.119.133:18080/imageUpload.php");
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = client.execute(post);
            //HttpEntity entity = response.getEntity();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void voiceUpload(){
        voiceFile = new File(Environment.getExternalStorageDirectory() + "/.connectionist/" + id + "/user.3gp");
        byte[] ba = new byte[(int) voiceFile.length()];
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(voiceFile);
            fis.read(ba); //read file into bytes[]
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String ba1 = Base64.encodeToString(ba, Base64.DEFAULT);
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("voice", ba1));
        nameValuePairs.add(new BasicNameValuePair("id", id));

        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost("http://125.176.119.133:18080/voiceUpload.php");

            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = client.execute(post);
            //HttpEntity entity = response.getEntity();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}

