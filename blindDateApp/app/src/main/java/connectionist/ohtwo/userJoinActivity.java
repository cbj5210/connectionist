package connectionist.ohtwo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fsn.cauly.CaulyAdInfo;
import com.fsn.cauly.CaulyAdInfoBuilder;
import com.fsn.cauly.CaulyCloseAd;
import com.fsn.cauly.CaulyCloseAdListener;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.IOException;

public class userJoinActivity extends AppCompatActivity implements CaulyCloseAdListener {

    private final long FINISH_INTERVAL_TIME = 500;
    private long backPressedTime = 0;

    public static Activity userJoinActivity;


    EditText joinNameText;
    Spinner joinSexText;
    Spinner joinAgeText;
    ImageView joinMapButton;
    public static TextView joinLocationText;
    Spinner joinPreferageText;
    EditText joinJobText;
    Spinner joinSmokeText;
    Spinner joinReligionText;

    ImageView joinUserVoice;
    MediaRecorder recorder;
    boolean toggleRecord = false;
    TextView joinUserVoiceCount;
    CountDownTimer UserVoiceCountDownTimer;

    int countDown=29;


    ImageView joinUserVoiceConfirm;
    MediaPlayer mediaPlayer;
    boolean togglePlay = false;
    TextView joinUserVoiceConfirmCount;
    CountDownTimer UserVoiceConfirmCountDownTimer;

    TextView userJoinConfirmButton;


    String name;
    String sex;
    String age;
    String preferage;
    String job;
    String smoke;
    String religion;

    boolean hasVoice = false;

    //////////// cauly

    //private static final String APP_CODE = "CAULY"; // 광고 요청 코드 ( 자신의 APP_CODE 쓸 것 )
    private static final String APP_CODE = "cYoXgrqI"; // 광고 요청 코드 ( 자신의 APP_CODE 쓸 것 )

    CaulyCloseAd mCloseAd ;                         // CloseAd광고 객체


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_join);

        CaulyAdInfo closeAdInfo = new CaulyAdInfoBuilder(APP_CODE).build();
        mCloseAd = new CaulyCloseAd();
        mCloseAd.setButtonText("취소", "종료");
        mCloseAd.setDescriptionText("종료하시겠습니까?");
        mCloseAd.setAdInfo(closeAdInfo);
        mCloseAd.setCloseAdListener(this); // CaulyCloseAdListener 등록
        // 종료광고 노출 후 back버튼 사용을 막기 원할 경우 disableBackKey();을 추가한다
        // mCloseAd.disableBackKey();

        userJoinActivity = this;

        joinNameText = (EditText) findViewById(R.id.joinNameText);
        joinNameText.setText(currentUser.name);

        joinSexText = (Spinner) findViewById(R.id.joinSexText);
        ArrayAdapter sexAdapter = ArrayAdapter.createFromResource(this,
                R.array.sex, R.layout.spinner_item);
        sexAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        joinSexText.setAdapter(sexAdapter);
        joinSexText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sex = joinSexText.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        joinAgeText = (Spinner) findViewById(R.id.joinAgeText);
        ArrayAdapter ageAdapter = ArrayAdapter.createFromResource(this,
                R.array.age, R.layout.spinner_item);
        ageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        joinAgeText.setAdapter(ageAdapter);
        joinAgeText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                age = joinAgeText.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        joinMapButton = (ImageView) findViewById(R.id.joinMapButton);
        joinMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), MapsActivity.class);
                startActivity(intent);
            }
        });

        joinLocationText = (TextView) findViewById(R.id.joinLocationText);

        joinPreferageText = (Spinner) findViewById(R.id.joinPreferageText);
        ArrayAdapter preferageAdapter = ArrayAdapter.createFromResource(this,
                R.array.age, R.layout.spinner_item);
        preferageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        joinPreferageText.setAdapter(preferageAdapter);
        joinPreferageText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                preferage = joinPreferageText.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        joinJobText = (EditText) findViewById(R.id.joinJobText);


        joinSmokeText = (Spinner) findViewById(R.id.joinSmokeText);
        ArrayAdapter smokeAdapter = ArrayAdapter.createFromResource(this,
                R.array.smoke, R.layout.spinner_item);
        smokeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        joinSmokeText.setAdapter(smokeAdapter);
        joinSmokeText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                smoke = joinSmokeText.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        joinReligionText = (Spinner) findViewById(R.id.joinReligionText);
        ArrayAdapter religionAdapter = ArrayAdapter.createFromResource(this,
                R.array.religion, R.layout.spinner_item);
        religionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        joinReligionText.setAdapter(religionAdapter);
        joinReligionText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                religion = joinReligionText.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        joinUserVoiceCount = findViewById(R.id.joinUserVoiceCount);
        joinUserVoiceConfirmCount = findViewById(R.id.joinUserVoiceConfirmCount);

        joinUserVoice = findViewById(R.id.joinUserVoice);
        joinUserVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!toggleRecord) {
                    try {
                        toggleRecord = true;
                        hasVoice = false;
                        File file = Environment.getExternalStorageDirectory();
                        //갤럭시 S4기준으로 /storage/emulated/0/의 경로를 갖고 시작한다.
                        String path = file.getAbsolutePath() + "/.connectionist/" + currentUser.id + "/user.3gp";

                        recorder = new MediaRecorder();
                        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                        //첫번째로 어떤 것으로 녹음할것인가를 설정한다. 마이크로 녹음을 할것이기에 MIC로 설정한다.
                        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                        //이것은 파일타입을 설정한다. 녹음파일의경우 3gp로해야 용량도 작고 효율적인 녹음기를 개발할 수있다.
                        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
                        //이것은 코덱을 설정하는 것이라고 생각하면된다.
                        recorder.setOutputFile(path);
                        //저장될 파일을 저장한뒤
                        recorder.setMaxDuration(30000);
                        //timeOut 30초
                        recorder.prepare();
                        recorder.start();
                        //시작하면된다.

                        joinUserVoice.setEnabled(false);

                        //Toast.makeText(userJoinActivity, "start Record", Toast.LENGTH_LONG).show();
                        //30seconds -> count
                        UserVoiceCountDownTimer = new CountDownTimer(30000, 1000) {
                            @Override
                            public void onTick(long l) {
                                joinUserVoiceCount.setText("0:"+Integer.toString(countDown));
                                countDown--;
                                joinUserVoice.setEnabled(true);
                            }

                            @Override
                            public void onFinish() {
                                joinUserVoiceCount.setText("");
                                countDown=29;
                                toggleRecord=false;
                                hasVoice = true;
                                recorder.stop();
                            }
                        };

                        UserVoiceCountDownTimer.start();

                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                        Toast.makeText(userJoinActivity, e.toString(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(userJoinActivity, e.toString(), Toast.LENGTH_LONG).show();
                    }catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(userJoinActivity, e.toString(), Toast.LENGTH_LONG).show();
                    }
                } else{
                    if(countDown<25) {
                        toggleRecord = false;
                        hasVoice = true;
                        try {
                            recorder.stop();
                        } catch (RuntimeException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(userJoinActivity,"stop Record",Toast.LENGTH_LONG).show();
                        //30seconds -> invisible
                        UserVoiceCountDownTimer.cancel();
                        joinUserVoiceCount.setText("");
                        countDown = 29;
                    }
                }

            }
        });

        joinUserVoiceConfirm = findViewById(R.id.joinUserVoiceConfirm);
        joinUserVoiceConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(hasVoice==true){
                    if(!togglePlay){
                        File file = Environment.getExternalStorageDirectory();
                        //갤럭시 S4기준으로 /storage/emulated/0/의 경로를 갖고 시작한다.
                        String path = file.getAbsolutePath() + "/.connectionist/" + currentUser.id + "/user.3gp";
                        try {
                            mediaPlayer = new MediaPlayer();
                            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            mediaPlayer.setDataSource(getApplicationContext(), Uri.parse(path));
                            mediaPlayer.prepare();
                            mediaPlayer.start();
                            togglePlay = true;

                            joinUserVoiceConfirm.setEnabled(false);

                            //30seconds -> count
                            UserVoiceConfirmCountDownTimer = new CountDownTimer(30000, 1000) {
                                @Override
                                public void onTick(long l) {
                                    joinUserVoiceConfirmCount.setText("0:"+Integer.toString(countDown));
                                    countDown--;
                                    joinUserVoiceConfirm.setEnabled(true);
                                }

                                @Override
                                public void onFinish() {
                                    joinUserVoiceConfirmCount.setText("");
                                    countDown=29;
                                    togglePlay=false;
                                    mediaPlayer.stop();
                                }
                            };

                            UserVoiceConfirmCountDownTimer.start();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else if (togglePlay){
                        if(countDown<25) {
                            try {
                                mediaPlayer.stop();
                            } catch (RuntimeException e) {
                                e.printStackTrace();
                            }
                            togglePlay = false;
                            //30seconds -> invisible
                            UserVoiceConfirmCountDownTimer.cancel();
                            countDown = 29;
                            joinUserVoiceConfirmCount.setText("");
                        }
                    }else {
                        Toast.makeText(userJoinActivity,"먼저 녹음을 완료해주세요.",Toast.LENGTH_LONG).show();
                    }


                }else{
                    Toast.makeText(userJoinActivity,"먼저 녹음을 완료해주세요",Toast.LENGTH_LONG).show();
                }

            }
        });



        userJoinConfirmButton = (TextView) findViewById(R.id.userJoinConfirmButton);
        userJoinConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                name = joinNameText.getText().toString();
                job = joinJobText.getText().toString();

                boolean checkWordName = ohtwoUtil.checkWord(name);
                boolean checkWordJob = ohtwoUtil.checkWord(job);
                //name sex age dodo city preferage job pr

                if ( name.equals("")) {
                    Toast.makeText(getApplicationContext(), "이름을 입력해주세요.", Toast.LENGTH_SHORT).show();
                } else if(!checkWordName){
                    Toast.makeText(getApplicationContext(), "이름에 비속어가 포함되어 있습니다.", Toast.LENGTH_SHORT).show();
                } else if(sex.equals("")){
                    Toast.makeText(getApplicationContext(), "성별을 선택해주세요.", Toast.LENGTH_SHORT).show();
                } else if(age.equals("")){
                    Toast.makeText(getApplicationContext(), "나이를 선택해주세요.", Toast.LENGTH_SHORT).show();
                } else if(currentUser.location==null || currentUser.location.equals("")){
                    Toast.makeText(getApplicationContext(), "지역을 선택해주세요.", Toast.LENGTH_SHORT).show();
                } else if(preferage.equals("")){
                    Toast.makeText(getApplicationContext(), "선호나이를 선택해주세요.", Toast.LENGTH_SHORT).show();
                } else if(!checkWordJob){
                    Toast.makeText(getApplicationContext(), "직업에 비속어가 포함되어 있습니다.", Toast.LENGTH_SHORT).show();
                } else if(job.equals("")){
                    Toast.makeText(getApplicationContext(), "직업을 입력해주세요.", Toast.LENGTH_SHORT).show();
                } else if(smoke.equals("")){
                    Toast.makeText(getApplicationContext(), "흡연 여부를 선택해주세요.", Toast.LENGTH_SHORT).show();
                } else if(religion.equals("")){
                    Toast.makeText(getApplicationContext(), "종교를 선택해주세요.", Toast.LENGTH_SHORT).show();
                } else if(!hasVoice){
                    Toast.makeText(getApplicationContext(), "목소리를 녹음해주세요.", Toast.LENGTH_SHORT).show();
                } else {

                    if(currentUser.id!=null) {
                        apacheCall.voiceUpload(currentUser.id);
                        boolean setDBResult = callJsp.setDBUserInfo("insert", currentUser.id, name, sex, age, currentUser.latitude, currentUser.longitude, currentUser.location, preferage, job, smoke, religion, currentUser.phone);
                        callJsp.setCurrentUserInfo(currentUser.id);

                        UserModel userModel = new UserModel();
                        userModel.uid = currentUser.id;
                        userModel.isCaptain = false;
                        userModel.isMeeting = false;
                        userModel.isWaiting = false;
                        FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.id).setValue(userModel);

                        Intent intent = new Intent(getBaseContext(), HelpActivity.class);
                        GlobalApplication.needHelpSkip = true;
                        //Intent intent = new Intent(getBaseContext(), OpenWaitActivity.class);
                        startActivity(intent);
                        finish();
                    } else{
                        Toast.makeText(userJoinActivity,"네이버, 카카오, 페이스북 로그인에 이상이 생겼습니다. 앱을 재설치해주세요.",Toast.LENGTH_LONG).show();
                    }
                }
            }

        });



    }

    ////cauly

    // Back Key가 눌러졌을 때, CloseAd 호출
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
            // 앱을 처음 설치하여 실행할 때, 필요한 리소스를 다운받았는지 여부.
            if (mCloseAd.isModuleLoaded())
            {
                mCloseAd.show(this);
            }
            else
            {
                // 광고에 필요한 리소스를 한번만  다운받는데 실패했을 때 앱의 종료팝업 구현
                showDefaultClosePopup();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showDefaultClosePopup()
    {
        new AlertDialog.Builder(this).setTitle("").setMessage("종료 하시겠습니까?")
                .setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        finishAffinity();
                        System.runFinalization();
                        System.exit(0);
                    }
                })
                .setNegativeButton("아니요",null)
                .show();
    }

    // CaulyCloseAdListener
    @Override
    public void onFailedToReceiveCloseAd(CaulyCloseAd ad, int errCode,String errMsg) {


    }
    // CloseAd의 광고를 클릭하여 앱을 벗어났을 경우 호출되는 함수이다.
    @Override
    public void onLeaveCloseAd(CaulyCloseAd ad) {
    }
    // CloseAd의 request()를 호출했을 때, 광고의 여부를 알려주는 함수이다.
    @Override
    public void onReceiveCloseAd(CaulyCloseAd ad, boolean isChargable) {

    }
    //왼쪽 버튼을 클릭 하였을 때, 원하는 작업을 수행하면 된다.
    @Override
    public void onLeftClicked(CaulyCloseAd ad) {

    }
    //오른쪽 버튼을 클릭 하였을 때, 원하는 작업을 수행하면 된다. (종료창에서 예 눌렀을때)
    //Default로는 오른쪽 버튼이 종료로 설정되어있다.
    @Override
    public void onRightClicked(CaulyCloseAd ad) {
        finishAffinity();
        System.runFinalization();
        System.exit(0);
    }
    @Override
    public void onShowedCloseAd(CaulyCloseAd ad, boolean isChargable) {
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCloseAd != null)
            mCloseAd.resume(this); // 필수 호출
    }
    ////


    /*@Override
    public void onBackPressed() {
        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;

        if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime)
        {
            finishAffinity();
            System.runFinalization();
            System.exit(0);
        }
        else
        {
            backPressedTime = tempTime;
            Toast.makeText(getApplicationContext(), "\'뒤로\'버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
        }
    }*/

}

