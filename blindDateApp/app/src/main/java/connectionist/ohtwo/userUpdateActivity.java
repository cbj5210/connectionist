package connectionist.ohtwo;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class userUpdateActivity extends AppCompatActivity {

    private final long FINISH_INTERVAL_TIME = 500;
    private long backPressedTime = 0;

    public static Activity userUpdateActivity;


    EditText updateNameText;
    Spinner updateSexText;
    Spinner updateAgeText;
    ImageView updateMapButton;
    public static TextView updateLocationText;
    Spinner updatePreferageText;
    EditText updateJobText;
    Spinner updateSmokeText;
    Spinner updateReligionText;

    ImageView updateUserVoice;
    MediaRecorder recorder;
    boolean toggleRecord = false;

    ImageView updateUserVoiceConfirm;
    MediaPlayer mediaPlayer;
    boolean togglePlay = false;

    TextView userUpdateConfirmButton;

    int countDown=29;

    CountDownTimer UserVoiceCountDownTimer;
    CountDownTimer UserVoiceConfirmCountDownTimer;

    TextView updateUserVoiceCount;
    TextView updateUserVoiceConfirmCount;


    String name;
    String sex;
    String age;
    String preferage;
    String job;
    String smoke;
    String religion;

    boolean hasMap = false;
    boolean hasVoice = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_update);

        userUpdateActivity = this;

        updateNameText = (EditText) findViewById(R.id.updateNameText);
        updateNameText.setText(currentUser.name);

        updateSexText = (Spinner) findViewById(R.id.updateSexText);
        ArrayAdapter sexAdapter = ArrayAdapter.createFromResource(this,
                R.array.sex, R.layout.spinner_item);
        sexAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        updateSexText.setAdapter(sexAdapter);
        updateSexText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sex = updateSexText.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        if(currentUser.sex.equals("남자"))
            updateSexText.setSelection(1);
        else
            updateSexText.setSelection(2);

        updateAgeText = (Spinner) findViewById(R.id.updateAgeText);
        ArrayAdapter ageAdapter = ArrayAdapter.createFromResource(this,
                R.array.age, R.layout.spinner_item);
        ageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        updateAgeText.setAdapter(ageAdapter);
        updateAgeText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                age = updateAgeText.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        updateAgeText.setSelection(Integer.parseInt(currentUser.age) - 17 );

        updateMapButton = (ImageView) findViewById(R.id.updateMapButton);
        updateMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hasMap = true;
                Intent intent = new Intent(getBaseContext(), MapsActivity.class);
                startActivity(intent);
            }
        });

        updateLocationText = (TextView) findViewById(R.id.updateLocationText);
        updateLocationText.setText(currentUser.location);

        updatePreferageText = (Spinner) findViewById(R.id.updatePreferageText);
        ArrayAdapter preferageAdapter = ArrayAdapter.createFromResource(this,
                R.array.age, R.layout.spinner_item);
        preferageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        updatePreferageText.setAdapter(preferageAdapter);
        updatePreferageText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                preferage = updatePreferageText.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        updatePreferageText.setSelection(Integer.parseInt(currentUser.preferage) - 17 );

        updateJobText = (EditText) findViewById(R.id.updateJobText);
        updateJobText.setText(currentUser.job);


        updateSmokeText = (Spinner) findViewById(R.id.updateSmokeText);
        ArrayAdapter smokeAdapter = ArrayAdapter.createFromResource(this,
                R.array.smoke, R.layout.spinner_item);
        smokeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        updateSmokeText.setAdapter(smokeAdapter);
        updateSmokeText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                smoke = updateSmokeText.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        if(currentUser.smoke.equals("흡연"))
            updateSmokeText.setSelection(1);
        else
            updateSmokeText.setSelection(2);

        updateReligionText = (Spinner) findViewById(R.id.updateReligionText);
        ArrayAdapter religionAdapter = ArrayAdapter.createFromResource(this,
                R.array.religion, R.layout.spinner_item);
        religionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        updateReligionText.setAdapter(religionAdapter);
        updateReligionText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                religion = updateReligionText.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        if(currentUser.religion.equals("무교"))
            updateReligionText.setSelection(1);
        else if(currentUser.religion.equals("불교"))
            updateReligionText.setSelection(2);
        else if(currentUser.religion.equals("기독교"))
            updateReligionText.setSelection(3);
        else if(currentUser.religion.equals("천주교"))
            updateReligionText.setSelection(4);
        else if(currentUser.religion.equals("기타"))
            updateReligionText.setSelection(5);
        else
            updateReligionText.setSelection(0);

        updateUserVoiceCount = findViewById(R.id.updateUserVoiceCount);
        updateUserVoiceConfirmCount = findViewById(R.id.updateUserVoiceConfirmCount);

        updateUserVoice = findViewById(R.id.updateUserVoice);
        updateUserVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!toggleRecord) {
                    try {
                        toggleRecord = true;
                        hasVoice = false;
                        File file = Environment.getExternalStorageDirectory();
                        //갤럭시 S4기준으로 /storage/emulated/0/의 경로를 갖고 시작한다.
                        String path = file.getAbsolutePath() + "/.connectionist/" + currentUser.id + "/user.3gp";

                        recorder = new MediaRecorder();
                        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                        //첫번째로 어떤 것으로 녹음할것인가를 설정한다. 마이크로 녹음을 할것이기에 MIC로 설정한다.
                        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                        //이것은 파일타입을 설정한다. 녹음파일의경우 3gp로해야 용량도 작고 효율적인 녹음기를 개발할 수있다.
                        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
                        //이것은 코덱을 설정하는 것이라고 생각하면된다.
                        recorder.setOutputFile(path);
                        //저장될 파일을 저장한뒤
                        recorder.setMaxDuration(30000);
                        //timeOut 30초
                        recorder.prepare();
                        recorder.start();
                        //시작하면된다.

                        updateUserVoice.setEnabled(false);

                        //Toast.makeText(userJoinActivity, "start Record", Toast.LENGTH_LONG).show();
                        //30seconds -> count
                        UserVoiceCountDownTimer = new CountDownTimer(30000, 1000) {
                            @Override
                            public void onTick(long l) {
                                updateUserVoiceCount.setText("0:"+Integer.toString(countDown));
                                countDown--;
                                updateUserVoice.setEnabled(true);
                            }

                            @Override
                            public void onFinish() {
                                updateUserVoiceCount.setText("");
                                countDown=29;
                                toggleRecord=false;
                                hasVoice = true;
                                recorder.stop();
                            }
                        };

                        UserVoiceCountDownTimer.start();

                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                        Toast.makeText(userUpdateActivity, e.toString(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(userUpdateActivity, e.toString(), Toast.LENGTH_LONG).show();
                    }catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(userUpdateActivity, e.toString(), Toast.LENGTH_LONG).show();
                    }
                } else{
                    if(countDown<25) {
                        toggleRecord = false;
                        hasVoice = true;
                        try {
                            recorder.stop();
                        } catch (RuntimeException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(userJoinActivity,"stop Record",Toast.LENGTH_LONG).show();
                        //30seconds -> invisible
                        UserVoiceCountDownTimer.cancel();
                        updateUserVoiceCount.setText("");
                        countDown = 29;
                    }
                }

            }
        });

        updateUserVoiceConfirm = findViewById(R.id.updateUserVoiceConfirm);
        updateUserVoiceConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(hasVoice==true){
                    if(!togglePlay){
                        File file = Environment.getExternalStorageDirectory();
                        //갤럭시 S4기준으로 /storage/emulated/0/의 경로를 갖고 시작한다.
                        String path = file.getAbsolutePath() + "/.connectionist/" + currentUser.id + "/user.3gp";
                        try {
                            mediaPlayer = new MediaPlayer();
                            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            mediaPlayer.setDataSource(getApplicationContext(), Uri.parse(path));
                            mediaPlayer.prepare();
                            mediaPlayer.start();
                            togglePlay = true;

                            updateUserVoiceConfirm.setEnabled(false);

                            //30seconds -> count
                            UserVoiceConfirmCountDownTimer = new CountDownTimer(30000, 1000) {
                                @Override
                                public void onTick(long l) {
                                    updateUserVoiceConfirmCount.setText("0:"+Integer.toString(countDown));
                                    countDown--;
                                    updateUserVoiceConfirm.setEnabled(true);
                                }

                                @Override
                                public void onFinish() {
                                    updateUserVoiceConfirmCount.setText("");
                                    countDown=29;
                                    togglePlay=false;
                                    mediaPlayer.stop();
                                }
                            };

                            UserVoiceConfirmCountDownTimer.start();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else if (togglePlay){
                        if(countDown<25) {
                            try {
                                mediaPlayer.stop();
                            } catch (RuntimeException e) {
                                e.printStackTrace();
                            }
                            togglePlay = false;
                            //30seconds -> invisible
                            UserVoiceConfirmCountDownTimer.cancel();
                            countDown = 29;
                            updateUserVoiceConfirmCount.setText("");
                        }
                    }else {
                        Toast.makeText(userUpdateActivity,"먼저 녹음을 완료해주세요.",Toast.LENGTH_LONG).show();
                    }


                }else{
                    Toast.makeText(userUpdateActivity,"먼저 녹음을 완료해주세요",Toast.LENGTH_LONG).show();
                }
            }
        });



        userUpdateConfirmButton = (TextView) findViewById(R.id.userUpdateConfirmButton);
        userUpdateConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                name = updateNameText.getText().toString();
                job = updateJobText.getText().toString();

                boolean checkWordName = ohtwoUtil.checkWord(name);
                boolean checkWordJob = ohtwoUtil.checkWord(job);

                //name sex age dodo city preferage job pr

                if ( name.equals("")) {
                    Toast.makeText(getApplicationContext(), "이름을 입력해주세요.", Toast.LENGTH_SHORT).show();
                } else if(!checkWordName){
                    Toast.makeText(getApplicationContext(), "이름에 비속어가 포함되어 있습니다.", Toast.LENGTH_SHORT).show();
                } else if(sex.equals("")){
                    Toast.makeText(getApplicationContext(), "성별을 선택해주세요.", Toast.LENGTH_SHORT).show();
                } else if(age.equals("")){
                    Toast.makeText(getApplicationContext(), "나이를 선택해주세요.", Toast.LENGTH_SHORT).show();
                } else if(preferage.equals("")){
                    Toast.makeText(getApplicationContext(), "선호나이를 선택해주세요.", Toast.LENGTH_SHORT).show();
                } else if(job.equals("")){
                    Toast.makeText(getApplicationContext(), "직업을 입력해주세요.", Toast.LENGTH_SHORT).show();
                } else if(!checkWordJob){
                    Toast.makeText(getApplicationContext(), "직업에 비속어가 포함되어 있습니다.", Toast.LENGTH_SHORT).show();
                } else if(smoke.equals("")){
                    Toast.makeText(getApplicationContext(), "흡연 여부를 선택해주세요.", Toast.LENGTH_SHORT).show();
                } else if(religion.equals("")){
                    Toast.makeText(getApplicationContext(), "종교를 선택해주세요.", Toast.LENGTH_SHORT).show();
                } else {
                    apacheCall.voiceUpload(currentUser.id);

                    boolean setDBResult = callJsp.setDBUserInfo("update", currentUser.id, name, sex, age, currentUser.latitude, currentUser.longitude, currentUser.location, preferage, job, smoke, religion, currentUser.phone);
                    callJsp.setCurrentUserInfo(currentUser.id);

                    /*
                    UserModel userModel = new UserModel();
                    userModel.uid = currentUser.id;
                    userModel.userName = currentUser.name;
                    FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.id).setValue(userModel);
                    */

                    Intent intent = new Intent(getBaseContext(), main_page.class);
                    intent.putExtra("setting","ok");
                    intent.putExtra("isRefresh","false");
                    intent.putExtra("isRefreshMeeting","false");
                    startActivity(intent);
                    finish();
                }
            }

        });

    }



    @Override
    public void onBackPressed() {
        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;

        if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime)
        {
            finishAffinity();
            System.runFinalization();
            System.exit(0);
        }
        else
        {
            backPressedTime = tempTime;
            Toast.makeText(getApplicationContext(), "\'뒤로\'버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
        }
    }

}

