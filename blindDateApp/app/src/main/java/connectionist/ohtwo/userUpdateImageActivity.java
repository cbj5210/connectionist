package connectionist.ohtwo;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nhn.android.idp.common.logger.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class userUpdateImageActivity extends AppCompatActivity {

    private final long FINISH_INTERVAL_TIME = 500;
    private long backPressedTime = 0;

    ImageView userUpdateImage;
    ImageView userUpdateSubImage1;
    ImageView userUpdateSubImage2;

    public int selection=0;

    public boolean changeImage = false;
    public boolean changeSubImage1 = false;
    public boolean changeSubImage2 = false;

    private final int GALLERY_CODE=1112;

    Bitmap bitmap;
    Bitmap bitmap_sub_1;
    Bitmap bitmap_sub_2;

    TextView userUpdateImageNextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_update_image);

        File tempFile;
        Uri imageUri;



        userUpdateImage = findViewById(R.id.userUpdateImage);
        imageUri= Uri.parse("file:///" + Environment.getExternalStorageDirectory() + "/.connectionist/"+currentUser.id+"/user.jpg");
        tempFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/.connectionist/"+currentUser.id+"/user.jpg");
        if(tempFile.exists())
            userUpdateImage.setImageURI(imageUri);
        userUpdateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selection = 0;
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, GALLERY_CODE);
            }
        });

        userUpdateSubImage1 = findViewById(R.id.userUpdateSubImage1);
        imageUri= Uri.parse("file:///" + Environment.getExternalStorageDirectory() + "/.connectionist/"+currentUser.id+"/user_sub_1.jpg");
        tempFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/.connectionist/"+currentUser.id+"/user_sub_1.jpg");
        if(tempFile.exists())
            userUpdateSubImage1.setImageURI(imageUri);
        userUpdateSubImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selection = 1;
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, GALLERY_CODE);
            }
        });

        userUpdateSubImage2 = findViewById(R.id.userUpdateSubImage2);
        imageUri= Uri.parse("file:///" + Environment.getExternalStorageDirectory() + "/.connectionist/"+currentUser.id+"/user_sub_2.jpg");
        tempFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/.connectionist/"+currentUser.id+"/user_sub_2.jpg");
        if(tempFile.exists())
            userUpdateSubImage2.setImageURI(imageUri);
        userUpdateSubImage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selection = 2;
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, GALLERY_CODE);
            }
        });

        userUpdateImageNextButton = (TextView) findViewById(R.id.userUpdateImageNextButton);
        userUpdateImageNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if(changeImage) {
                        saveBitmapToJpg(bitmap, 0); //bit image save to exStorage/connectionist/user_id/user.jpg
                        apacheCall.imageUpload(currentUser.id, bitmap, 0);
                    }
                    if(changeSubImage1) {
                        saveBitmapToJpg(bitmap_sub_1, 1); //bit image save to exStorage/connectionist/user_id/user.jpg
                        apacheCall.imageUpload(currentUser.id, bitmap_sub_1, 1);
                    }
                    if(changeSubImage2) {
                        saveBitmapToJpg(bitmap_sub_2, 2); //bit image save to exStorage/connectionist/user_id/user.jpg
                        apacheCall.imageUpload(currentUser.id, bitmap_sub_2, 2);
                    }

                    Intent intent = new Intent(getBaseContext(), userUpdateActivity.class);
                    startActivity(intent);
                    finish();
            }

        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GALLERY_CODE:
                    sendPicture(data.getData()); //갤러리에서 가져오기
                    break;
                default:
                    break;
            }

        }
    }

    private void sendPicture(Uri imgUri) {

        String imagePath = getRealPathFromURI(imgUri); // path 경로
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        int exifDegree = exifOrientationToDegrees(exifOrientation);

        if(selection==0) {
            bitmap = BitmapFactory.decodeFile(imagePath); //경로를 통해 비트맵으로 전환
            bitmap = rotate(bitmap, exifDegree);
            userUpdateImage.setImageBitmap(bitmap); //이미지 뷰에 비트맵 넣기
            changeImage = true;
        }
        else if(selection==1){
            bitmap_sub_1 = BitmapFactory.decodeFile(imagePath); //경로를 통해 비트맵으로 전환
            bitmap_sub_1 = rotate(bitmap_sub_1, exifDegree);
            userUpdateSubImage1.setImageBitmap(bitmap_sub_1); //이미지 뷰에 비트맵 넣기
            changeSubImage1 = true;
        }
        else if(selection==2){
            bitmap_sub_2 = BitmapFactory.decodeFile(imagePath); //경로를 통해 비트맵으로 전환
            bitmap_sub_2 = rotate(bitmap_sub_2, exifDegree);
            userUpdateSubImage2.setImageBitmap(bitmap_sub_2); //이미지 뷰에 비트맵 넣기
            changeSubImage2 = true;
        }

    }

    private int exifOrientationToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private Bitmap rotate(Bitmap src, float degree) {

        // Matrix 객체 생성
        Matrix matrix = new Matrix();
        // 회전 각도 셋팅
        matrix.postRotate(degree);
        // 이미지와 Matrix 를 셋팅해서 Bitmap 객체 생성
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(),
                src.getHeight(), matrix, true);
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        cursor.moveToNext();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DATA));
        Uri uri = Uri.fromFile(new File(path));
        Logger.d("realPath", "getRealPathFromURI(), path : " + uri.toString()); cursor.close();
        return path;
    }

    public void saveBitmapToJpg(Bitmap bitmap, int selection){
        String ex_storage = Environment.getExternalStorageDirectory().getAbsolutePath(); // Get Absolute Path in External Sdcard
        String foler_name = "/.connectionist/"+currentUser.id+"/";
        String file_name="";
        if(selection==0)
            file_name = "user.jpg";
        else if(selection==1)
            file_name = "user_sub_1.jpg";
        else if(selection==2)
            file_name = "user_sub_2.jpg";
        String string_path = ex_storage+foler_name;
        File file_path;
        try{
            file_path = new File(string_path);
            if(!file_path.isDirectory()){
                file_path.mkdirs();
            }
            FileOutputStream out = new FileOutputStream(string_path+file_name);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.close();
        }catch(FileNotFoundException exception){
            Log.e("FileNotFoundException", exception.getMessage());
        }catch(IOException exception){
            Log.e("IOException", exception.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;

        if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime)
        {
            finishAffinity();
            System.runFinalization();
            System.exit(0);
        }
        else
        {
            backPressedTime = tempTime;
            Toast.makeText(getApplicationContext(), "\'뒤로\'버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
        }
    }

}
