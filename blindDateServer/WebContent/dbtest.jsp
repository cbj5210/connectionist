<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>mysql에서 db받고 json리턴</title>
</head>
<body>

<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@page import="org.json.simple.JSONObject"%>

<% 
String sql = null; 
Connection conn = null;
PreparedStatement pstmt = null;
ResultSet rs = null;
try {	

	String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
	String remoteip = request.getRemoteAddr();
	TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
	TraceTxId.setTraceTime();
	TraceTxId.setIp(remoteip);
	Logger.debug("Service Start.");
	
	sql = "Select name, age from user_info";
	TxMgr.getInstance().begin();
	conn = TxMgr.getInstance().getConnection(); 
		
		
		pstmt = conn.prepareStatement(sql);
		
		long laptime = System.currentTimeMillis();
		
		rs = pstmt.executeQuery();
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql);
		
		while(rs.next()) {
			out.print("테스트");
			out.print(rs.getString("name"));
			out.print(rs.getInt("age"));
		}
		
		TxMgr.getInstance().commit();
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");
		
	} catch (Exception e) {
		Logger.error("Service Error.");
 		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (pstmt != null) pstmt.close();
		if (conn != null) TxMgr.getInstance().close();
	}
%>
<%
    JSONObject obj=new JSONObject();
    obj.put("name","KYH");
    obj.put("age",new Integer(25));
    obj.put("weight",new Double(70.3));
    obj.put("is_vip",new Boolean(true));
    obj.put("nickname",null);
    out.print(obj);
    out.flush();
%>


</body>
</html>