<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>
<%
	String sql = null; 
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	try {	
		
    	String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");   

		String phone = request.getParameter("phone");

		Logger.debug("---------------<< Input value >>---------------------");
		Logger.debug("phone = " + phone);
		
		TxMgr.getInstance().begin();
		conn = TxMgr.getInstance().getConnection(); 
		
		stmt = conn.createStatement();
		
		sql = "SELECT id FROM User Where phone='"+phone+"'";
		long laptime = System.currentTimeMillis();

		rs = stmt.executeQuery(sql);

		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING
		
		JSONArray arr = new JSONArray();
//signed, name, sex, latitude, longitude, gu, age, job, pr, cash 
		
		while(rs.next())
		{
			String id = rs.getString("id");
			JSONObject obj = new JSONObject();
			obj.put("id", id);
			
			if(obj != null)
				arr.add(obj);
		}
		out.print(arr);
		TxMgr.getInstance().commit();
		
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");
	} catch (Exception e) {
		Logger.error("Service Error.");
 		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (stmt != null) stmt.close();
		if (conn != null) TxMgr.getInstance().close();
	}
%>
