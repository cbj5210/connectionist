<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>
<%
	String sql = null;
	Connection conn = null;
	PreparedStatement pstmt = null;
	Statement stmt = null;
	ResultSet rs = null;
		
	String id = request.getParameter("id");
	String newF = request.getParameter("newF"); //수락할 친구 번호, 추가할 친구 번호
		
	//String id = "naver78046897";	
	//String newF = "01036575957";
	
	String newF1 = "82"+newF.substring(1,11); //821044445555
	String newF2 = "+82"+newF.substring(1,11); //+821044445555
	String newF3 = " 82"+newF.substring(1,11); // 821044445555;
		  
	String a = "";
	String b = "";
	String c = "";
	String d = "";
		
	try{ 
    	String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");   
		
		Logger.debug("---------------<< Input value >>---------------------");
		Logger.debug("id = " + id);
		Logger.debug("newF = a = " + newF);
		Logger.debug("newF1 = b = " + newF1);
		Logger.debug("newF2 = c = " + newF2);
		Logger.debug("newF3 = d = " + newF3);
		
		
		TxMgr.getInstance().begin();
		conn = TxMgr.getInstance().getConnection(); 
		
		stmt = conn.createStatement();
		long laptime = System.currentTimeMillis();

		sql = "SELECT id FROM User where phone = '" + newF + "';";
		rs = stmt.executeQuery(sql);
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING
		
		while(rs.next()){
			a = rs.getString("id");
		}
		
		if(stmt!= null)stmt.close();
		if(rs!= null)rs.close();
		
		sql = "SELECT id FROM User where phone = '" + newF1 + "';";
		laptime = System.currentTimeMillis();
		stmt = conn.createStatement();
		rs = stmt.executeQuery(sql);
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING
		
		while(rs.next()){
			b = rs.getString("id");
		}	
		
		if(stmt!= null)stmt.close();
		if(rs!= null)rs.close();		
		
		sql = "SELECT id FROM User where phone = '" + newF2 + "';";
		laptime = System.currentTimeMillis();
		stmt = conn.createStatement();
		rs = stmt.executeQuery(sql);
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING
		
		while(rs.next()){
			c = rs.getString("id");
		}	
		
		if(stmt!= null)stmt.close();
		if(rs!= null)rs.close();
		
		sql = "SELECT id FROM User where phone = '" + newF3 + "';";
		laptime = System.currentTimeMillis();
		
		stmt = conn.createStatement();
		rs = stmt.executeQuery(sql);
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING
		
		while(rs.next()){
			d = rs.getString("id");
		}	
		
		if(stmt!= null)stmt.close();
		if(rs!= null)rs.close();
		
		if (a.equals("")){
			if(!b.equals("")){
				a = b;
			}else if(!c.equals("")){
				a= c;
			}else if(!d.equals("")){
				a= d;
			}
		}
		
		
		if (!a.equals("")){
			sql  = "insert into friend(ID1, ID2, success) values(?,?,?)";
			pstmt = conn.prepareStatement(sql);
        	pstmt.setString(1, id);
        	pstmt.setString(2, a);
        	pstmt.setString(3, "0");
    		laptime = System.currentTimeMillis(); 
	        int result = pstmt.executeUpdate();
	        Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
	        Logger.sql(sql); // SQL LOGGING
	        
	        if(result != 0){
	        	out.print("success");
				System.out.println("insert 성공");
				TxMgr.getInstance().commit();
			}
			else{
    	    	out.print("fail");
    	    	System.out.println("insert 실패");
    	    	TxMgr.getInstance().rollback();
			}
		}else{
			out.print("false");
			System.out.println("4종류의 번호 검색 다 실패");
			TxMgr.getInstance().commit();  // Nothing insert somthing, so commit it. 
		}
		
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");

	} catch (Exception e) {
		Logger.error("Service Error.");
		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (stmt != null) stmt.close();
		if (pstmt != null) pstmt.close();
		if (conn != null) TxMgr.getInstance().close();
	}
		
%>