<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>
<%
	
	String sql = null;
	Connection conn = null;
	PreparedStatement pstmt = null;
	Statement stmt = null;
	ResultSet rs = null;
		
	String id = request.getParameter("id");
	//String id = "naver78046897"; 
		
	try{
    	String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");   
		
		Logger.debug("---------------<< Input value >>---------------------");
		Logger.debug("id = " + id);
		
		TxMgr.getInstance().begin();
		conn = TxMgr.getInstance().getConnection(); 
		 
		stmt = conn.createStatement();
		
		JSONArray arr = new JSONArray();
		
		
		// 내가 ID1
		sql = "SELECT name,id FROM friend JOIN User ON friend.ID2 = User.id WHERE success = 1 AND ID1 ='" + id + "';";
		long laptime = System.currentTimeMillis();
		rs = stmt.executeQuery(sql);
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING

	 	while(rs.next())
		{
			String name = rs.getString("name");
			String idOut = rs.getString("id");
			JSONObject obj = new JSONObject();
			obj.put("id",idOut);
			obj.put("name",name);
			if(obj != null)
			arr.add(obj);
		}
	 	
	 	if(stmt!= null)stmt.close();
		if(rs!= null)rs.close();
		
		// 내가 ID2
		sql = "SELECT name,id FROM friend JOIN User ON friend.ID1 = User.id WHERE success = 1 AND ID2 ='" + id + "';";
		laptime = System.currentTimeMillis();
		stmt = conn.createStatement();
	 	rs = stmt.executeQuery(sql);
	 	Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING

	 	while(rs.next())
	 	{
	 		String name = rs.getString("name");
	 		String idOut = rs.getString("id");
	 		JSONObject obj = new JSONObject();
	 		obj.put("id",idOut);
	 		obj.put("name",name);
	 		if(obj != null)
	 		arr.add(obj);
	 	}
	 	
		out.print(arr);
		TxMgr.getInstance().commit();
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");
	} catch (Exception e) {
		Logger.error("Service Error.");
 		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (stmt != null) stmt.close();
		if (pstmt != null) pstmt.close();
		if (conn != null) TxMgr.getInstance().close();
	}
	
%>