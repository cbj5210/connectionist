<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>
<%
 
	String sql = null;
	Connection conn = null;
	PreparedStatement pstmt = null;
	Statement stmt = null;
	ResultSet rs = null;
		
	String id = request.getParameter("id");
	String newF = request.getParameter("newF"); //수락할 친구 번호, 추가할 친구 번호
			
	//String id = "naver78046897";
	//String newF = "201";
			 
	String newId = "";
	
	try{
    	String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");  
		
		Logger.debug("---------------<< Input value >>---------------------");
		Logger.debug("id = " + id);
		Logger.debug("newF = " + newF);
		
		TxMgr.getInstance().begin();
		
		conn = TxMgr.getInstance().getConnection();  
		
		stmt = conn.createStatement();
		
		//phone으로 ID 검색
		sql = "SELECT id FROM User where phone = '" + newF + "';";
		
		long laptime = System.currentTimeMillis();
		rs = stmt.executeQuery(sql);
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING

		while(rs.next()){
			newId = rs.getString("id");
		}	
				
		//success 1로 바꿔서 요청 수락을 표시
		
		sql  = "delete from friend where success = 0 AND ID1='" + newId +"' AND ID2='"+ id +"'";
		laptime = System.currentTimeMillis();
		pstmt = conn.prepareStatement(sql);
		
		int result = pstmt.executeUpdate();
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING

		if(result != 0){
        	out.print("true");
        	Logger.debug("delete success.");
			TxMgr.getInstance().commit();
		}
		else{
        	out.print("false");
			System.out.println("delete fail");
        	Logger.debug("delete fail.");

			TxMgr.getInstance().rollback();
		}
		
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");
	} catch (Exception e) {
		Logger.error("Service Error.");
 		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (stmt != null) stmt.close();
		if (pstmt != null) pstmt.close(); 
		if (conn != null) TxMgr.getInstance().close();
	}
	
%>