<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>
<% 
	
	String sql = null;
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	ResultSet rs2 = null;
	
	try {	
		String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");   

		String myId = request.getParameter("id");
		String mySex = request.getParameter("sex");
		String myLat = request.getParameter("lat");
		String myLon = request.getParameter("lon");
		String myMeetNum = request.getParameter("mee");
		
		
		Logger.debug("---------------<< Input value >>---------------------");
		Logger.debug("myId = " + myId);
		Logger.debug("mySex = " + mySex);
		Logger.debug("myLat = " + myLat);
		Logger.debug("myLon = " + myLon);
		Logger.debug("myMeetNum = " + myMeetNum);
		
		TxMgr.getInstance().begin();
		conn = TxMgr.getInstance().getConnection();  
		
		
		
		long laptime = 0;
		//String myId = "naver78046897";
		//String mySex = "남자";
		//String myLat = "37";
		//String myLon = "127";
		//String myMeetNum = "2";
		
		String id_1 ="";
		String id_2 ="";
		String id_3 ="";
		String id_4 ="";
		
		String count = "";
				
		if(myMeetNum.equals("2")){
			sql = "SELECT *, COUNT(*) AS cnt, (6371*acos(cos(radians("+myLat+"))*cos(radians(latitude))*cos(radians(longitude)-radians("+myLon+"))+sin(radians("+myLat+"))*sin(radians(latitude)))) AS km FROM wait2 HAVING sex != '"+mySex+"' AND km <= 5";
			laptime = System.currentTimeMillis();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			
			while(rs.next())
			{	
				count = rs.getString("cnt");
			}
			
			if(stmt!= null)stmt.close();
			if(rs!= null)rs.close();
			
			if(count.equals("")){
				count = "0";
			}else{
				sql = "SELECT *, (6371*acos(cos(radians("+myLat+"))*cos(radians(latitude))*cos(radians(longitude)-radians("+myLon+"))+sin(radians("+myLat+"))*sin(radians(latitude)))) AS km FROM wait2 HAVING sex != '"+mySex+"' AND km <= 5 ORDER BY rand() limit 1";
				laptime = System.currentTimeMillis();
				stmt = conn.createStatement();
				rs = stmt.executeQuery(sql);
				Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
				Logger.sql(sql); // SQL LOGGING
				while(rs.next())
				{	
					id_1 = rs.getString("id_1");
					id_2 = rs.getString("id_2");
				}
				
				if(stmt!= null)stmt.close();
				if(rs!= null)rs.close();
			}
			
		}
		
		if(myMeetNum.equals("3")){
			sql = "SELECT *, COUNT(*) AS cnt, (6371*acos(cos(radians("+myLat+"))*cos(radians(latitude))*cos(radians(longitude)-radians("+myLon+"))+sin(radians("+myLat+"))*sin(radians(latitude)))) AS km FROM wait3 HAVING sex != '"+mySex+"' AND km <= 5";
			laptime = System.currentTimeMillis();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			while(rs.next())
			{	
				count = rs.getString("cnt");
			}
			
			if(stmt!= null)stmt.close();
			if(rs!= null)rs.close();
			
			if(count.equals("")){
				count = "0";
			}else{
				sql = "SELECT *, (6371*acos(cos(radians("+myLat+"))*cos(radians(latitude))*cos(radians(longitude)-radians("+myLon+"))+sin(radians("+myLat+"))*sin(radians(latitude)))) AS km FROM wait3 HAVING sex != '"+mySex+"' AND km <= 5 ORDER BY rand() limit 1";
				laptime = System.currentTimeMillis();
				stmt = conn.createStatement();
				rs = stmt.executeQuery(sql);
				Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
				Logger.sql(sql); // SQL LOGGING
				while(rs.next())
				{	
					id_1 = rs.getString("id_1");
					id_2 = rs.getString("id_2");
					id_3 = rs.getString("id_3");
				}
				
				if(stmt!= null)stmt.close();
				if(rs!= null)rs.close();
			}
		}
		
		if(myMeetNum.equals("4")){
			sql = "SELECT *, COUNT(*) AS cnt, (6371*acos(cos(radians("+myLat+"))*cos(radians(latitude))*cos(radians(longitude)-radians("+myLon+"))+sin(radians("+myLat+"))*sin(radians(latitude)))) AS km FROM wait4 HAVING sex != '"+mySex+"' AND km <= 5";
			laptime = System.currentTimeMillis();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			while(rs.next())
			{	
				count = rs.getString("cnt");
			}
			
			if(stmt!= null)stmt.close();
			if(rs!= null)rs.close();
			
			if(count.equals("")){
				count = "0";
			}else{
				sql = "SELECT *, (6371*acos(cos(radians("+myLat+"))*cos(radians(latitude))*cos(radians(longitude)-radians("+myLon+"))+sin(radians("+myLat+"))*sin(radians(latitude)))) AS km FROM wait4 HAVING sex != '"+mySex+"' AND km <= 5 ORDER BY rand() limit 1";
				laptime = System.currentTimeMillis();
				stmt = conn.createStatement();
				rs = stmt.executeQuery(sql);
				Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
				Logger.sql(sql); // SQL LOGGING
				while(rs.next())
				{	
					id_1 = rs.getString("id_1");
					id_2 = rs.getString("id_2");
					id_3 = rs.getString("id_3");
					id_4 = rs.getString("id_4");
				}
				
				if(stmt!= null)stmt.close();
				if(rs!= null)rs.close();
			}
		}
		int waitCount = Integer.parseInt(count);
		JSONArray arr = new JSONArray();
		
		if(waitCount > 0) {
			Logger.debug("[" + count+" team is waiting ."+id_1+","+id_2+","+id_3+","+id_4+"]"); 
			sql = "SELECT * FROM User WHERE id ='"+id_1+"'";
			laptime = System.currentTimeMillis();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			while(rs.next()){
				String id = rs.getString("id");
				String signed = rs.getString("signed");
				String name = rs.getString("name");
				String sex = rs.getString("sex");
				String location = rs.getString("location");
				String age = rs.getString("age");
				String job = rs.getString("job");
				String smoke = rs.getString("smoke");
				String religion = rs.getString("religion");
				String cash = rs.getString("cash");
				String preferage = rs.getString("preferage");
				
				JSONObject obj = new JSONObject();
				obj.put("id", id);
				obj.put("signed", signed);
				obj.put("name", name);
				obj.put("sex", sex);
				obj.put("location", location); 
				obj.put("age", age);
				obj.put("job", job);
				obj.put("smoke", smoke);
				obj.put("religion", religion);
				obj.put("cash", cash);
				obj.put("preferage", preferage);
				
				if(obj != null)
					arr.add(obj);	
			}
			
			if(stmt!= null)stmt.close();
			if(rs!= null)rs.close();
			
			sql = "SELECT * FROM User WHERE id ='"+id_2+"'";
			laptime = System.currentTimeMillis();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			while(rs.next()){
				String id = rs.getString("id");
				String signed = rs.getString("signed");
				String name = rs.getString("name");
				String sex = rs.getString("sex");
				String location = rs.getString("location");
				String age = rs.getString("age");
				String job = rs.getString("job");
				String smoke = rs.getString("smoke");
				String religion = rs.getString("religion");
				String cash = rs.getString("cash");
				String preferage = rs.getString("preferage");
				
				JSONObject obj = new JSONObject();
				obj.put("id", id);
				obj.put("signed", signed);
				obj.put("name", name);
				obj.put("sex", sex);
				obj.put("location", location);
				obj.put("age", age);
				obj.put("job", job);
				obj.put("smoke", smoke);
				obj.put("religion", religion);
				obj.put("cash", cash);
				obj.put("preferage", preferage);
				
				if(obj != null)
					arr.add(obj);	
			}
			
			if(stmt!= null)stmt.close();
			if(rs!= null)rs.close();
			
			sql = "SELECT * FROM User WHERE id ='"+id_3+"'";
			laptime = System.currentTimeMillis();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			while(rs.next()){
				String id = rs.getString("id");
				String signed = rs.getString("signed");
				String name = rs.getString("name");
				String sex = rs.getString("sex");
				String location = rs.getString("location");
				String age = rs.getString("age");
				String job = rs.getString("job");
				String smoke = rs.getString("smoke");
				String religion = rs.getString("religion");
				String cash = rs.getString("cash");
				String preferage = rs.getString("preferage");
				
				JSONObject obj = new JSONObject();
				obj.put("id", id);
				obj.put("signed", signed);
				obj.put("name", name);
				obj.put("sex", sex);
				obj.put("location", location);
				obj.put("age", age);
				obj.put("job", job);
				obj.put("smoke", smoke);
				obj.put("religion", religion);
				obj.put("cash", cash);
				obj.put("preferage", preferage);
				
				if(obj != null)
					arr.add(obj);	
			}
			
			if(stmt!= null)stmt.close();
			if(rs!= null)rs.close();
			
			sql = "SELECT * FROM User WHERE id ='"+id_4+"'";
			laptime = System.currentTimeMillis();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			while(rs.next()){
				String id = rs.getString("id");
				String signed = rs.getString("signed");
				String name = rs.getString("name");
				String sex = rs.getString("sex");
				String location = rs.getString("location");
				String age = rs.getString("age");
				String job = rs.getString("job");
				String smoke = rs.getString("smoke");
				String religion = rs.getString("religion");
				String cash = rs.getString("cash");
				String preferage = rs.getString("preferage");
				
				JSONObject obj = new JSONObject();
				obj.put("id", id);
				obj.put("signed", signed);
				obj.put("name", name);
				obj.put("sex", sex);
				obj.put("location", location);
				obj.put("age", age);
				obj.put("job", job);
				obj.put("smoke", smoke);
				obj.put("religion", religion);
				obj.put("cash", cash);
				obj.put("preferage", preferage);
				
				if(obj != null)
					arr.add(obj);
			}
			
			if(stmt!= null)stmt.close();
			if(rs!= null)rs.close();
			
			out.print(arr+count);
		} else {
			Logger.debug("waiting team [" + count +"]"); 
			out.print(arr+count);
		}
		 
		
		TxMgr.getInstance().commit();
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");
	} catch (Exception e) { 
		Logger.error("Service Error.");
		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (rs2 != null) rs2.close();
		if (stmt != null) stmt.close(); 
		if (conn != null) TxMgr.getInstance().close();
	}
%>