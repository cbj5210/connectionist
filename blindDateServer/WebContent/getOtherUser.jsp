<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>
<%
	 
	String sql = null;
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	
	try {	
    	String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");  

		String myId = request.getParameter("id");
		String mySex = request.getParameter("sex");
		String myLat = request.getParameter("lat");
		String myLon = request.getParameter("lon");
		
		Logger.debug("---------------<< Input value >>---------------------");
		
		Logger.debug("myId = " + myId);
		Logger.debug("mySex = " + mySex);
		Logger.debug("myLat = " + myLat);
		Logger.debug("myLon = " + myLon);
		
		TxMgr.getInstance().begin();
		
		conn = TxMgr.getInstance().getConnection();  
	
		stmt = conn.createStatement();
		
		
		//String myId = "naver78046897";
		//String mySex = "남자";
		//String myLat = "37.586184";
		//String myLon = "127.016807";
	
		sql = "SELECT *, (6371*acos(cos(radians("+myLat+"))*cos(radians(latitude))*cos(radians(longitude)-radians("+myLon+"))+sin(radians("+myLat+"))*sin(radians(latitude)))) AS km FROM User HAVING sex != '"+mySex+"' AND km <= 200 ORDER BY rand() limit 5";
		long laptime = System.currentTimeMillis();

		rs = stmt.executeQuery(sql);
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING
		
		JSONArray arr = new JSONArray();

//signed, name, sex, dodo, city, gu, age, job, pr, cash 
		
		while(rs.next())
		{
			String id = rs.getString("id");
			String signed = rs.getString("signed");
			String name = rs.getString("name");
			String sex = rs.getString("sex");
			String location = rs.getString("location");
			String age = rs.getString("age");
			String job = rs.getString("job");
			String smoke = rs.getString("smoke");
			String religion = rs.getString("religion");
			String cash = rs.getString("cash");
			String preferage = rs.getString("preferage");
			String km = rs.getString("km");
			
			JSONObject obj = new JSONObject();
			obj.put("id", id);
			obj.put("signed", signed);
			obj.put("name", name);
			obj.put("sex", sex);
			obj.put("location", location);
			obj.put("age", age);
			obj.put("job", job);
			obj.put("smoke", smoke);
			obj.put("religion", religion);
			obj.put("cash", cash);
			obj.put("preferage", preferage);
			obj.put("km", km);
			
			if(obj != null)
				arr.add(obj);
		}
		out.print(arr);
		TxMgr.getInstance().commit();
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");

	} catch (Exception e) {
		System.out.println("접속 실패");
		e.printStackTrace();
	}
%>