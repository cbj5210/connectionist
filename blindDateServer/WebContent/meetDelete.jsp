<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>
<%
	
	String sql = null;
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	ResultSet rs2 = null;
	PreparedStatement pstmt = null;
	
	try {	
		String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");     
		
		TxMgr.getInstance().begin();
		conn = TxMgr.getInstance().getConnection();  
		stmt = conn.createStatement();
		
		String mid_1 = "";
		String mid_2 = "";
		String mid_3 = "";
		String mid_4 = "";
				
		String wid_1 = "";
		String wid_2 = "";
		String wid_3 = "";
		String wid_4 = "";
		
		String myId = request.getParameter("id");		
		String what = request.getParameter("what");
		String mySex = request.getParameter("sex");
		
		Logger.debug("---------------<< Input value >>---------------------");
		Logger.debug("myId = " + myId);
		Logger.debug("what = " + what); 
		Logger.debug("mySex = " + mySex); 
		
		//myId = "naver78046897";
		//what = "meet";
		//mySex = "남자";
		long laptime = 0;				
		if(what.equals("meet")){ //미팅테이블에서 row 지움
			if(mySex.equals("남자"))
				sql = "delete from meet where m1_id='"+myId+"' OR m2_id='"+myId+"'OR m3_id='"+myId+"'OR m4_id='"+myId+"'";
			//	sql = "delete from meet where m1_id='"+myId+"'";
			else if(mySex.equals("여자"))
				sql = "delete from meet where w1_id='"+myId+"' OR w2_id='"+myId+"'OR w3_id='"+myId+"'OR w4_id='"+myId+"'";
				//sql = "delete from meet where w1_id='"+myId+"'";
		
			laptime = System.currentTimeMillis();
			pstmt = conn.prepareStatement(sql);
			int result = pstmt.executeUpdate();
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			
			if(result != 0)
			{
				out.print("true");
	        	Logger.debug("delete meet ["+myId +"] success."  );
	        }
			else{
		    	out.print("false");
				Logger.debug("delete meet ["+myId +"] fail."  );

			}		
			
			if(pstmt!= null)pstmt.close(); 
		}
		
		if(what.equals("wait2")){ //wait2테이블에서 row 지움
			sql = "delete from wait2 where id_1='"+myId+"' OR id_2='"+myId+"'";
//			sql = "delete from wait2 where id_1='"+myId+"'";
			pstmt = conn.prepareStatement(sql);
			laptime = System.currentTimeMillis();
			int result = pstmt.executeUpdate();
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			
			if(result != 0)
			{
	        	out.print("true");
				Logger.debug("delete wait2 ["+myId +"] success."  );
	        }
			else{
		    	out.print("false");
		    	Logger.debug("delete wait2 ["+myId +"] fail."  );
			}		
			
			if(pstmt!= null)pstmt.close(); 
		}
	
		if(what.equals("wait3")){ //wait3테이블에서 row 지움
			sql = "delete from wait3 where id_1='"+myId+"' OR id_2='"+myId+"'OR id_3='"+myId+"'";
//			sql = "delete from wait3 where id_1='"+myId+"'";
			pstmt = conn.prepareStatement(sql);
			laptime = System.currentTimeMillis();
			int result = pstmt.executeUpdate();
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			
			if(result != 0)
			{
	        	out.print("true");
		    	Logger.debug("delete wait3 ["+myId +"] success."  );
	        }
			else{
		    	out.print("false");
		    	Logger.debug("delete wait3 ["+myId +"] fail."  );
			}		
			
			if(pstmt!= null)pstmt.close(); 
		}

		if(what.equals("wait4")){ //wait2테이블에서 row 지움
			sql = "delete from wait4 where id_1='"+myId+"' OR id_2='"+myId+"'OR id_3='"+myId+"'OR id_4='"+myId+"'";
			//sql = "delete from wait4 where id_1='"+myId+"'";
			laptime = System.currentTimeMillis();
			pstmt = conn.prepareStatement(sql);
			int result = pstmt.executeUpdate();
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			
			if(result != 0)
			{
    			out.print("true");
		    	Logger.debug("delete wait4 ["+myId +"] success."  );
    		}
			else{
    			out.print("false");
		    	Logger.debug("delete wait4 ["+myId +"] fail."  );
			}	
			
			if(pstmt!= null)pstmt.close(); 
		}		
		
		TxMgr.getInstance().commit();
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");
		
	} catch (Exception e) {
		Logger.error("Service Error.");
		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (rs2 != null) rs2.close();
		if (stmt != null) stmt.close(); 
		if (pstmt != null) pstmt.close(); 
		if (conn != null) TxMgr.getInstance().close();
	}
%>