<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>
<%
	 
	String sql = null;
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	ResultSet rs2 = null;
	PreparedStatement pstmt = null;
	
	try {	
		String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");     
		
		TxMgr.getInstance().begin();
		conn = TxMgr.getInstance().getConnection();  
		
		
		String mid_1 = "";
		String mid_2 = "";
		String mid_3 = "";
		String mid_4 = "";
				
		String wid_1 = "";
		String wid_2 = "";
		String wid_3 = "";
		String wid_4 = "";
		
		String myId = request.getParameter("id");		
		String mySex = request.getParameter("sex");
		String myMeetNum = request.getParameter("mee");		
		String wid = request.getParameter("wid");
		
		
		mid_1 = myId;
		mid_2 = request.getParameter("mid2");
		mid_3 = request.getParameter("mid3");
		mid_4 = request.getParameter("mid4");
		
		//mid_1 = "naver78046897";
		//mid_2 = "102";
		//wid = "101";
		//myMeetNum = "2";
		
		Logger.debug("---------------<< Input value >>---------------------");
		Logger.debug("myId = " + myId);
		Logger.debug("mySex = " + mySex); 
		Logger.debug("myMeetNum = " + myMeetNum);
		Logger.debug("mid_1 = " + mid_1);
		Logger.debug("mid_2 = " + mid_2); 
		Logger.debug("mid_3 = " + mid_3);
		Logger.debug("mid_4 = " + mid_4);
		
		long laptime = 0;	
		
		if(myMeetNum.equals("2")){
			sql = "SELECT * FROM wait2 where id_1='"+wid+"'"; 		
			laptime = System.currentTimeMillis();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			
			while(rs.next())
			{	
				wid_1 = rs.getString("id_1");
				wid_2 = rs.getString("id_2");
			}
			
			if(stmt!= null)stmt.close();
			if(rs!= null)rs.close();
									
			sql = "delete from wait2 where id_1='"+wid+"'";
			pstmt = conn.prepareStatement(sql);
			laptime = System.currentTimeMillis();
			int result = pstmt.executeUpdate();
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			
			if(pstmt!= null)pstmt.close(); 
			
			if(result != 0)
			{
				Logger.debug("delete wait2 ["+wid+"] success."  ); 
				sql = "insert into meet(m1_id, m2_id, w1_id, w2_id) values(?, ?, ?, ?)";
				pstmt = conn.prepareStatement(sql);
	        	pstmt.setString(1, mid_1);
	        	pstmt.setString(2, mid_2);
	        	pstmt.setString(3, wid_1);
	        	pstmt.setString(4, wid_2);
	        	
	        	laptime = System.currentTimeMillis();
	        	result = pstmt.executeUpdate();
	        	Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
				Logger.sql(sql); // SQL LOGGING
				
	        	if(result != 0){
	        		Logger.debug("insert meet 2:2 ["+mid_1+","+mid_2+","+wid_1+","+wid_2+"] success."  );
	        		out.print("true");
	        	}
	        	else{
	        		Logger.debug("insert  meet 2:2 ["+mid_1+","+mid_2+","+wid_1+","+wid_2+"] fail."  );
	        		out.print("false");
	        	}
				
	        	if(pstmt!= null)pstmt.close(); 
			}
			else{
		    	out.print("false");
				Logger.debug("delete wait2 ["+wid+"] fail."  );
			}
		}
		
		
		
		
		if(myMeetNum.equals("3")){
			sql = "SELECT * FROM wait3 where id_1='"+wid+"'";
			laptime = System.currentTimeMillis();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			
			while(rs.next())
			{	
				wid_1 = rs.getString("id_1");
				wid_2 = rs.getString("id_2");
				wid_3 = rs.getString("id_3");
			}
			
			if(stmt!= null)stmt.close();
			if(rs!= null)rs.close();
									
			sql = "delete from wait3 where id_1='"+wid+"'";
			laptime = System.currentTimeMillis();
			pstmt = conn.prepareStatement(sql);
			int result = pstmt.executeUpdate();
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			
			if(pstmt!= null)pstmt.close(); 
			
			if(result != 0)
			{
				Logger.debug("delete wait3 ["+wid+"] success."  );
				sql = "insert into meet(m1_id, m2_id, m3_id, w1_id, w2_id, w3_id) values(?, ?, ?, ?, ?, ?)";
				pstmt = conn.prepareStatement(sql);
	        	pstmt.setString(1, mid_1);
	        	pstmt.setString(2, mid_2);
	        	pstmt.setString(3, mid_3);
	        	pstmt.setString(4, wid_1);
	        	pstmt.setString(5, wid_2);
	        	pstmt.setString(6, wid_3);
	        	
				laptime = System.currentTimeMillis();
	        	result = pstmt.executeUpdate();
	        	Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
    			Logger.sql(sql); // SQL LOGGING
    			
	        	if(result != 0){
	        		out.print("true");
	        		Logger.debug("insert meet 3:3 ["+mid_1+","+mid_2+","+mid_3+","+wid_1+","+wid_2+","+wid_3+"] success."  );
	        	}
	        	else{
	        		out.print("false"); 
	        		Logger.debug("insert meet 3:3 ["+mid_1+","+mid_2+","+mid_3+","+wid_1+","+wid_2+","+wid_3+"] fail."  );
	        	}	
	        	
	        	if(pstmt!= null)pstmt.close(); 
			}
			else{
		    	out.print("false"); 
				Logger.debug("delete wait3 ["+wid+"] fail."  );
			}
		}
		
		
		if(myMeetNum.equals("4")){
		      sql = "SELECT * FROM wait4 where id_1='"+wid+"'";     
		      laptime = System.currentTimeMillis();
		      stmt = conn.createStatement();
		      rs = stmt.executeQuery(sql);
		      Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			  Logger.sql(sql); // SQL LOGGING
					
		      while(rs.next())
		      {  
		        wid_1 = rs.getString("id_1");  
		        wid_2 = rs.getString("id_2");
		        wid_3 = rs.getString("id_3");
		        wid_4 = rs.getString("id_4");
		      }
		            
		      if(stmt!= null)stmt.close();
			  if(rs!= null)rs.close();      
		                  
		      sql = "delete from wait4 where id_1='"+wid+"'";
		      pstmt = conn.prepareStatement(sql);
		      laptime = System.currentTimeMillis();
		      int result = pstmt.executeUpdate();
		      Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			  Logger.sql(sql); // SQL LOGGING
					
			  if(pstmt!= null)pstmt.close(); 
					
		      if(result != 0)
		      {
				Logger.debug("delete wait4 ["+wid+"] success."  );
		        sql = "insert into meet(m1_id, m2_id, m3_id, m4_id, w1_id, w2_id, w3_id, w4_id) values(?, ?, ?, ?, ?, ?, ?, ?)";
		        pstmt = conn.prepareStatement(sql);
		            pstmt.setString(1, mid_1);
		            pstmt.setString(2, mid_2);
		            pstmt.setString(3, mid_3);
		            pstmt.setString(4, mid_4);
		            pstmt.setString(5, wid_1);
		            pstmt.setString(6, wid_2);
		            pstmt.setString(7, wid_3);
		            pstmt.setString(8, wid_4);
		            laptime = System.currentTimeMillis();
		            result = pstmt.executeUpdate();
		            Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
					Logger.sql(sql); // SQL LOGGING
					
		            if(result != 0){
		              out.print("true");
	        		  Logger.debug("insert meet 4:4 ["+mid_1+","+mid_2+","+mid_3+"," + mid_4+","+wid_1+","+wid_2+","+wid_3+","+wid_4+"] success."  );
		            }
		            else{
		              out.print("false");
		              Logger.debug("insert meet 4:4 ["+mid_1+","+mid_2+","+mid_3+"," + mid_4+","+wid_1+","+wid_2+","+wid_3+","+wid_4+"] fail."  );
		            }
		            
		            if(pstmt!= null)pstmt.close();   
		      }
		      else{
		          out.print("false");
				  Logger.debug("delete wait4 ["+wid+"] fail."  );
		      }
		      
		      
		    }
				
		TxMgr.getInstance().commit();
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");
	        
	    
		
				
	} catch (Exception e) {
		Logger.error("Service Error.");
		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (rs2 != null) rs2.close();
		if (stmt != null) stmt.close(); 
		if (pstmt != null) pstmt.close(); 
		if (conn != null) TxMgr.getInstance().close();
	}
%>