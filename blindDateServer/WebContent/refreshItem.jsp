<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>

<%@page import="java.security.InvalidKeyException"%>
<%@page import="java.security.Key"%>
 
<%@page import="javax.crypto.BadPaddingException"%>
<%@page import="javax.crypto.Cipher"%>
<%@page import="javax.crypto.IllegalBlockSizeException"%>
<%@page import="javax.crypto.KeyGenerator"%>
<%@page import="javax.crypto.spec.IvParameterSpec"%>
<%@page import="javax.crypto.spec.SecretKeySpec"%>

<%@page import="sun.misc.BASE64Decoder"%>
<%@page import="sun.misc.BASE64Encoder"%>

<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>

<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>

<% 
	
	String sql = null; 
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	try {	
    	String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");  
		String userId = request.getParameter("userId");

		Logger.debug("---------------<< Input value >>---------------------");
		Logger.debug("userId = " + userId);
		TxMgr.getInstance().begin();
		
		conn = TxMgr.getInstance().getConnection();  
		
		stmt = conn.createStatement(); 
		
		String returnString = "false";
		String resultString = "false";
		
		String exTime="";
		Date exDate=null;
		
		String result="";
		Date resultDate=null;
		
		JSONArray arr = new JSONArray();
		JSONObject obj = new JSONObject();
		
		sql = "SELECT refreshItem FROM User Where id='"+userId+"'";
		long laptime = System.currentTimeMillis();

		rs = stmt.executeQuery(sql);
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");

		Logger.sql(sql); // SQL LOGGING

		while(rs.next()){
			exTime = rs.getString("refreshItem");
		}
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");

		Calendar cal = Calendar.getInstance();
		String server = sdf.format(cal.getTime());
		Date serverDate = null;
		

		if(exTime.equals("")){
			cal.add(Calendar.MONTH, 1);
			result = sdf.format(cal.getTime());
			
		} else{
			
			try {
				exDate = sdf.parse(exTime);
				serverDate = sdf.parse(server);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			
			if(exDate.getTime() <= serverDate.getTime()){
				cal.add(Calendar.MONTH, 1);
				result = sdf.format(cal.getTime());
			} else{
				cal.setTime(exDate);
				cal.add(Calendar.MONTH, 1);
				result = sdf.format(cal.getTime());
			}
			
		}
		
		sql = "UPDATE User SET refreshItem ='"+ result +"' where id = '" + userId + "'";
		laptime = System.currentTimeMillis();
 		stmt.executeUpdate(sql);
 		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING

		obj.put("return", "true");
		obj.put("result", result);
		
		if(obj != null)
			arr.add(obj);
		
		out.print(arr);
    	Logger.debug("["+ userId + "] User refreshItem ["+ result +"] Update success.");

		TxMgr.getInstance().commit(); 
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");

		
	} catch (Exception e) {
		Logger.error("Service Error.");
 		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (stmt != null) stmt.close();
		if (conn != null) TxMgr.getInstance().close();
	}
%>
