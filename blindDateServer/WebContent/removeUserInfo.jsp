<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>
<%
	 
	String sql = ""; 
	PreparedStatement pstmt = null;
	Connection conn = null;
	ResultSet rs = null;
	try {	
    	String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");  

		String id = request.getParameter("id");
		
		
		Logger.debug("---------------<< Input value >>---------------------");
		Logger.debug("id = " + id);
		TxMgr.getInstance().begin();
		
		conn = TxMgr.getInstance().getConnection();  
		
		
		sql = "delete from User where id='"+id+"'";
		
		long laptime = System.currentTimeMillis();
		pstmt = conn.prepareStatement(sql);
        
        int result = pstmt.executeUpdate();
        Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING

        if(result != 0) {
        	out.print("true");
        	Logger.debug("["+ id + "] User is delete Success.");
    		TxMgr.getInstance().commit();
        }
        else {
        	out.print("false");
        	Logger.debug("["+ id + "] User is delete fail.");
    		TxMgr.getInstance().rollback();
        }
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");

        
	} catch (Exception e) {
		Logger.error("Service Error.");
 		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (pstmt != null) pstmt.close();
		if (conn != null) TxMgr.getInstance().close();
	}
%>