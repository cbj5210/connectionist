<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>

<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>

<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>

<%@page import="java.security.InvalidKeyException"%>
<%@page import="java.security.Key"%>
 
<%@page import="javax.crypto.BadPaddingException"%>
<%@page import="javax.crypto.Cipher"%>
<%@page import="javax.crypto.IllegalBlockSizeException"%>
<%@page import="javax.crypto.KeyGenerator"%>
<%@page import="javax.crypto.spec.IvParameterSpec"%>
<%@page import="javax.crypto.spec.SecretKeySpec"%>

<%@page import="sun.misc.BASE64Decoder"%>
<%@page import="sun.misc.BASE64Encoder"%>

<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>

<% 
	
	String sql = null; 
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	try {	
		String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");     
		
		TxMgr.getInstance().begin();
		conn = TxMgr.getInstance().getConnection();  
		
		stmt = conn.createStatement();
		
		
		String userId = request.getParameter("userId");
		
		Logger.debug("---------------<< Input value >>---------------------");
		Logger.debug("userId = " + userId);
		
		String returnString = "false";
		String resultString = "false";
		
		int exCount=0;
		String exReportDay = "";
		Date exDate=null;
		
		String result="";
		Date resultDate=null;
		
		JSONArray arr = new JSONArray();
		JSONObject obj = new JSONObject();
		
		sql = "SELECT reportCount, reportDay FROM User Where id='"+userId+"'";
		long laptime = System.currentTimeMillis();
		rs = stmt.executeQuery(sql);
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING
		
		while(rs.next()){
			exCount = rs.getInt("reportCount");
			exReportDay = rs.getString("reportDay");
		}
		
		if(exCount>=99){
			//reportDay setting & reportCount reset
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			
			Calendar cal = Calendar.getInstance();
			String server = sdf.format(cal.getTime());
			Date serverDate = null;
			
			if(exReportDay.equals("")){
				cal.add(Calendar.DATE, 1);
				result = sdf.format(cal.getTime());
			} else{
				cal.add(Calendar.DATE, 3);
				result = sdf.format(cal.getTime());
			}
			
			sql = "UPDATE User SET reportCount = 0 where id = '" + userId + "'"; 
			laptime = System.currentTimeMillis();
			stmt.executeUpdate(sql);
			Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
			Logger.sql(sql); // SQL LOGGING
			
			sql = "UPDATE User SET reportDay ='"+ result +"' where id = '" + userId + "'";
			
		} else{
			//just add one
			sql = "UPDATE User SET reportCount ='"+ Integer.toString(exCount+1) +"' where id = '" + userId + "'";
			
		}
		 
		laptime = System.currentTimeMillis();
		stmt.executeUpdate(sql);
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING
		
		TxMgr.getInstance().commit();
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");
		
	} catch (Exception e) {
		Logger.error("Service Error.");
		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close(); 
		if (stmt != null) stmt.close();  
		if (conn != null) TxMgr.getInstance().close();
	}
%>
