<%@page import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@page import="javax.sql.*"%>
<%@page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>

<%@ include file="/envSetting.jsp" %>
<% out.println("ServerContainer");
%>
<%
		Logger.setDebug(DEBUGLOGLEVEL);
		Logger.setSql(SQLLOGLEVEL);
		Logger.setTrace(TRACELOGLEVEL);
		Logger.setDbio(DBIOLOGLEVEL); 
		
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try {
        	String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
    		String remoteip = request.getRemoteAddr();
    		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
    		TraceTxId.setTraceTime();
    		TraceTxId.setIp(remoteip);
    		Logger.debug("Service Start.");  
    		 
    		TxMgr.getInstance().begin(); 
    		conn = TxMgr.getInstance().getConnection();   
 
%>
<p>
<%
				String query = "Select name, age from User";
				pstmt = conn.prepareStatement(query);
                long laptime = System.currentTimeMillis();
                rs = pstmt.executeQuery();
                Logger.sql("select executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
				Logger.sql(query);    // 2. SQL

                while(rs.next()) {
                        out.println("test");
                        out.println(rs.getString("name"));
                        out.println(rs.getInt("age"));
                }
                
                TxMgr.getInstance().commit();
                Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");    // 3. SERVICE END

        } catch (Exception e) {
        		Logger.error("Service Error.");
                e.printStackTrace();
                TxMgr.getInstance().rollback();
        } finally {
                if (rs != null) rs.close();
                if (pstmt != null) pstmt.close();
                if (conn != null) TxMgr.getInstance().close();
        }
%>
 
