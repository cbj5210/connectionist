<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=euc_kr" pageEncoding="euc_kr"%>
<% request.setCharacterEncoding("euc-kr"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%
	 
	String sql = null; 
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	try {	
    	String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");  
		 
		TxMgr.getInstance().begin();
		
		conn = TxMgr.getInstance().getConnection();  
		
		stmt = conn.createStatement();
		
/* 		if(request.getParameter("name") != null)
		{
			request.setCharacterEncoding("UTF-8");
			String name = new String(request.getParameter("name").getBytes("8859_1"),"UTF-8");
			String age = new String(request.getParameter("age").getBytes("8859_1"),"UTF-8");
			sql = String.format("INSERT INTO XMLTEST(NAME, AGE) values('%s', %s)", name, age);
			System.out.print(sql);
			stmt.executeUpdate(sql);
			System.out.println("�߰� ����");
		} */

		sql = "SELECT * FROM User";
		long laptime = System.currentTimeMillis();

		rs = stmt.executeQuery(sql);
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");

		Logger.sql(sql); // SQL LOGGING

		JSONArray arr = new JSONArray();
		
		while(rs.next())
		{
			String name = URLEncoder.encode(rs.getString("name"), "UTF-8");
			String age = URLEncoder.encode(rs.getString("age"), "UTF-8");
			JSONObject obj = new JSONObject();
			obj.put("name", name);
			obj.put("age", age);
			if(obj != null)
				arr.add(obj);
		}
		out.print(arr);
		TxMgr.getInstance().commit();

		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");
	} catch (Exception e) {
		Logger.error("Service Error.");
 		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (stmt != null) stmt.close();
		if (conn != null) TxMgr.getInstance().close();
	}
%>