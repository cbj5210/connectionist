<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>

<%@page import="java.security.InvalidKeyException"%>
<%@page import="java.security.Key"%>
 
<%@page import="javax.crypto.BadPaddingException"%>
<%@page import="javax.crypto.Cipher"%>
<%@page import="javax.crypto.IllegalBlockSizeException"%>
<%@page import="javax.crypto.KeyGenerator"%>
<%@page import="javax.crypto.spec.IvParameterSpec"%>
<%@page import="javax.crypto.spec.SecretKeySpec"%>

<%@page import="sun.misc.BASE64Decoder"%>
<%@page import="sun.misc.BASE64Encoder"%>

<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>

<% 
	String encYek = "ckouhftfweoe";
	String sql = null; 
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	try {	
    	String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");  
		String id = request.getParameter("id");
		Logger.debug("---------------<< Input value >>---------------------");
		Logger.debug("id = " + id);
		TxMgr.getInstance().begin();
		
		conn = TxMgr.getInstance().getConnection();   
		stmt = conn.createStatement();
		
		  
		sql = "SELECT * FROM User Where id='"+id+"'";
		long laptime = System.currentTimeMillis();

		rs = stmt.executeQuery(sql);
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING

		
		JSONArray arr = new JSONArray();
		JSONObject obj = new JSONObject();
		

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		
		Calendar cal = Calendar.getInstance();
		String server = sdf.format(cal.getTime());
		String serverTimeMinute = sdf2.format(cal.getTime()); 
		

//signed, name, sex, latitude, longitude, gu, age, job, pr, cash 
		
		while(rs.next())
		{
			String signed = rs.getString("signed");
			String name = rs.getString("name");
			String sex = rs.getString("sex");
			double latitude = rs.getDouble("latitude");
			double longitude = rs.getDouble("longitude");
			String location = rs.getString("location");
			String age = rs.getString("age");
			String job = rs.getString("job");
			String smoke = rs.getString("smoke");
			String religion = rs.getString("religion");
			String cash = Decrypt(rs.getString("cash"), encYek);
			String preferage = rs.getString("preferage");
			String phone = rs.getString("phone");
			String refreshItem = rs.getString("refreshItem");
			String lastLogin = rs.getString("lastLogin"); 
			String reportCount = rs.getString("reportCount");
			String reportDay = rs.getString("reportDay");
			
			obj.put("signed", signed);
			obj.put("name", name);
			obj.put("sex", sex);
			obj.put("latitude", latitude);
			obj.put("longitude", longitude);
			obj.put("location", location);
			obj.put("age", age);
			obj.put("job", job);
			obj.put("smoke", smoke);
			obj.put("job", job);
			obj.put("religion", religion);
			obj.put("cash", cash);
			obj.put("preferage", preferage);
			obj.put("phone", phone);
			obj.put("refreshItem", refreshItem);
			obj.put("lastLogin", lastLogin);

			obj.put("reportCount", reportCount);
			obj.put("reportDay", reportDay); 
			obj.put("serverTime", server);
			obj.put("serverTimeMinute", serverTimeMinute);
		}
		
		if(obj != null)
			arr.add(obj);

		out.print(arr); 
		
		sql = "UPDATE User SET lastLogin ='"+ server +"' where id = '" + id + "'";
		
		laptime = System.currentTimeMillis();

 		stmt.executeUpdate(sql);
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING
		TxMgr.getInstance().commit();
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");

	} catch (Exception e) {
		Logger.error("Service Error.");
 		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (stmt != null) stmt.close();
		if (conn != null) TxMgr.getInstance().close();
	}
%>

<%!

public String Decrypt(String text, String key) throws Exception
{
          Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
          byte[] keyBytes= new byte[16];
          byte[] b= key.getBytes("UTF-8");
          int len= b.length;
          if (len > keyBytes.length) len = keyBytes.length;
          System.arraycopy(b, 0, keyBytes, 0, len);
          SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
          IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
          cipher.init(Cipher.DECRYPT_MODE,keySpec,ivSpec);

          BASE64Decoder decoder = new BASE64Decoder();
          byte [] results = cipher.doFinal(decoder.decodeBuffer(text));
          return new String(results,"UTF-8");
}

public String Encrypt(String text, String key) throws Exception
{
          Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
          byte[] keyBytes= new byte[16];
          byte[] b= key.getBytes("UTF-8");
          int len= b.length;
          if (len > keyBytes.length) len = keyBytes.length;
          System.arraycopy(b, 0, keyBytes, 0, len);
          SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
          IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
          cipher.init(Cipher.ENCRYPT_MODE,keySpec,ivSpec);

          byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
          BASE64Encoder encoder = new BASE64Encoder();
          return encoder.encode(results);
}

%>