<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>
<% 
	String sql = ""; 
	PreparedStatement pstmt = null;
	Connection conn = null;
	ResultSet rs = null;
	try {	
    	String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");  
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String sex = request.getParameter("sex");
		String age = request.getParameter("age");
		double latitude = Double.parseDouble(request.getParameter("latitude")) ;
		double longitude = Double.parseDouble(request.getParameter("longitude"));
		String location = request.getParameter("location");
		String preferage = request.getParameter("preferage");
		String job = request.getParameter("job");
		String smoke = request.getParameter("smoke");
		String religion = request.getParameter("religion");
		String phone = request.getParameter("phone");
		String action = request.getParameter("action");

		Logger.debug("---------------<< Input value >>---------------------");
		Logger.debug("id = " + id);
		Logger.debug("name = " + name);
		Logger.debug("sex = " + sex);
		Logger.debug("age = " + age);
		Logger.debug("latitude = " + latitude);
		Logger.debug("longitude = " + longitude);
		Logger.debug("location = " + location);
		Logger.debug("preferage = " + preferage);
		Logger.debug("job = " + job);
		Logger.debug("smoke = " + smoke);
		Logger.debug("religion = " + religion);
		Logger.debug("phone = " + phone);
		Logger.debug("action = " + action);

		TxMgr.getInstance().begin();
		
		conn = TxMgr.getInstance().getConnection();  
		 
		
		if(action.equals("insert")){
			//sql = "insert into User(id, name, sex, age, latitude, longitude, location, preferage, job, smoke, religion, phone, signed, cash) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 'Izd4Yom9eiSIm+E+Ua+8bg==')";
			sql = "insert into User(id, name, sex, age, latitude, longitude, location, preferage, job, smoke, religion, phone, signed, cash) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '/5ug6Q723IpaXElo0qIdXg==')";
		} else if(action.equals("update")){
			sql = "update User set id=?, name=?, sex=?, age=?, latitude=?, longitude=?, location=?, preferage=?, job=?, smoke=?, religion=?, phone=?, signed=? where id='"+id+"'";
		}
		
 		
		pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, id);
        pstmt.setString(2, name);
        pstmt.setString(3, sex);
        pstmt.setString(4, age);
        pstmt.setDouble(5, latitude);
        pstmt.setDouble(6, longitude);
        pstmt.setString(7, location);
        pstmt.setString(8, preferage);
        pstmt.setString(9, job);
        pstmt.setString(10, smoke);
        pstmt.setString(11, religion);
        pstmt.setString(12, phone);
        pstmt.setString(13, "1");
        
        long laptime = System.currentTimeMillis();
        int result = pstmt.executeUpdate();
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");
		Logger.sql(sql); // SQL LOGGING

        if(result != 0) {
        	out.print("true");
        	Logger.debug("["+ id + "] User " + action + " success. ");
    		TxMgr.getInstance().commit();
        }
        else {
        	out.print("false");
        	Logger.debug("["+ id + "] User " + action + " fail. ");
    		TxMgr.getInstance().rollback();
        }
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");

	} catch (Exception e) {
		Logger.error("Service Error.");
 		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (pstmt != null) pstmt.close();
		if (conn != null) TxMgr.getInstance().close();
	}
%>