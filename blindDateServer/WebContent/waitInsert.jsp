<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.sql.*"%>
<%@ page import="javax.sql.*"%>
<%@ page import="javax.naming.*"%>
<%@page import="com.connections.blindDateServer.db.tx.*"%>
<%@page import="com.connections.blindDateServer.common.*"%>
<%@page import="com.connections.blindDateServer.common.utils.*"%>
<%@ page import="org.json.simple.*"%>
<%@page import="java.util.Enumeration"%>
<%
	 
	String sql = ""; 
	PreparedStatement pstmt = null;
	Connection conn = null;
	ResultSet rs = null;
	try {	
    	String actionName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().lastIndexOf("."));
		String remoteip = request.getRemoteAddr();
		TraceTxId.regist(SystemUtil.getTraceId(remoteip), "["+actionName+"]");
		TraceTxId.setTraceTime();
		TraceTxId.setIp(remoteip);
		Logger.debug("Service Start.");  
		String id_1 = request.getParameter("id_1");
		String id_2 = request.getParameter("id_2");
		String id_3 = request.getParameter("id_3");
		String id_4 = request.getParameter("id_4");
		double latitude = Double.parseDouble(request.getParameter("latitude")) ;
		double longitude = Double.parseDouble(request.getParameter("longitude"));
		
		String location = request.getParameter("location");
		String sex = request.getParameter("sex");
		String action = request.getParameter("action");
		
		Logger.debug("---------------<< Input value >>---------------------");
		Logger.debug("id_1 = " + id_1);
		Logger.debug("id_2 = " + id_2);
		Logger.debug("id_3 = " + id_3);
		Logger.debug("id_4 = " + id_4);

		Logger.debug("latitude = " + latitude);
		Logger.debug("longitude = " + longitude);
		Logger.debug("location = " + location); 
		Logger.debug("sex = " + sex);
		Logger.debug("action = " + action);
		
		
		TxMgr.getInstance().begin(); 
		conn = TxMgr.getInstance().getConnection();  
		
		  
		if(action.equals("two")){
			
			sql = "insert into wait2(id_1, id_2, latitude, longitude, location, sex) values(?, ?, ?, ?, ?, ?)";
		 
			
			pstmt = conn.prepareStatement(sql);
	        pstmt.setString(1, id_1);
	        pstmt.setString(2, id_2);
	        pstmt.setDouble(3, latitude);
	        pstmt.setDouble(4, longitude);
	        pstmt.setString(5, location);
	        pstmt.setString(6, sex);
        
		} else if (action.equals("three")){
			
			sql = "insert into wait3(id_1, id_2, id_3, latitude, longitude, location, sex) values(?, ?, ?, ?, ?, ?, ?)";
			 
			pstmt = conn.prepareStatement(sql);
	        pstmt.setString(1, id_1);
	        pstmt.setString(2, id_2);
	        pstmt.setString(3, id_3);
	        pstmt.setDouble(4, latitude);
	        pstmt.setDouble(5, longitude);
	        pstmt.setString(6, location);
	        pstmt.setString(7, sex);	
			
		} else if(action.equals("four")){
			
			sql = "insert into wait4(id_1, id_2, id_3, id_4, latitude, longitude, location, sex) values(?, ?, ?, ?, ?, ?, ?, ?)";
			  
			pstmt = conn.prepareStatement(sql);
	        pstmt.setString(1, id_1);
	        pstmt.setString(2, id_2);
	        pstmt.setString(3, id_3);
	        pstmt.setString(4, id_4);
	        pstmt.setDouble(5, latitude);
	        pstmt.setDouble(6, longitude);
	        pstmt.setString(7, location);
	        pstmt.setString(8, sex);
			
		}
        
		long laptime = System.currentTimeMillis();

         int result = pstmt.executeUpdate();
		Logger.sql("executeQuery(" + (System.currentTimeMillis() - laptime) + "ms)");

		Logger.sql(sql); // SQL LOGGING
		

		
        if(result!=0){ 
        	out.print("true");
    		TxMgr.getInstance().commit();
        }
        else {
        	out.print("false");
    		TxMgr.getInstance().rollback();

        }
		
		Logger.debug("Service End. (" + TraceTxId.getExecuteTime()+ "ms)");

	} catch (Exception e) {
		Logger.error("Service Error.");
 		e.printStackTrace();
		TxMgr.getInstance().rollback();
	} finally {
		if (rs != null) rs.close();
		if (pstmt != null) pstmt.close();
		if (conn != null) TxMgr.getInstance().close();
	}
%>
